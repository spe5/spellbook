# Spellbook Project

Spellbook project develops **database of equations** and related tools, aimed for rapid technology-based development. Its open source, released under MIT licence.

It is NOT aimed or limited to certain topic - like text editor, you can use it to write any document.

For example, author uses his database of equations to store topics related to thermonuclear fusion.

## Motivation

Main problems of current projects, products & technologies are:
- largeness
- complexity
- dependency
- low efficiency

In order to work on bigger projects, developers need to know many related tools (i.e. for C++ programming you need to know also IDE, Git, CMake, debugger, etc.)

Bigger projects consists of too many components, programs and applications (microservices), each have own states and dependencies. Its hard to keep track of it all and troubleshoot when something goes wrong. Sonner or later, it always does.

When some dependency is *black box* with Intellectual Property of 3rd party, troubleshooting options are limited. When project depends on Intellectual Property of 3rd party, which refuses cooperate at all, project is blocked.

As result of project largeness and high complexity with dependencies, its
1. hard to compose team, with competent team members covering all needed topics
2. educate junior team members, which takes many years, with risk of leaving without being actually productive
3. hard to do innovations (change management) - detect relevant changes in used and new technologies, analyze impact on whole project (incl. dependencies), update the project
4. inefficient

## Solution

Lets simplify things and be transparent: use mathematical formalism based on equations, as definitions, theorems and proofs (called *spells* here).

Then, the only requirement for team member is knowledge how to work with this mathematical formalism. Everything else, all technologies, comes in as an equation(s). Putting together project, which consists of many technologies, is equivalent to putting together set of equations and solving them.

Innovations and change management can be easily analyzed by switching one equation for another, or in special cases just using perturbation theory.

Even protected black boxes may be translated to equation with unknown *plus something here* part. Also, algebraic properties like *its linear* or *converges to zero* can be written as theorem. Since rest of the equation is still available, it can be used for dummy component in early development, troubleshooting or later in regression tests with more accurate model (equation). Owner of intellectual property stays also happy.

In principle, this equation database, along with several other related tools, shall ensure perfect information handling, making developer team and project more efficient. No more *"I dont understand it"*, *"documentation is too long"* or *"documentation is missing"* - its an equation (theorem), its exact, with defined quantities, linked to their exact definitions. Even verification is possible due to proofs included. There is nothing to be misuderstood, missing or incomplete.

## Architecture & Related tools

The **equation database** comes with several tools:
1. editor of the equation database
2. implementations of the equations as new or existing plug-ins or modules into existing simulation tools
3. plug-in for existing 3D geometry editor
4. article database aka database of experiments

The wokflow then looks like this:
1. developer gets new measurement from an article with experiment
2. developer decomposes the experiment into equations by modelling
3. developer implements new or updated model into classic simulation tools
4. developer deploys new models on 3D digital twin of experiment
5. developer verifies new model by comparing 3D digital twin with experiment
6. developer uses updated model in his product to make money

<img src="/doc/spellbook-business-workflow.png" alt="Spellbook Business Workflow" style="height: 300px; width:400px;"/>

# Installation & Status

As of 2023, there is no release available, sorry.

Work done for first (alpha) release:
1. Database Editor 50%
2. SpellTeX: 50%
3. Implementation of equations: 0%
4. 3D Plugin: 0%
5. Database of equations (public part): 50%
6. Database of experiments and articles (public part): 0%

# Components

## Database Editor
Equation Database stores LaTeX-formatted equations. Thus, database editor implements LaTeX typesetter to render them. This typesetter is called **SpellTeX** and stored in subfolder spelltex.

TODO


