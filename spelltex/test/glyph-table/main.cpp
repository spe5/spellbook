#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <QGuiApplication>
#include <QPixmap>
#include <QPainter>
#include <QFile>
#include <QFont>
#include <QRawFont>
#include <QGlyphRun>

#define TTF_CMMI10_SMALL_ALPHA_GLYPH_INDEX 110
#define TTF_CMMI10_SMALL_BETA_GLYPH_INDEX 111
#define TTF_CMMI10_SMALL_GAMMA_GLYPH_INDEX 112
#define TTF_CMMI10_SMALL_DELTA_GLYPH_INDEX 113
#define TTF_CMMI10_SMALL_EPSILON_GLYPH_INDEX 114
#define TTF_CMMI10_SMALL_VAREPSILON_GLYPH_INDEX 5
#define TTF_CMMI10_SMALL_ZETA_GLYPH_INDEX 115
#define TTF_CMMI10_SMALL_ETA_GLYPH_INDEX 116
#define TTF_CMMI10_SMALL_THETA_GLYPH_INDEX 117
#define TTF_CMMI10_SMALL_VARTHETA_GLYPH_INDEX 6
#define TTF_CMMI10_SMALL_IOTA_GLYPH_INDEX 118
#define TTF_CMMI10_SMALL_KAPPA_GLYPH_INDEX 119
#define TTF_CMMI10_SMALL_LAMBDA_GLYPH_INDEX 120
#define TTF_CMMI10_SMALL_MU_GLYPH_INDEX 121
#define TTF_CMMI10_SMALL_NU_GLYPH_INDEX 122
#define TTF_CMMI10_SMALL_XI_GLYPH_INDEX 123
#define TTF_CMMI10_SMALL_OMICRON_GLYPH_INDEX 82
#define TTF_CMMI10_SMALL_PI_GLYPH_INDEX 124
#define TTF_CMMI10_SMALL_RHO_GLYPH_INDEX 125
#define TTF_CMMI10_SMALL_VARRHO_GLYPH_INDEX 8
#define TTF_CMMI10_SMALL_SIGMA_GLYPH_INDEX 126
#define TTF_CMMI10_SMALL_TAU_GLYPH_INDEX 127
#define TTF_CMMI10_SMALL_YPSILON_GLYPH_INDEX 128
#define TTF_CMMI10_SMALL_PHI_GLYPH_INDEX 129
#define TTF_CMMI10_SMALL_VARPHI_GLYPH_INDEX 10
#define TTF_CMMI10_SMALL_CHI_GLYPH_INDEX 130
#define TTF_CMMI10_SMALL_PSI_GLYPH_INDEX 131
#define TTF_CMMI10_SMALL_OMEGA_GLYPH_INDEX 4

#define TTF_CMMI10_DOT_GLYPH_INDEX 29
#define TTF_CMMI10_COMMA_GLYPH_INDEX 30
#define TTF_CMMI10_LEFT_ANGLE_BRACKET_GLYPH_INDEX 31
#define TTF_CMMI10_RIGHT_ANGLE_BRACKET_GLYPH_INDEX 33
#define TTF_CMMI10_SLASH_GLYPH_INDEX 32
#define TTF_CMMI10_PARTIAL_GLYPH_INDEX 35
#define TTF_CMMI10_CAPITAL_GAMMA_GLYPH_INDEX 99
#define TTF_CMMI10_CAPITAL_DELTA_GLYPH_INDEX 100
#define TTF_CMMI10_CAPITAL_THETA_GLYPH_INDEX 101
#define TTF_CMMI10_CAPITAL_LAMBDA_GLYPH_INDEX 102
#define TTF_CMMI10_CAPITAL_XI_GLYPH_INDEX 103
#define TTF_CMMI10_CAPITAL_PI_GLYPH_INDEX 104
#define TTF_CMMI10_CAPITAL_SIGMA_GLYPH_INDEX 105
#define TTF_CMMI10_CAPITAL_YPSILON_GLYPH_INDEX 106
#define TTF_CMMI10_CAPITAL_PHI_GLYPH_INDEX 107
#define TTF_CMMI10_CAPITAL_PSI_GLYPH_INDEX 108
#define TTF_CMMI10_CAPITAL_OMEGA_GLYPH_INDEX 109
#define TTF_CMMI10_VECTOR_GLYPH_INDEX 97
#define TTF_CMMI10_STAR_GLYPH_INDEX 34
#define TTF_CMMI10_LEFT_UPPER_HALF_ARROW_GLYPH_INDEX 11
#define TTF_CMMI10_LEFT_LOWER_HALF_ARROW_GLYPH_INDEX 12
#define TTF_CMMI10_RIGHT_UPPER_HALF_ARROW_GLYPH_INDEX 13
#define TTF_CMMI10_RIGHT_LOWER_HALF_ARROW_GLYPH_INDEX 14
#define TTF_CMMI10_HASH_SIGN_GLYPH_INDEX 64


#define TTF_CMMI10_CAPITAL_A_GLYPH_INDEX 36
#define TTF_CMMI10_CAPITAL_B_GLYPH_INDEX 37
#define TTF_CMMI10_CAPITAL_C_GLYPH_INDEX 38
#define TTF_CMMI10_CAPITAL_D_GLYPH_INDEX 39
#define TTF_CMMI10_CAPITAL_E_GLYPH_INDEX 40
#define TTF_CMMI10_CAPITAL_F_GLYPH_INDEX 41
#define TTF_CMMI10_CAPITAL_G_GLYPH_INDEX 42
#define TTF_CMMI10_CAPITAL_H_GLYPH_INDEX 43
#define TTF_CMMI10_CAPITAL_I_GLYPH_INDEX 44
#define TTF_CMMI10_CAPITAL_J_GLYPH_INDEX 45
#define TTF_CMMI10_CAPITAL_K_GLYPH_INDEX 46
#define TTF_CMMI10_CAPITAL_L_GLYPH_INDEX 47
#define TTF_CMMI10_CAPITAL_M_GLYPH_INDEX 48
#define TTF_CMMI10_CAPITAL_N_GLYPH_INDEX 49
#define TTF_CMMI10_CAPITAL_O_GLYPH_INDEX 50
#define TTF_CMMI10_CAPITAL_P_GLYPH_INDEX 51
#define TTF_CMMI10_CAPITAL_Q_GLYPH_INDEX 52
#define TTF_CMMI10_CAPITAL_R_GLYPH_INDEX 53
#define TTF_CMMI10_CAPITAL_S_GLYPH_INDEX 54
#define TTF_CMMI10_CAPITAL_T_GLYPH_INDEX 55
#define TTF_CMMI10_CAPITAL_U_GLYPH_INDEX 56
#define TTF_CMMI10_CAPITAL_V_GLYPH_INDEX 57
#define TTF_CMMI10_CAPITAL_W_GLYPH_INDEX 58
#define TTF_CMMI10_CAPITAL_X_GLYPH_INDEX 59
#define TTF_CMMI10_CAPITAL_Y_GLYPH_INDEX 60
#define TTF_CMMI10_CAPITAL_Z_GLYPH_INDEX 61

#define TTF_CMMI10_SMALL_A_GLYPH_INDEX 68
#define TTF_CMMI10_SMALL_B_GLYPH_INDEX 69
#define TTF_CMMI10_SMALL_C_GLYPH_INDEX 70
#define TTF_CMMI10_SMALL_D_GLYPH_INDEX 71
#define TTF_CMMI10_SMALL_E_GLYPH_INDEX 72
#define TTF_CMMI10_SMALL_F_GLYPH_INDEX 73
#define TTF_CMMI10_SMALL_G_GLYPH_INDEX 74
#define TTF_CMMI10_SMALL_H_GLYPH_INDEX 75
#define TTF_CMMI10_SMALL_I_GLYPH_INDEX 76
#define TTF_CMMI10_SMALL_J_GLYPH_INDEX 77
#define TTF_CMMI10_SMALL_K_GLYPH_INDEX 78
#define TTF_CMMI10_SMALL_L_GLYPH_INDEX 79
#define TTF_CMMI10_SMALL_VARL_GLYPH_INDEX 67
#define TTF_CMMI10_SMALL_M_GLYPH_INDEX 80
#define TTF_CMMI10_SMALL_N_GLYPH_INDEX 81
#define TTF_CMMI10_SMALL_O_GLYPH_INDEX 82
#define TTF_CMMI10_SMALL_P_GLYPH_INDEX 83
#define TTF_CMMI10_SMALL_Q_GLYPH_INDEX 84
#define TTF_CMMI10_SMALL_R_GLYPH_INDEX 85
#define TTF_CMMI10_SMALL_S_GLYPH_INDEX 86
#define TTF_CMMI10_SMALL_T_GLYPH_INDEX 87
#define TTF_CMMI10_SMALL_U_GLYPH_INDEX 88
#define TTF_CMMI10_SMALL_V_GLYPH_INDEX 89
#define TTF_CMMI10_SMALL_W_GLYPH_INDEX 90
#define TTF_CMMI10_SMALL_X_GLYPH_INDEX 91
#define TTF_CMMI10_SMALL_Y_GLYPH_INDEX 92
#define TTF_CMMI10_SMALL_Z_GLYPH_INDEX 93

#define TTF_CMSY10_RIGHT_SIMPLE_ARROW_GLYPH_INDEX 4
#define TTF_CMSY10_LEFT_SIMPLE_ARROW_GLYPH_INDEX 131
#define TTF_CMSY10_UPPER_SIMPLE_ARROW_GLYPH_INDEX 5
#define TTF_CMSY10_LOWER_SIMPLE_ARROW_GLYPH_INDEX 6
#define TTF_CMSY10_LEFTRIGHT_SIMPLE_ARROW_GLYPH_INDEX 7
#define TTF_CMSY10_RIGHT_UPPER_SIMPLE_ARROW_GLYPH_INDEX 8
#define TTF_CMSY10_RIGHT_LOWER_SIMPLE_ARROW_GLYPH_INDEX 9
#define TTF_CMSY10_APPROX_GLYPH_INDEX 10
#define TTF_CMSY10_LEFT_IMPLY_GLYPH_INDEX 11
#define TTF_CMSY10_RIGHT_IMPLY_GLYPH_INDEX 12
#define TTF_CMSY10_UPPER_IMPLY_GLYPH_INDEX 13
#define TTF_CMSY10_LOWER_IMPLY_GLYPH_INDEX 14
#define TTF_CMSY10_LEFT_RIGHT_IMPLY_GLYPH_INDEX 15
#define TTF_CMSY10_UPPER_LOWER_IMPLY_GLYPH_INDEX 80
#define TTF_CMSY10_INFINITY_GLYPH_INDEX 20
#define TTF_CMSY10_IN_GLYPH_INDEX 21
#define TTF_CMSY10_REVERSE_IN_GLYPH_INDEX 22

#define TTF_CMSY10_UPPER_RECTANGLE_GLYPH_INDEX 23
#define TTF_CMSY10_LOWER_RECTANGLE_GLYPH_INDEX 24
#define TTF_CMSY10_SLASH_GLYPH_INDEX 25
#define TTF_CMSY10_FORALL_GLYPH_INDEX 27
#define TTF_CMSY10_EXISTS_GLYPH_INDEX 28
#define TTF_CMSY10_EMPTY_GLYPH_INDEX 30
#define TTF_CMSY10_MATHCAL_A_GLYPH_INDEX 36
#define TTF_CMSY10_MATHCAL_B_GLYPH_INDEX 37
#define TTF_CMSY10_MATHCAL_C_GLYPH_INDEX 38
#define TTF_CMSY10_MATHCAL_D_GLYPH_INDEX 39
#define TTF_CMSY10_MATHCAL_E_GLYPH_INDEX 40
#define TTF_CMSY10_MATHCAL_F_GLYPH_INDEX 41
#define TTF_CMSY10_MATHCAL_G_GLYPH_INDEX 42
#define TTF_CMSY10_MATHCAL_H_GLYPH_INDEX 43
#define TTF_CMSY10_MATHCAL_I_GLYPH_INDEX 44
#define TTF_CMSY10_MATHCAL_J_GLYPH_INDEX 45
#define TTF_CMSY10_MATHCAL_K_GLYPH_INDEX 46
#define TTF_CMSY10_MATHCAL_L_GLYPH_INDEX 47
#define TTF_CMSY10_MATHCAL_M_GLYPH_INDEX 48
#define TTF_CMSY10_MATHCAL_N_GLYPH_INDEX 49
#define TTF_CMSY10_MATHCAL_O_GLYPH_INDEX 50
#define TTF_CMSY10_MATHCAL_P_GLYPH_INDEX 51
#define TTF_CMSY10_MATHCAL_Q_GLYPH_INDEX 52
#define TTF_CMSY10_MATHCAL_R_GLYPH_INDEX 53
#define TTF_CMSY10_MATHCAL_S_GLYPH_INDEX 54
#define TTF_CMSY10_MATHCAL_T_GLYPH_INDEX 55
#define TTF_CMSY10_MATHCAL_U_GLYPH_INDEX 56
#define TTF_CMSY10_MATHCAL_V_GLYPH_INDEX 57
#define TTF_CMSY10_MATHCAL_W_GLYPH_INDEX 58
#define TTF_CMSY10_MATHCAL_X_GLYPH_INDEX 59
#define TTF_CMSY10_MATHCAL_Y_GLYPH_INDEX 60
#define TTF_CMSY10_MATHCAL_Z_GLYPH_INDEX 61

#define TTF_CMSY10_UNION_GLYPH_INDEX 62
#define TTF_CMSY10_PENETRATION_GLYPH_INDEX 63
#define TTF_CMSY10_UNION_PLUS_GLYPH_INDEX 64
#define TTF_CMSY10_WEDGE_GLYPH_INDEX 65
#define TTF_CMSY10_VEE_GLYPH_INDEX 66
#define TTF_CMSY10_LEFT_FLOOR_GLYPH_INDEX 69
#define TTF_CMSY10_RIGHT_FLOOR_GLYPH_INDEX 70
#define TTF_CMSY10_LEFT_CEIL_GLYPH_INDEX 71
#define TTF_CMSY10_RIGHT_CEIL_GLYPH_INDEX 72
#define TTF_CMSY10_LEFT_BRACE_GLYPH_INDEX 73
#define TTF_CMSY10_RIGHT_BRACE_GLYPH_INDEX 74
#define TTF_CMSY10_LEFT_ANGLE_BRACKET_GLYPH_INDEX 75
#define TTF_CMSY10_RIGHT_ANGLE_BRACKET_GLYPH_INDEX 76
#define TTF_CMSY10_VERTICAL_LINE_GLYPH_INDEX 77
#define TTF_CMSY10_VERTICAL_DOUBLE_LINE_GLYPH_INDEX 78
#define TTF_CMSY10_BACK_SLASH_GLYPH_INDEX 81
#define TTF_CMSY10_SQUARE_ROOT_GLYPH_INDEX 83
#define TTF_CMSY10_REVERSED_SIGMA_GLYPH_INDEX 84
#define TTF_CMSY10_GRAD_GLYPH_INDEX 85
#define TTF_CMSY10_INTEGRAL_GLYPH_INDEX 86
#define TTF_CMSY10_LOWER_SQUARED_CUP_GLYPH_INDEX 87
#define TTF_CMSY10_UPPER_SQUARED_CAP_GLYPH_INDEX 88
#define TTF_CMSY10_SQUARED_SUBSET_OR_EQUAL_GLYPH_INDEX 89
#define TTF_CMSY10_SQUARED_SUPERSET_OR_EQUAL_GLYPH_INDEX 90
#define TTF_CMSY10_PARAGRAPH_GLYPH_INDEX 91

#define TTF_CMSY10_DAGGER_GLYPH_INDEX 92
#define TTF_CMSY10_DOUBLE_DAGGER_GLYPH_INDEX 93
#define TTF_CMSY10_UNDERSCORE_GLYPH_INDEX 99
#define TTF_CMSY10_CDOT_GLYPH_INDEX 100
#define TTF_CMSY10_TIMES_GLYPH_INDEX 101
#define TTF_CMSY10_AST_GLYPH_INDEX 102
#define TTF_CMSY10_DIVISION_GLYPH_INDEX 103
#define TTF_CMSY10_DIAMOND_GLYPH_INDEX 104
#define TTF_CMSY10_PLUS_MINUS_GLYPH_INDEX 105
#define TTF_CMSY10_MINUS_PLUS_GLYPH_INDEX 106
#define TTF_CMSY10_O_PLUS_GLYPH_INDEX 107
#define TTF_CMSY10_O_MINUS_GLYPH_INDEX 108
#define TTF_CMSY10_O_TIMES_GLYPH_INDEX 109
#define TTF_CMSY10_O_SLASH_GLYPH_INDEX 110
#define TTF_CMSY10_O_DOT_GLYPH_INDEX 111
#define TTF_CMSY10_BIGCIRC_GLYPH_INDEX 112
#define TTF_CMSY10_CIRC_GLYPH_INDEX 113
#define TTF_CMSY10_BULLET_GLYPH_INDEX 114
#define TTF_CMSY10_EQUIV_GLYPH_INDEX 116
#define TTF_CMSY10_SUBSETEQ_GLYPH_INDEX 117
#define TTF_CMSY10_SUPSETEQ_GLYPH_INDEX 118
#define TTF_CMSY10_LESS_OR_EQUAL_GLYPH_INDEX 119
#define TTF_CMSY10_BIGGER_OR_EQUAL_GLYPH_INDEX 120
#define TTF_CMSY10_SIMILAR_GLYPH_INDEX 123
#define TTF_CMSY10_APPROXIMATE_GLYPH_INDEX 124
#define TTF_CMSY10_SUBSET_GLYPH_INDEX 125
#define TTF_CMSY10_SUPSET_GLYPH_INDEX 126
#define TTF_CMSY10_DOUBLE_LESSER_GLYPH_INDEX 127
#define TTF_CMSY10_DOUBLE_GREATER_GLYPH_INDEX 128



void set_red_brush(QPainter &painter){
        QPen current_pen = painter.pen();
        QBrush current_brush = current_pen.brush();
        current_brush.setColor(Qt::red);
        current_pen.setBrush(current_brush);
        painter.setPen(current_pen);
}


void set_black_brush(QPainter &painter){
        QPen current_pen = painter.pen();
        QBrush current_brush = current_pen.brush();
        current_brush.setColor(Qt::black);
        current_pen.setBrush(current_brush);
        painter.setPen(current_pen);
}


void draw_lower_debug_rectangle(QPainter &painter, const QPointF left_baseline_position, const double _width, const double _depth){
        QRectF lower_debug_rectangle = QRectF(0, 0, _width , _depth);
	set_black_brush(painter);
	lower_debug_rectangle.moveTopLeft(left_baseline_position);
	painter.drawRect(lower_debug_rectangle);
}


void draw_upper_debug_rectangle(QPainter &painter, const QPointF left_baseline_position, const double _width, const double _partial_height){
        QRectF upper_debug_rectangle = QRectF(0, 0, _width , _partial_height);
	set_red_brush(painter);
	upper_debug_rectangle.moveBottomLeft(left_baseline_position);
	painter.drawRect(upper_debug_rectangle);
}


void get_glyph_geometry(const QRawFont raw_font, const int glyph_index, double &_width, double &_partial_height, double &_depth, double &_height){
        QRectF full_debug_rectangle = raw_font.boundingRect(glyph_index);

	_depth = abs(full_debug_rectangle.bottomLeft().y());
	_width = abs(full_debug_rectangle.topRight().x() - full_debug_rectangle.bottomLeft().x());
	_height = abs(full_debug_rectangle.topRight().y() - full_debug_rectangle.bottomLeft().y());
	_partial_height = abs(_height - _depth);
}


void get_letter_geometry(QFont &font, const std::string letter, double &_width, double &_partial_height, double &_depth, double &_height, double &_ascent){
        QFontMetrics metrics(font);
	QRectF box = metrics.tightBoundingRect(QString::fromStdString(letter));
        _width = abs(box.width());
        _height = abs(box.height());
        double overline = metrics.overlinePos();
        _ascent = metrics.ascent();
        double descent = metrics.descent();
        QPointF bot_left = box.bottomLeft();
	QPointF top_right = box.topRight();
	_depth = abs(box.bottomLeft().y());
	_partial_height = abs(_height - _depth);


}


void draw_std_letter_only(QPainter &painter, QFont &font, const std::string letter, QPointF left_baseline_position, const double ascent){
	set_black_brush(painter);
        painter.setFont(font);
	QPointF top_left_corner(left_baseline_position.x(), left_baseline_position.y() - ascent); // convert left baseline position point to top left corner point
        const QRect _remaining_page_space = QRect(top_left_corner.x(), top_left_corner.y(), 800, 800); // QRectF(qreal x, qreal y, qreal width, qreal height)
	painter.drawText(_remaining_page_space, Qt::AlignLeft, QString::fromStdString(letter), nullptr);
}


int draw_std_letter(QPainter &painter, QFont &font, const std::string letter, QPointF left_baseline_position){

	// draw debug rectangle
	double _width = 0.0;
	double _partial_height = 0.0;
	double _height = 0.0;
	double _ascent = 0.0;
	double _depth = 0.0;
	get_letter_geometry(font, letter, _width, _partial_height, _depth, _height, _ascent);
        draw_std_letter_only(painter, font, letter, left_baseline_position, _ascent);
	draw_upper_debug_rectangle(painter, left_baseline_position, _width, _partial_height);
	draw_lower_debug_rectangle(painter, left_baseline_position, _width, _depth);

        std::cout << "STD " << letter << " (x=" << left_baseline_position.x() << " y=" << left_baseline_position.y() << " w=" << _width << " h=" << _height << " ph=" << _partial_height << " d=" << _depth << " a=" << _ascent << ")" << std::endl;
	
	return 0;
}


void draw_glyph_only(QPainter &painter, const QRawFont raw_font, const int glyph_index, QPointF left_baseline_position){
        QVector<quint32> my_glyphs;
        my_glyphs.push_back(glyph_index);

        QVector<QPointF> my_positions;
        QPointF first_position(0,0);
        my_positions.push_back(first_position);

        QGlyphRun my_glyph_run;
        my_glyph_run.setRawFont(raw_font);
        my_glyph_run.setGlyphIndexes(my_glyphs);
        my_glyph_run.setPositions(my_positions);
	set_black_brush(painter);
        painter.drawGlyphRun(left_baseline_position, my_glyph_run);
}




int draw_glyph(QPainter &painter, const QRawFont raw_font, const int glyph_index, QPointF left_baseline_position){

	double _width = 0.0;
	double _partial_height = 0.0;
	double _depth = 0.0;
	double _height = 0.0;

	get_glyph_geometry(raw_font, glyph_index, _width, _partial_height, _depth, _height);
	draw_glyph_only(painter, raw_font, glyph_index, left_baseline_position);
	draw_lower_debug_rectangle(painter, left_baseline_position, _width, _depth);
	draw_upper_debug_rectangle(painter, left_baseline_position, _width, _partial_height);

        std::cout << "TTF GLYPH " << " gid=" << glyph_index << " (x=" << left_baseline_position.x() << " y=" << left_baseline_position.y() << " w=" << _width << " h=" << _height << " ph=" << _partial_height << " d=" << _depth << ")" << std::endl;
        return 0;
}


int main(int argc, char* argv[]){
        QGuiApplication app(argc, argv);

	int root_box_width = 1820; // got from available space in UI
	int root_box_height = 820; // got from available space in UI
	QPixmap my_pixmap(root_box_width, root_box_height);

        my_pixmap.fill(Qt::white);
        QPainter my_painter(&my_pixmap);
        my_painter.setRenderHint(QPainter::Antialiasing, true);

	int _pixel_size = 20;
	QFont bold_times_20_font("Times", _pixel_size, QFont::Bold);

        draw_std_letter(my_painter, bold_times_20_font, std::string("a"), QPointF(20,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("b"), QPointF(40,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("c"), QPointF(60,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("d"), QPointF(80,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("e"), QPointF(100,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("f"), QPointF(120,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("g"), QPointF(140,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("h"), QPointF(160,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("i"), QPointF(180,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("j"), QPointF(200,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("k"), QPointF(220,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("l"), QPointF(240,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("m"), QPointF(260,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("n"), QPointF(280,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("o"), QPointF(300,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("p"), QPointF(320,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("q"), QPointF(340,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("r"), QPointF(360,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("s"), QPointF(380,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("t"), QPointF(400,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("u"), QPointF(420,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("v"), QPointF(440,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("w"), QPointF(460,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("x"), QPointF(480,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("y"), QPointF(500,20));
        draw_std_letter(my_painter, bold_times_20_font, std::string("z"), QPointF(520,20));

	_pixel_size = 12;
	QFont bold_times_12_font("Times", _pixel_size, QFont::Normal);
        draw_std_letter(my_painter, bold_times_12_font, std::string("a"), QPointF(20,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("b"), QPointF(40,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("c"), QPointF(60,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("d"), QPointF(80,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("e"), QPointF(100,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("f"), QPointF(120,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("g"), QPointF(140,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("h"), QPointF(160,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("i"), QPointF(180,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("j"), QPointF(200,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("k"), QPointF(220,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("l"), QPointF(240,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("m"), QPointF(260,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("n"), QPointF(280,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("o"), QPointF(300,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("p"), QPointF(320,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("q"), QPointF(340,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("r"), QPointF(360,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("s"), QPointF(380,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("t"), QPointF(400,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("u"), QPointF(420,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("v"), QPointF(440,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("w"), QPointF(460,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("x"), QPointF(480,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("y"), QPointF(500,50));
        draw_std_letter(my_painter, bold_times_12_font, std::string("z"), QPointF(520,50));

	_pixel_size = 20;
	QFont bold_times_20_italic_font("Times", _pixel_size, QFont::Normal, QFont::StyleItalic);
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("a"), QPointF(20,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("b"), QPointF(40,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("c"), QPointF(60,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("d"), QPointF(80,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("e"), QPointF(100,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("f"), QPointF(120,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("g"), QPointF(140,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("h"), QPointF(160,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("i"), QPointF(180,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("j"), QPointF(200,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("k"), QPointF(220,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("l"), QPointF(240,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("m"), QPointF(260,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("n"), QPointF(280,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("o"), QPointF(300,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("p"), QPointF(320,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("q"), QPointF(340,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("r"), QPointF(360,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("s"), QPointF(380,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("t"), QPointF(400,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("u"), QPointF(420,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("v"), QPointF(440,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("w"), QPointF(460,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("x"), QPointF(480,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("y"), QPointF(500,80));
        draw_std_letter(my_painter, bold_times_20_italic_font, std::string("z"), QPointF(520,80));


	_pixel_size = 20;
        QRawFont my_raw_font(QString("cmmi10.ttf"), _pixel_size, QFont::PreferDefaultHinting);
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_ALPHA_GLYPH_INDEX, QPointF(20,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_BETA_GLYPH_INDEX, QPointF(40,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_GAMMA_GLYPH_INDEX, QPointF(60,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_DELTA_GLYPH_INDEX, QPointF(80,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_EPSILON_GLYPH_INDEX, QPointF(100,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_VAREPSILON_GLYPH_INDEX, QPointF(120,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_ZETA_GLYPH_INDEX, QPointF(140,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_ETA_GLYPH_INDEX, QPointF(160,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_THETA_GLYPH_INDEX, QPointF(180,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_VARTHETA_GLYPH_INDEX, QPointF(200,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_IOTA_GLYPH_INDEX, QPointF(220,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_KAPPA_GLYPH_INDEX, QPointF(240,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_LAMBDA_GLYPH_INDEX, QPointF(260,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_MU_GLYPH_INDEX, QPointF(280,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_NU_GLYPH_INDEX, QPointF(300,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_XI_GLYPH_INDEX, QPointF(320,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_OMICRON_GLYPH_INDEX, QPointF(340,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_PI_GLYPH_INDEX, QPointF(360,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_RHO_GLYPH_INDEX, QPointF(380,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_VARRHO_GLYPH_INDEX, QPointF(400,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_SIGMA_GLYPH_INDEX, QPointF(420,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_TAU_GLYPH_INDEX, QPointF(440,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_YPSILON_GLYPH_INDEX, QPointF(460,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_PHI_GLYPH_INDEX, QPointF(480,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_VARPHI_GLYPH_INDEX, QPointF(500,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_CHI_GLYPH_INDEX, QPointF(520,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_PSI_GLYPH_INDEX, QPointF(540,110));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_OMEGA_GLYPH_INDEX, QPointF(560,110));

	_pixel_size = 16;
        QRawFont my_raw_font16(QString("cmmi10.ttf"), _pixel_size, QFont::PreferDefaultHinting);
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_ALPHA_GLYPH_INDEX, QPointF(20,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_BETA_GLYPH_INDEX, QPointF(40,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_GAMMA_GLYPH_INDEX, QPointF(60,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_DELTA_GLYPH_INDEX, QPointF(80,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_EPSILON_GLYPH_INDEX, QPointF(100,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_VAREPSILON_GLYPH_INDEX, QPointF(120,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_ZETA_GLYPH_INDEX, QPointF(140,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_ETA_GLYPH_INDEX, QPointF(160,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_THETA_GLYPH_INDEX, QPointF(180,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_VARTHETA_GLYPH_INDEX, QPointF(200,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_IOTA_GLYPH_INDEX, QPointF(220,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_KAPPA_GLYPH_INDEX, QPointF(240,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_LAMBDA_GLYPH_INDEX, QPointF(260,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_MU_GLYPH_INDEX, QPointF(280,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_NU_GLYPH_INDEX, QPointF(300,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_XI_GLYPH_INDEX, QPointF(320,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_OMICRON_GLYPH_INDEX, QPointF(340,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_PI_GLYPH_INDEX, QPointF(360,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_RHO_GLYPH_INDEX, QPointF(380,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_VARRHO_GLYPH_INDEX, QPointF(400,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_SIGMA_GLYPH_INDEX, QPointF(420,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_TAU_GLYPH_INDEX, QPointF(440,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_YPSILON_GLYPH_INDEX, QPointF(460,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_PHI_GLYPH_INDEX, QPointF(480,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_VARPHI_GLYPH_INDEX, QPointF(500,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_CHI_GLYPH_INDEX, QPointF(520,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_PSI_GLYPH_INDEX, QPointF(540,140));
        draw_glyph(my_painter, my_raw_font16, TTF_CMMI10_SMALL_OMEGA_GLYPH_INDEX, QPointF(560,140));


        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_DOT_GLYPH_INDEX, QPointF(20,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_COMMA_GLYPH_INDEX, QPointF(40,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_LEFT_ANGLE_BRACKET_GLYPH_INDEX, QPointF(60,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_RIGHT_ANGLE_BRACKET_GLYPH_INDEX, QPointF(80,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SLASH_GLYPH_INDEX, QPointF(100,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_PARTIAL_GLYPH_INDEX, QPointF(120,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_GAMMA_GLYPH_INDEX, QPointF(140,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_DELTA_GLYPH_INDEX, QPointF(160,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_THETA_GLYPH_INDEX, QPointF(180,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_LAMBDA_GLYPH_INDEX, QPointF(200,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_XI_GLYPH_INDEX, QPointF(220,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_PI_GLYPH_INDEX, QPointF(240,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_SIGMA_GLYPH_INDEX, QPointF(260,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_YPSILON_GLYPH_INDEX, QPointF(280,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_PHI_GLYPH_INDEX, QPointF(300,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_PSI_GLYPH_INDEX, QPointF(320,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_OMEGA_GLYPH_INDEX, QPointF(340,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_VECTOR_GLYPH_INDEX, QPointF(360,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_STAR_GLYPH_INDEX, QPointF(380,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_LEFT_UPPER_HALF_ARROW_GLYPH_INDEX, QPointF(400,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_LEFT_LOWER_HALF_ARROW_GLYPH_INDEX, QPointF(420,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_RIGHT_UPPER_HALF_ARROW_GLYPH_INDEX, QPointF(440,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_RIGHT_LOWER_HALF_ARROW_GLYPH_INDEX, QPointF(460,170));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_HASH_SIGN_GLYPH_INDEX, QPointF(480,170));

        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_A_GLYPH_INDEX, QPointF(20,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_B_GLYPH_INDEX, QPointF(40,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_C_GLYPH_INDEX, QPointF(60,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_D_GLYPH_INDEX, QPointF(80,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_E_GLYPH_INDEX, QPointF(100,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_F_GLYPH_INDEX, QPointF(120,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_G_GLYPH_INDEX, QPointF(140,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_H_GLYPH_INDEX, QPointF(160,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_I_GLYPH_INDEX, QPointF(180,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_J_GLYPH_INDEX, QPointF(200,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_K_GLYPH_INDEX, QPointF(220,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_L_GLYPH_INDEX, QPointF(240,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_M_GLYPH_INDEX, QPointF(260,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_N_GLYPH_INDEX, QPointF(280,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_O_GLYPH_INDEX, QPointF(300,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_P_GLYPH_INDEX, QPointF(320,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_Q_GLYPH_INDEX, QPointF(340,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_R_GLYPH_INDEX, QPointF(360,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_S_GLYPH_INDEX, QPointF(380,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_T_GLYPH_INDEX, QPointF(400,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_U_GLYPH_INDEX, QPointF(420,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_V_GLYPH_INDEX, QPointF(440,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_W_GLYPH_INDEX, QPointF(460,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_X_GLYPH_INDEX, QPointF(480,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_Y_GLYPH_INDEX, QPointF(500,200));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_CAPITAL_Z_GLYPH_INDEX, QPointF(520,200));

        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_A_GLYPH_INDEX, QPointF(20,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_B_GLYPH_INDEX, QPointF(40,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_C_GLYPH_INDEX, QPointF(60,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_D_GLYPH_INDEX, QPointF(80,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_E_GLYPH_INDEX, QPointF(100,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_F_GLYPH_INDEX, QPointF(120,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_G_GLYPH_INDEX, QPointF(140,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_H_GLYPH_INDEX, QPointF(160,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_I_GLYPH_INDEX, QPointF(180,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_J_GLYPH_INDEX, QPointF(200,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_K_GLYPH_INDEX, QPointF(220,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_L_GLYPH_INDEX, QPointF(240,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_VARL_GLYPH_INDEX, QPointF(260,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_M_GLYPH_INDEX, QPointF(280,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_N_GLYPH_INDEX, QPointF(300,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_O_GLYPH_INDEX, QPointF(320,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_P_GLYPH_INDEX, QPointF(340,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_Q_GLYPH_INDEX, QPointF(360,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_R_GLYPH_INDEX, QPointF(380,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_S_GLYPH_INDEX, QPointF(400,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_T_GLYPH_INDEX, QPointF(420,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_U_GLYPH_INDEX, QPointF(440,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_V_GLYPH_INDEX, QPointF(460,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_W_GLYPH_INDEX, QPointF(480,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_X_GLYPH_INDEX, QPointF(500,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_Y_GLYPH_INDEX, QPointF(520,260));
        draw_glyph(my_painter, my_raw_font, TTF_CMMI10_SMALL_Z_GLYPH_INDEX, QPointF(540,260));

        QRawFont cmsy10_raw_font(QString("cmsy10.ttf"), _pixel_size, QFont::PreferDefaultHinting);
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_RIGHT_SIMPLE_ARROW_GLYPH_INDEX, QPointF(20,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_LEFT_SIMPLE_ARROW_GLYPH_INDEX, QPointF(40,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_UPPER_SIMPLE_ARROW_GLYPH_INDEX, QPointF(60,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_LOWER_SIMPLE_ARROW_GLYPH_INDEX, QPointF(80,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_LEFTRIGHT_SIMPLE_ARROW_GLYPH_INDEX, QPointF(100,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_RIGHT_UPPER_SIMPLE_ARROW_GLYPH_INDEX, QPointF(120,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_RIGHT_LOWER_SIMPLE_ARROW_GLYPH_INDEX, QPointF(140,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_APPROX_GLYPH_INDEX, QPointF(160,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_LEFT_IMPLY_GLYPH_INDEX, QPointF(180,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_RIGHT_IMPLY_GLYPH_INDEX, QPointF(200,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_UPPER_IMPLY_GLYPH_INDEX, QPointF(220,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_LOWER_IMPLY_GLYPH_INDEX, QPointF(240,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_LEFT_RIGHT_IMPLY_GLYPH_INDEX, QPointF(260,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_UPPER_LOWER_IMPLY_GLYPH_INDEX, QPointF(280,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_INFINITY_GLYPH_INDEX, QPointF(300,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_IN_GLYPH_INDEX, QPointF(320,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_REVERSE_IN_GLYPH_INDEX, QPointF(340,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_UPPER_RECTANGLE_GLYPH_INDEX, QPointF(360,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_LOWER_RECTANGLE_GLYPH_INDEX, QPointF(380,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_SLASH_GLYPH_INDEX, QPointF(400,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_FORALL_GLYPH_INDEX, QPointF(420,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_EXISTS_GLYPH_INDEX, QPointF(440,320));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_EMPTY_GLYPH_INDEX, QPointF(460,320));

	draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_A_GLYPH_INDEX, QPointF(20,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_B_GLYPH_INDEX, QPointF(40,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_C_GLYPH_INDEX, QPointF(60,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_D_GLYPH_INDEX, QPointF(80,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_E_GLYPH_INDEX, QPointF(100,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_F_GLYPH_INDEX, QPointF(120,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_G_GLYPH_INDEX, QPointF(140,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_H_GLYPH_INDEX, QPointF(160,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_I_GLYPH_INDEX, QPointF(180,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_J_GLYPH_INDEX, QPointF(200,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_K_GLYPH_INDEX, QPointF(220,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_L_GLYPH_INDEX, QPointF(240,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_M_GLYPH_INDEX, QPointF(260,380));
	draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_N_GLYPH_INDEX, QPointF(280,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_O_GLYPH_INDEX, QPointF(300,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_P_GLYPH_INDEX, QPointF(320,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_Q_GLYPH_INDEX, QPointF(340,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_R_GLYPH_INDEX, QPointF(360,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_S_GLYPH_INDEX, QPointF(380,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_T_GLYPH_INDEX, QPointF(400,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_U_GLYPH_INDEX, QPointF(420,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_V_GLYPH_INDEX, QPointF(440,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_W_GLYPH_INDEX, QPointF(460,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_X_GLYPH_INDEX, QPointF(480,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_Y_GLYPH_INDEX, QPointF(500,380));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MATHCAL_Z_GLYPH_INDEX, QPointF(520,380));


        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_UNION_GLYPH_INDEX, QPointF(20,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_PENETRATION_GLYPH_INDEX, QPointF(40,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_UNION_PLUS_GLYPH_INDEX, QPointF(60,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_WEDGE_GLYPH_INDEX, QPointF(80,460));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_VEE_GLYPH_INDEX, QPointF(100,460));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_LEFT_FLOOR_GLYPH_INDEX, QPointF(120,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_RIGHT_FLOOR_GLYPH_INDEX, QPointF(140,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_LEFT_CEIL_GLYPH_INDEX, QPointF(160,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_RIGHT_CEIL_GLYPH_INDEX, QPointF(180,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_LEFT_BRACE_GLYPH_INDEX, QPointF(200,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_RIGHT_BRACE_GLYPH_INDEX, QPointF(220,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_LEFT_ANGLE_BRACKET_GLYPH_INDEX, QPointF(240,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_RIGHT_ANGLE_BRACKET_GLYPH_INDEX, QPointF(260,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_VERTICAL_LINE_GLYPH_INDEX, QPointF(260,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_VERTICAL_DOUBLE_LINE_GLYPH_INDEX, QPointF(280,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_BACK_SLASH_GLYPH_INDEX, QPointF(300,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_SQUARE_ROOT_GLYPH_INDEX, QPointF(320,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_REVERSED_SIGMA_GLYPH_INDEX, QPointF(340,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_GRAD_GLYPH_INDEX, QPointF(360,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_INTEGRAL_GLYPH_INDEX, QPointF(380,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_LOWER_SQUARED_CUP_GLYPH_INDEX, QPointF(400,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_UPPER_SQUARED_CAP_GLYPH_INDEX, QPointF(420,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_SQUARED_SUBSET_OR_EQUAL_GLYPH_INDEX, QPointF(440,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_SQUARED_SUPERSET_OR_EQUAL_GLYPH_INDEX, QPointF(460,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_PARAGRAPH_GLYPH_INDEX, QPointF(480,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_DAGGER_GLYPH_INDEX, QPointF(500,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_DOUBLE_DAGGER_GLYPH_INDEX, QPointF(520,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_UNDERSCORE_GLYPH_INDEX, QPointF(540,440));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_CDOT_GLYPH_INDEX, QPointF(560,440));

        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_TIMES_GLYPH_INDEX, QPointF(20,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_AST_GLYPH_INDEX, QPointF(40,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_DIVISION_GLYPH_INDEX, QPointF(60,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_DIAMOND_GLYPH_INDEX, QPointF(80,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_PLUS_MINUS_GLYPH_INDEX, QPointF(100,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_MINUS_PLUS_GLYPH_INDEX, QPointF(120,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_O_PLUS_GLYPH_INDEX, QPointF(140,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_O_MINUS_GLYPH_INDEX, QPointF(160,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_O_TIMES_GLYPH_INDEX, QPointF(180,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_O_SLASH_GLYPH_INDEX, QPointF(200,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_O_DOT_GLYPH_INDEX, QPointF(220,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_BIGCIRC_GLYPH_INDEX, QPointF(240,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_CIRC_GLYPH_INDEX, QPointF(260,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_BULLET_GLYPH_INDEX, QPointF(280,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_EQUIV_GLYPH_INDEX, QPointF(300,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_SUBSETEQ_GLYPH_INDEX, QPointF(320,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_SUPSETEQ_GLYPH_INDEX, QPointF(340,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_LESS_OR_EQUAL_GLYPH_INDEX, QPointF(360,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_BIGGER_OR_EQUAL_GLYPH_INDEX, QPointF(380,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_SIMILAR_GLYPH_INDEX, QPointF(400,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_APPROXIMATE_GLYPH_INDEX, QPointF(420,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_SUBSET_GLYPH_INDEX, QPointF(440,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_SUPSET_GLYPH_INDEX, QPointF(480,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_DOUBLE_LESSER_GLYPH_INDEX, QPointF(500,500));
        draw_glyph(my_painter, cmsy10_raw_font, TTF_CMSY10_DOUBLE_GREATER_GLYPH_INDEX, QPointF(520,500));
        //draw_glyph(my_painter, cmsy10_raw_font, , QPointF(360,500));


	my_pixmap.save("result.png");

	return 0;
}
