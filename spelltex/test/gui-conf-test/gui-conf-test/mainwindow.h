#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsPixmapItem>
#include <QGraphicsView>
#include <QFont>
#include <QPixmap>
#include <QTreeWidgetItem>
//#include "controller.h"
//#include "SpellTeXView.h"
#include "AtomTreeMVCModel.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow, public ISingletonSubscriber
{
    Q_OBJECT

public:
    MainWindow(AtomTreeMVCModel *SecondModel, AtomTreeMVCModel *ThirdModel, AtomTreeMVCModel *PositionModel, QWidget *parent = nullptr);
    ~MainWindow();

    //SpellTeXController* spelltex_controller;
    void CheckForUpdate();
    void UpdatedPageDimensions();

private slots:
    void on_cmb_font_currentIndexChanged(int index);

    void on_cmb_font_size_currentIndexChanged(int index);

    void on_cmb_zoom_currentIndexChanged(int index);

    void on_btn_log_clicked();

    void on_edt_latex_source_textChanged();

    void on_actionExit_triggered();

    void on_edt_page_width_textChanged(const QString &arg1);

    void on_edt_page_height_textChanged(const QString &arg1);

    void on_edt_page_top_margin_textChanged(const QString &arg1);

    void on_edt_page_left_margin_textChanged(const QString &arg1);

    void on_edt_page_bottom_margin_textChanged(const QString &arg1);

    void on_edt_page_right_margin_textChanged(const QString &arg1);

public slots:
    void ShowNewPixmap();
    void UpdateDebugRectangle(const QItemSelection &selected, const QItemSelection &deselected); // slot for SIGNAL selectionChanged()


private:
    Ui::MainWindow *ui;
    AtomTreeMVCModel *SecondSpellTeXModel;
    AtomTreeMVCModel *ThirdSpellTeXModel;
    QGraphicsScene *scene;
    QGraphicsView * gview;
    int default_font_index; // position of default font in this window's a combo box

    // debug rectangles
    std::vector<spelltex::Atom*> atoms_with_debug_rects;
    QPixmap* pixmap_buffer; // buffer used to combine original pixmap from SpellTeX and debug rectangles

};
#endif // MAINWINDOW_H
