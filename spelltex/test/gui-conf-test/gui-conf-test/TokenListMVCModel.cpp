#include <QApplication>
#include "TokenListMVCModel.h"


TokenListMVCModel::TokenListMVCModel(SpellTeXSingleton* SpellTeXSingleton){
    // expects existing and valid atom tree from spelltex
    //tree_root_item = atom_tree_root;
    // initialize object pointers with variables
    Singleton = SpellTeXSingleton;
    TokenList = Singleton->GetTokenList();
}


TokenListMVCModel::~TokenListMVCModel(){
    // TODO cleanup
}


QVariant TokenListMVCModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    int row = index.row();
    int counter = 0;
    for(std::list<spelltex::token>::iterator it = TokenList->begin(); it != TokenList->end(); ++it){
        if(counter == row){
            if(index.column() == 0)
                return QString::fromStdString(cat_to_str(it->cat));
            if(index.column() == 1)
                return QString::fromStdString(it->chars);
        }
        counter++;
    }

    return QVariant();

}


Qt::ItemFlags TokenListMVCModel::flags(const QModelIndex &index) const {
    // inform view that model is read-only
    if (!index.isValid())
        return Qt::NoItemFlags;

    return QAbstractItemModel::flags(index);
}


QVariant TokenListMVCModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section == 0)
        return QString("Category");
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section == 1)
        return QString("Chars");

    return QVariant();

}


int TokenListMVCModel::rowCount(const QModelIndex &parent) const {
    // returns number of child items for the parent, specified by the provided index
    // simple list does NOT have any child (unlike tree), thus return 0
    // root item shall return number of items in the list
    // return 0 recommended by Qt documentation at https://doc.qt.io/qt-6/qabstractitemmodel.html#rowCount

    if(parent.isValid())
        return 0;

    return TokenList->size();
}


int TokenListMVCModel::columnCount(const QModelIndex &parent) const {
    return 2; // first column contains token category, second column token character
}


void TokenListMVCModel::CheckForUpdate(){
    beginResetModel();
    TokenList = Singleton->GetTokenList();
    endResetModel();
}
