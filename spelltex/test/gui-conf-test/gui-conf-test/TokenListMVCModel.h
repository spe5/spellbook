#ifndef TOKENLISTMVCMODEL_H
#define TOKENLISTMVCMODEL_H

#include <QApplication>
#include <QAbstractTableModel>
#include "SpellTeXSingleton.h"
#include "ISingletonSubscriber.h"

class TokenListMVCModel : public QAbstractTableModel, public ISingletonSubscriber
{
    Q_OBJECT

public:
    explicit TokenListMVCModel(SpellTeXSingleton* SpellTeXSingleton);
    ~TokenListMVCModel();

    QVariant data(const QModelIndex &index, int role) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    //QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    //QModelIndex parent(const QModelIndex &index) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    void CheckForUpdate() override; // used by singleton to inform subscribers about its data change

private:
    SpellTeXSingleton* Singleton;
    std::list<spelltex::token>* TokenList;

};

#endif // TOKENLISTMVCMODEL_H
