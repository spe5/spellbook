#include <iomanip>
#include <sstream>

#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QDir>
#include <QFileDialog>

#include "mainwindow.h"
#include "./ui_mainwindow.h"

void MainWindow::UpdateDebugRectangle(const QItemSelection &selected, const QItemSelection &deselected){
    // slot for SIGNAL selectionChanged()
    // 1. get Atom IDs from selected and deselected items, from different data models (atom trees)
    QModelIndexList current_selection = selected.indexes();
    for(QList<QModelIndex>::iterator it = current_selection.begin(); it != current_selection.end(); it++){
        spelltex::Atom* selected_atom = static_cast<spelltex::Atom*>(it->internalPointer());
        // check if selected atom already in list
        std::string selected_id = selected_atom->_id;
        bool found = false;
        for(std::vector<spelltex::Atom*>::iterator it = atoms_with_debug_rects.begin(); it != atoms_with_debug_rects.end(); it++){
            if((*it)->_id.compare(selected_id) == 0)
                found = true;
        }
        // add selected atom, if its NOT in the list yet
        if(!found)
            atoms_with_debug_rects.push_back(selected_atom);
    }

    QModelIndexList current_deselection = deselected.indexes();
    for(QList<QModelIndex>::iterator it = current_deselection.begin(); it != current_deselection.end(); it++){
        spelltex::Atom* deselected_atom = static_cast<spelltex::Atom*>(it->internalPointer());
        // check if deselected atom already NOT in list
        std::string deselected_id = deselected_atom->_id;
        bool found = false;
        for(std::vector<spelltex::Atom*>::iterator it = atoms_with_debug_rects.begin(); it != atoms_with_debug_rects.end(); it++){
            if((*it)->_id.compare(deselected_id) == 0){
                atoms_with_debug_rects.erase(it);
                break;
            }
        }
    }

    ShowNewPixmap();
}

void MainWindow::ShowNewPixmap(){
    scene->clear();
    delete pixmap_buffer;
    pixmap_buffer = new QPixmap(SecondSpellTeXModel->GetLatexPixmap());
    QPainter painter(pixmap_buffer);

    // add debug rectangle if atom selected in last model (Position)
    for(std::vector<spelltex::Atom*>::iterator it = atoms_with_debug_rects.begin(); it != atoms_with_debug_rects.end(); it++){
        QPointF upper_left_top((*it)->deb_rect_top_left_x, (*it)->deb_rect_top_left_y);
        QPointF upper_right_bot((*it)->deb_rect_mid_right_x, (*it)->deb_rect_mid_right_y);
        QPointF lower_left_top((*it)->deb_rect_mid_left_x, (*it)->deb_rect_mid_left_y);
        QPointF lower_right_bot((*it)->deb_rect_bot_right_x, (*it)->deb_rect_bot_right_y);

        QRectF upper_rectangle(upper_left_top, upper_right_bot);
        QRectF lower_rectangle(lower_left_top, lower_right_bot);

        QPen current_pen = painter.pen();
        QBrush current_brush = current_pen.brush();
        current_brush.setColor(*(*it)->_debug_color);
        current_pen.setBrush(current_brush);
        painter.setPen(current_pen);

        painter.drawRect(upper_rectangle);
        painter.drawRect(lower_rectangle);
    }

    scene->addPixmap(*pixmap_buffer);
}


MainWindow::MainWindow(AtomTreeMVCModel *SecondModel, AtomTreeMVCModel *ThirdModel, AtomTreeMVCModel *PositionModel, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    SecondSpellTeXModel = SecondModel;
    ThirdSpellTeXModel = ThirdModel;
    //ui->edt_log->setText(abs_log_filename);

    // initialize pixmap holding SpellTeX rendered data
    gview = new QGraphicsView();
    ui->qhbl_spelltex_data->insertWidget(0, gview);
    scene = new QGraphicsScene();
    gview->setScene(scene);
    pixmap_buffer = new QPixmap(SecondSpellTeXModel->GetLatexPixmap());

    connect(SecondModel, &QAbstractItemModel::modelReset, this, &MainWindow::ShowNewPixmap);

    //initialize_font_options(controller);
    //initialize_pixel_size_options(controller);
    //initialize_zoom_options(controller);
    QString buffer;
    SecondModel->GetLatexSource(buffer);
    ui->edt_latex_source->setPlainText(buffer);

    int default_page_width = 0;
    int default_page_height = 0;
    int default_page_top_margin = 0;
    int default_page_left_margin = 0;
    int default_page_bottom_margin = 0;
    int default_page_right_margin = 0;

    SecondModel->GetDefaultPageDimensions(default_page_width, default_page_height, default_page_top_margin, default_page_left_margin, default_page_bottom_margin, default_page_right_margin);
    ui->edt_page_width->setText(QString::number(default_page_width));
    ui->edt_page_height->setText(QString::number(default_page_height));
    ui->edt_page_top_margin->setText(QString::number(default_page_top_margin));
    ui->edt_page_left_margin->setText(QString::number(default_page_left_margin));
    ui->edt_page_bottom_margin->setText(QString::number(default_page_bottom_margin));
    ui->edt_page_right_margin->setText(QString::number(default_page_right_margin));

}

MainWindow::~MainWindow()
{
    delete ui;
    delete pixmap_buffer;
}



void MainWindow::on_cmb_font_currentIndexChanged(int index)
{
    // update SpellTeX configuration and call its main() function
    //qInfo() << "Changing font to " << spelltex_controller->installed_fonts->at(index);
    //spelltex_model->selected_font_index = index;
    //spelltex_model->update_pixmap();
}


void MainWindow::on_cmb_font_size_currentIndexChanged(int index)
{
    // update SpellTeX configuration and call its main() function
    //qInfo() << "Changing pixel size to " << spelltex_controller->pixel_sizes->at(index);
    //spelltex_controller->selected_pixel_size_index = index;
    //spelltex_controller->update_pixmap();
}


void MainWindow::on_cmb_zoom_currentIndexChanged(int index)
{
    //if(spelltex_controller->zoom_settings.size() <= index)
    //    return;

    //qInfo() << "Changing zoom to " << spelltex_controller->zoom_settings.at(index);
    //spelltex_controller->selected_zoom_index = index;
    //spelltex_controller->update_pixmap();
}


void MainWindow::on_btn_log_clicked()
{
    //QStringList fileNames;
    //QFileDialog dialog(this);
    //dialog.setFileMode(QFileDialog::AnyFile);
    //selected_spelltex_log = QFileDialog::getSaveFileName(this, tr("Open Log File"), selected_spelltex_log, tr("Log Files (*.log)"));
    //dialog.setNameFilter(tr("Log Files (*.log)"));
    //if (dialog.exec())
    //    fileNames = dialog.selectedFiles();
    //qDebug() << "Selected SpellTeX log file = '" << selected_spelltex_log << "'";
}


void MainWindow::on_edt_latex_source_textChanged()
{
    SecondSpellTeXModel->SetLatexSource(ui->edt_latex_source->toPlainText());
}


void MainWindow::on_actionExit_triggered()
{
    QCoreApplication::quit();
}

void MainWindow::UpdatedPageDimensions(){
    int page_width = ui->edt_page_width->text().toInt();
    int page_height = ui->edt_page_height->text().toInt();
    int page_top_margin = ui->edt_page_top_margin->text().toInt();
    int page_left_margin = ui->edt_page_left_margin->text().toInt();
    int page_bottom_margin = ui->edt_page_bottom_margin->text().toInt();
    int page_right_margin = ui->edt_page_right_margin->text().toInt();
    SecondSpellTeXModel->SetPageDimensions(page_width, page_height, page_top_margin, page_left_margin, page_bottom_margin, page_right_margin);
    SecondSpellTeXModel->RunSpellTeX();
}

void MainWindow::on_edt_page_width_textChanged(const QString &arg1)
{
    UpdatedPageDimensions();
}

void MainWindow::CheckForUpdate(){
    atoms_with_debug_rects.clear();
    ShowNewPixmap();
}

void MainWindow::on_edt_page_height_textChanged(const QString &arg1)
{
    UpdatedPageDimensions();
}


void MainWindow::on_edt_page_top_margin_textChanged(const QString &arg1)
{
    UpdatedPageDimensions();
}


void MainWindow::on_edt_page_left_margin_textChanged(const QString &arg1)
{
    UpdatedPageDimensions();
}


void MainWindow::on_edt_page_bottom_margin_textChanged(const QString &arg1)
{
    UpdatedPageDimensions();
}


void MainWindow::on_edt_page_right_margin_textChanged(const QString &arg1)
{
    UpdatedPageDimensions();
}

