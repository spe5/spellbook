#include <QApplication>
#include "AtomTreeMVCModel.h"




AtomTreeMVCModel::AtomTreeMVCModel(SpellTeXSingleton* SpellTeXSingleton, const int TreeNumber){
    // expects existing and valid atom tree from spelltex
    //tree_root_item = atom_tree_root;
    // initialize object pointers with variables
    Singleton = SpellTeXSingleton;
    TreeIndex = TreeNumber;
}


AtomTreeMVCModel::~AtomTreeMVCModel(){
    // TODO cleanup

}


QVariant AtomTreeMVCModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    spelltex::Atom *item = static_cast<spelltex::Atom*>(index.internalPointer());
    if(index.column() == 0){
        std::string buffer;
        item->get_str_type(buffer);
        if(item->type == spelltex::ATOM_LETTER)
            buffer += " " + item->_letter;
        return QString::fromStdString(buffer);
    }

    if(index.column() == 1){
        return QString::fromStdString(item->_id);
    }

    if(index.column() == 2){
        return QString::number(item->_offset_x);
    }

    if(index.column() == 3){
        return QString::number(item->_offset_y);
    }

    if(index.column() == 4){
        return QString::number(item->_width);
    }

    if(index.column() == 5){
        return QString::number(item->_partial_height);
    }

    if(index.column() == 6){
        return QString::number(item->_depth);
    }

    if(index.column() == 7){
        return QString::number(item->_scale);
    }

    if(index.column() == 8){
        if(item->_size_calculation_finished == true)
            return "Y";
        else
            return "N";
    }

    return QVariant();

}


Qt::ItemFlags AtomTreeMVCModel::flags(const QModelIndex &index) const {
    // inform view that model is read-only
    if (!index.isValid())
        return Qt::NoItemFlags;

    return QAbstractItemModel::flags(index);
}


QVariant AtomTreeMVCModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section == 0)
        return QString("Atom Type");
    //return tree_root_item->data(section);
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section == 1)
        return QString("Atom ID");
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section == 2)
        return QString("X");
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section == 3)
        return QString("Y");
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section == 4)
        return QString("W");
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section == 5)
        return QString("PH");
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section == 6)
        return QString("D");
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section == 7)
        return QString("S");
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section == 8)
        return QString("X");
    return QVariant();

}


QModelIndex AtomTreeMVCModel::index(int row, int column, const QModelIndex &parent) const {
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    spelltex::Atom *parent_item;

    if (!parent.isValid())
        parent_item = TreeRootItem;
        //return createIndex(row, column, TreeRootItem);
    else
        parent_item = static_cast<spelltex::Atom*>(parent.internalPointer());

    std::list<spelltex::Atom* >::iterator it = parent_item->_childs.begin();
    std::advance(it, row);
    spelltex::Atom *child_item = (*it);
    if (child_item)
        return createIndex(row, column, child_item);
    return QModelIndex();

}


QModelIndex AtomTreeMVCModel::parent(const QModelIndex &index) const {
    // never SHALL return root item, as requested by Qt docs
    // Returns the parent of the model item with the given index.
    // If the item has no parent, an invalid QModelIndex is returned.

    if (!index.isValid())
        return QModelIndex();

    spelltex::Atom *child_item = static_cast<spelltex::Atom*>(index.internalPointer());
    spelltex::Atom *parent_item = child_item->_parent;

    if(parent_item == nullptr)
        return QModelIndex();

    //if (parent_item == TreeRootItem)
    //    return QModelIndex();

    if(parent_item->get_atom_level() == 0)
        return QModelIndex();

    return createIndex(parent_item->get_atom_level(), 0, parent_item);

}


int AtomTreeMVCModel::rowCount(const QModelIndex &parent) const {
    // returns number of child items for the atom, specified by the provided index

    spelltex::Atom *parent_item;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parent_item = TreeRootItem;
    else
        parent_item = static_cast<spelltex::Atom*>(parent.internalPointer());

    return parent_item->_childs.size();

}


int AtomTreeMVCModel::columnCount(const QModelIndex &parent) const {
    //if (parent.isValid())
    //    return static_cast<spelltex::Atom*>(parent.internalPointer())->columnCount();
    //return rootItem->columnCount();
    //return 1; // TODO, for now during PoC development, lets keep it simple - provide only _debug_name as first column
    return 9;
}


void AtomTreeMVCModel::CheckForUpdate(){
    beginResetModel();
    TreeRootItem = Singleton->GetAtomTree(TreeIndex);
    endResetModel();
}

void AtomTreeMVCModel::GetLatexSource(QString &buffer){
    Singleton->GetLatexSource(buffer);
}

void AtomTreeMVCModel::SetLatexSource(const QString NewSource){
    Singleton->SetLatexSource(NewSource);
    Singleton->RunSpellTeX();
}

QPixmap AtomTreeMVCModel::GetLatexPixmap(){
    return Singleton->GetLatexPixmap();
}


QModelIndex AtomTreeMVCModel::find_index_by_atom_id(const QString remote_atom_id, QModelIndex parent_index){
    // search for Atom ID recursively, starting from main atom root

    for(int row = 0; row < rowCount(parent_index); row++){
        spelltex::Atom* current_atom = static_cast<spelltex::Atom*>(index(row, 1, parent_index).internalPointer());
        if(QString::fromStdString(current_atom->_id) == remote_atom_id)
            return index(row, 1, parent_index);
        if(find_index_by_atom_id(remote_atom_id, index(row, 0, parent_index)).isValid())
            return find_index_by_atom_id(remote_atom_id, index(row, 0, parent_index));
    }
    return QModelIndex();
}

void AtomTreeMVCModel::GetDefaultPageDimensions(int &page_width, int &page_height, int &page_top_margin, int &page_left_margin, int &page_bottom_margin, int &page_right_margin){
    Singleton->GetDefaultPageDimensions(page_width, page_height, page_top_margin, page_left_margin, page_bottom_margin, page_right_margin);
}

void AtomTreeMVCModel::SetPageDimensions(int &page_width, int &page_height, int &page_top_margin, int &page_left_margin, int &page_bottom_margin, int &page_right_margin){
    Singleton->SetPageDimensions(page_width, page_height, page_top_margin, page_left_margin, page_bottom_margin, page_right_margin);
}

void AtomTreeMVCModel::RunSpellTeX(){
    Singleton->RunSpellTeX();
}


/*void AtomTreeMVCModel::setupModelData(const QStringList &lines, spelltex::Atom *parent){


}*/


/*void AtomTreeMVCModel::initialize_font_options(){
    int counter = 0;
    int default_font_index = -1;
    for (const auto &item : (*(controller.installed_fonts))){
        ui->cmb_font->addItem(item);
        if(item.compare(controller.default_font->family()) == 0)
            default_font_index = counter;
        counter++;
    }
    if(default_font_index != -1)
        ui->cmb_font->setCurrentIndex(default_font_index);
}*/

/*void AtomTreeMVCModel::initialize_pixel_size_options(){
    int counter = 0;
    int default_index = -1;
    for (const auto &item : (*(controller.pixel_sizes))){
        ui->cmb_font_size->addItem(item); // already installed in list
        if(counter == controller.selected_pixel_size_index)
            default_index = counter;
        counter++;
    }
    if(default_index != -1)
        ui->cmb_font_size->setCurrentIndex(default_index);
}*/

/*void AtomTreeMVCModel::initialize_zoom_options(){
    int counter = 0;
    int default_index = -1;
    for (const auto &item : (controller.zoom_settings)){
        ui->cmb_zoom->addItem(QString::number(item*100) + "%");
        if(counter == controller.selected_zoom_index)
            default_index = counter;
        counter++;
    }
    if(default_index != -1)
        ui->cmb_zoom->setCurrentIndex(default_index);
}*/
