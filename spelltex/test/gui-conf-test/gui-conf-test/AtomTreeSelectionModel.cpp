#include "AtomTreeSelectionModel.h"
#include "atom.h"
#include "AtomTreeMVCModel.h"

void AtomTreeSelectionModel::updateFromAnotherTree(const QItemSelection &selected, const QItemSelection &deselected){
    // slot for SIGNAL selectionChanged()
    // analyze Atom IDs from selections and find them in this view's data model

    // 1. get Atom IDs from selected and deselected items, from different data models (atom trees)
    QModelIndexList current_selection = selected.indexes();
    for(QList<QModelIndex>::iterator it = current_selection.begin(); it != current_selection.end(); it++){
        spelltex::Atom* remote_atom = static_cast<spelltex::Atom*>(it->internalPointer());
        QString remote_atom_id = QString::fromStdString(remote_atom->_id);

        // 2. find the same Atom IDs in current atom tree, incl. its all columns
        QModelIndex index_of_local_atom = static_cast<AtomTreeMVCModel*>(model())->find_index_by_atom_id(remote_atom_id);
        if(!index_of_local_atom.isValid())
            continue;

        // 3. update view by setting selections accordingly
        select(index_of_local_atom, QItemSelectionModel::Select);

        // 4. (de)select whole row, across all columns
        QModelIndex parent = index_of_local_atom.parent();
        for(int column = 0; column < model()->columnCount(); column++){
            select(model()->index(index_of_local_atom.row(), column, parent), QItemSelectionModel::Select);
        }

    }

    // the same for deselected items
    QModelIndexList current_deselection = deselected.indexes();
    for(QList<QModelIndex>::iterator it = current_deselection.begin(); it != current_deselection.end(); it++){
        spelltex::Atom* remote_atom = static_cast<spelltex::Atom*>(it->internalPointer());
        QString remote_atom_id = QString::fromStdString(remote_atom->_id);

        // 2. find the same Atom IDs in current atom tree, incl. its all columns
        QModelIndex index_of_local_atom = static_cast<AtomTreeMVCModel*>(model())->find_index_by_atom_id(remote_atom_id);
        if(!index_of_local_atom.isValid())
            continue;

        // 3. update view by setting selections accordingly
        select(index_of_local_atom, QItemSelectionModel::Deselect);

        // 4. (de)select whole row, across all columns
        QModelIndex parent = index_of_local_atom.parent();
        for(int column = 0; column < model()->columnCount(); column++){
            select(model()->index(index_of_local_atom.row(), column, parent), QItemSelectionModel::Deselect);
        }
    }
}
