#ifndef TREELOGWINDOW_H
#define TREELOGWINDOW_H

#include <QMainWindow>
#include <QTreeWidget>
#include <QTableWidget>
//#include "controller.h"
#include "AtomTreeMVCModel.h"
#include "TokenListMVCModel.h"
//#include "AtomTreeSelectionModel.h"
#include "mainwindow.h"

namespace Ui {
class TreeLogWindow;
class MainWindow;
}

class TreeLogWindow : public QMainWindow, public ISingletonSubscriber
{
    Q_OBJECT

public:
    explicit TreeLogWindow(TokenListMVCModel *TokenModel, AtomTreeMVCModel *SecondModel, AtomTreeMVCModel *ThirdModel, AtomTreeMVCModel *ScaleModel, AtomTreeMVCModel *LayoutModel, AtomTreeMVCModel *PositionModel, AtomTreeMVCModel *DrawnModel, MainWindow* main_window, QWidget *parent = nullptr);
    ~TreeLogWindow();
    //SpellTeXController* spelltex_controller;

    //void update_tree_log_window(std::vector<spelltex::Atom*> atom_trees);
    int update_atom_tree(spelltex::Atom* atom_tree, QTreeWidget* widget);

    //void update_token_list(spelltex::SpellTeX* latex_interpret, QTableWidget* token_table);
    //void update_token_list(spelltex::SpellTeX* latex_interpret);

    void SetClassicColumnWidths(QTreeView* view);
    void SetClassicTokenColumnWidths(QTableView* view);
    void CheckForUpdate() override;

private slots:
    void on_actionExit_triggered();

public slots:
    void ExpandTrees();
    //void on_left_tree_widget_2nd_itemSelectionChanged();

private:
    Ui::TreeLogWindow *ui;
    //std::vector<spelltex::Atom*> atom_trees_copy;
    TokenListMVCModel *TokenSpellTeXModel;
    AtomTreeMVCModel *SecondSpellTeXModel;
    AtomTreeMVCModel *ThirdSpellTeXModel;
    AtomTreeMVCModel *ScaleSpellTeXModel;
    AtomTreeMVCModel *LayoutSpellTeXModel;
    AtomTreeMVCModel *PositionSpellTeXModel;
    AtomTreeMVCModel *DrawnSpellTeXModel;

    //AtomTreeSelectionModel *atom_tree_2nd_selection;
    //AtomTreeSelectionModel *atom_tree_3rd_selection;
};

#endif // TREELOGWINDOW_H
