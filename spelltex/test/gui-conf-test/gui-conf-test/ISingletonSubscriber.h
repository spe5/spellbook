#ifndef ISINGLETONSUBSCRIBER_H
#define ISINGLETONSUBSCRIBER_H

class ISingletonSubscriber{
public:
    void virtual CheckForUpdate() = 0;

};

#endif // ISINGLETONSUBSCRIBER_H
