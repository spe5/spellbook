#include <QApplication>
#include <QLocale>
#include <QTranslator>
#include <QtLogging>
#include <QWindow>
//#include "controller.h"
#include "mainwindow.h"
#include "treelogwindow.h"
#include "SpellTeXSingleton.h"
#include "AtomTreeMVCModel.h"
#include "TokenListMVCModel.h"

#define SPELLTEX_ATOM_TREE_FROM_TOKENS_INDEX 0
#define SPELLTEX_ATOM_TREE_MACROS_EXPANDED_INDEX 1
#define SPELLTEX_ATOM_TREE_SCALE_INDEX 2
#define SPELLTEX_ATOM_TREE_LAYOUT_INDEX 3
#define SPELLTEX_ATOM_TREE_POSITION_INDEX 4
#define SPELLTEX_ATOM_TREE_DRAWN_INDEX 5

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "gui-conf-test_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            a.installTranslator(&translator);
            break;
        }
    }

    SpellTeXSingleton Singleton; // holds all-in-one SpellTeX-related data, single source-of-truth regarding SpellTeX

    TokenListMVCModel TokenModel(&Singleton);
    AtomTreeMVCModel SecondModel(&Singleton, SPELLTEX_ATOM_TREE_FROM_TOKENS_INDEX);
    AtomTreeMVCModel ThirdModel(&Singleton, SPELLTEX_ATOM_TREE_MACROS_EXPANDED_INDEX);
    AtomTreeMVCModel ScaleModel(&Singleton, SPELLTEX_ATOM_TREE_SCALE_INDEX);
    AtomTreeMVCModel LayoutModel(&Singleton, SPELLTEX_ATOM_TREE_LAYOUT_INDEX);
    AtomTreeMVCModel PositionModel(&Singleton, SPELLTEX_ATOM_TREE_POSITION_INDEX);
    AtomTreeMVCModel DrawnModel(&Singleton, SPELLTEX_ATOM_TREE_DRAWN_INDEX);

    Singleton.SubscribeForUpdates(&SecondModel);
    Singleton.SubscribeForUpdates(&ThirdModel);
    Singleton.SubscribeForUpdates(&ScaleModel);
    Singleton.SubscribeForUpdates(&LayoutModel);
    Singleton.SubscribeForUpdates(&PositionModel);
    Singleton.SubscribeForUpdates(&TokenModel);
    Singleton.SubscribeForUpdates(&DrawnModel);

    qDebug() << "Number of detected screens: " << QString::number(qApp->screens().size());

    MainWindow* main_window = new MainWindow(&SecondModel, &ThirdModel, &PositionModel);
    main_window->show();
    main_window->move(1900,800);
    main_window->showFullScreen();

    Singleton.SubscribeForUpdates(main_window);

    TreeLogWindow* tree_log_window = new TreeLogWindow(&TokenModel, &SecondModel, &ThirdModel, &ScaleModel, &LayoutModel, &PositionModel, &DrawnModel, main_window);
    tree_log_window->show();
    tree_log_window->move(0,0);
    tree_log_window->showFullScreen();

    Singleton.SubscribeForUpdates(tree_log_window);

    return a.exec();
}
