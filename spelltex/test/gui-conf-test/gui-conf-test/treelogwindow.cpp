#include "treelogwindow.h"
#include "ui_treelogwindow.h"
#include "AtomTreeSelectionModel.h"

#define ATOM_TYPE_COLUMN_INDEX 0
#define ATOM_ID_COLUMN_INDEX 1
#define ATOM_OFFSET_X_COLUMN_INDEX 2
#define ATOM_OFFSET_Y_COLUMN_INDEX 3
#define ATOM_SCALE_COLUMN_INDEX 4
#define TREE_ID_COLUMN_INDEX 5
#define ALL_CHILD_SIZE_CALCULATION_FINISHED_INDEX 6

#define ATOM_TREE_FROM_TOKENS 0

void TreeLogWindow::ExpandTrees(){
    // New rows have been added to parent.  Make sure parent is fully expanded.
    ui->left_tree_view_2nd->expandAll(); // Recursively(parent);
    ui->right_tree_view_2nd->expandAll();
    ui->left_tree_view_3rd->expandAll();
    ui->right_tree_view_3rd->expandAll();
    ui->left_tree_view_4th->expandAll();
    ui->right_tree_view_4th->expandAll();
    ui->left_tree_view_5th->expandAll();
    ui->right_tree_view_5th->expandAll();
    ui->left_tree_view_6th->expandAll();
    ui->right_tree_view_6th->expandAll();
    ui->left_tree_view_7th->expandAll();
    ui->right_tree_view_7th->expandAll();
}

TreeLogWindow::TreeLogWindow(TokenListMVCModel *TokenModel, AtomTreeMVCModel *SecondModel, AtomTreeMVCModel *ThirdModel, AtomTreeMVCModel *ScaleModel, AtomTreeMVCModel *LayoutModel, AtomTreeMVCModel *PositionModel, AtomTreeMVCModel *DrawnModel, MainWindow *main_window, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TreeLogWindow)
{
    ui->setupUi(this);
    TokenSpellTeXModel = TokenModel;
    SecondSpellTeXModel = SecondModel;
    ThirdSpellTeXModel = ThirdModel;
    ScaleSpellTeXModel = ScaleModel;
    LayoutSpellTeXModel = LayoutModel;
    PositionSpellTeXModel = PositionModel;
    DrawnSpellTeXModel = DrawnModel;

    ui->left_table_view_1th->setModel(TokenModel);
    ui->right_table_view_1th->setModel(TokenModel);
    ui->left_tree_view_2nd->setModel(SecondModel);
    ui->right_tree_view_2nd->setModel(SecondModel);
    ui->left_tree_view_3rd->setModel(ThirdModel);
    ui->right_tree_view_3rd->setModel(ThirdModel);
    ui->left_tree_view_4th->setModel(ScaleModel);
    ui->right_tree_view_4th->setModel(ScaleModel);
    ui->left_tree_view_5th->setModel(LayoutModel);
    ui->right_tree_view_5th->setModel(LayoutModel);
    ui->left_tree_view_6th->setModel(PositionModel);
    ui->right_tree_view_6th->setModel(PositionModel);
    ui->left_tree_view_7th->setModel(DrawnModel);
    ui->right_tree_view_7th->setModel(DrawnModel);

    //connect(TokenModel, &QAbstractItemModel::modelReset, this, &TreeLogWindow::ExpandTrees);
    connect(SecondModel, &QAbstractItemModel::modelReset, this, &TreeLogWindow::ExpandTrees);
    connect(ThirdModel, &QAbstractItemModel::modelReset, this, &TreeLogWindow::ExpandTrees);
    connect(ScaleModel, &QAbstractItemModel::modelReset, this, &TreeLogWindow::ExpandTrees);
    connect(LayoutModel, &QAbstractItemModel::modelReset, this, &TreeLogWindow::ExpandTrees);
    connect(PositionModel, &QAbstractItemModel::modelReset, this, &TreeLogWindow::ExpandTrees);
    connect(DrawnModel, &QAbstractItemModel::modelReset, this, &TreeLogWindow::ExpandTrees);

    QModelIndexList indexes = SecondModel->match(SecondModel->index(0,0), Qt::DisplayRole, "*", -1, Qt::MatchWildcard|Qt::MatchRecursive);
    foreach (QModelIndex index, indexes){
        ui->left_tree_view_2nd->expand(index);
        ui->right_tree_view_2nd->expand(index);
    }

    indexes = ThirdModel->match(ThirdModel->index(0,0), Qt::DisplayRole, "*", -1, Qt::MatchWildcard|Qt::MatchRecursive);
    foreach (QModelIndex index, indexes){
        ui->left_tree_view_3rd->expand(index);
        ui->right_tree_view_3rd->expand(index);
    }

    indexes = ScaleModel->match(ScaleModel->index(0,0), Qt::DisplayRole, "*", -1, Qt::MatchWildcard|Qt::MatchRecursive);
    foreach (QModelIndex index, indexes){
        ui->left_tree_view_4th->expand(index);
        ui->right_tree_view_4th->expand(index);
    }

    indexes = LayoutModel->match(LayoutModel->index(0,0), Qt::DisplayRole, "*", -1, Qt::MatchWildcard|Qt::MatchRecursive);
    foreach (QModelIndex index, indexes){
        ui->left_tree_view_5th->expand(index);
        ui->right_tree_view_5th->expand(index);
    }

    indexes = PositionModel->match(PositionModel->index(0,0), Qt::DisplayRole, "*", -1, Qt::MatchWildcard|Qt::MatchRecursive);
    foreach (QModelIndex index, indexes){
        ui->left_tree_view_6th->expand(index);
        ui->right_tree_view_6th->expand(index);
    }

    indexes = DrawnModel->match(DrawnModel->index(0,0), Qt::DisplayRole, "*", -1, Qt::MatchWildcard|Qt::MatchRecursive);
    foreach (QModelIndex index, indexes){
        ui->left_tree_view_7th->expand(index);
        ui->right_tree_view_7th->expand(index);
    }

    SetClassicTokenColumnWidths(ui->left_table_view_1th);
    SetClassicTokenColumnWidths(ui->right_table_view_1th);

    SetClassicColumnWidths(ui->left_tree_view_2nd);
    SetClassicColumnWidths(ui->right_tree_view_2nd);

    SetClassicColumnWidths(ui->left_tree_view_3rd);
    SetClassicColumnWidths(ui->right_tree_view_3rd);

    SetClassicColumnWidths(ui->left_tree_view_4th);
    SetClassicColumnWidths(ui->right_tree_view_4th);

    SetClassicColumnWidths(ui->left_tree_view_5th);
    SetClassicColumnWidths(ui->right_tree_view_5th);

    SetClassicColumnWidths(ui->left_tree_view_6th);
    SetClassicColumnWidths(ui->right_tree_view_6th);

    SetClassicColumnWidths(ui->left_tree_view_7th);
    SetClassicColumnWidths(ui->right_tree_view_7th);

    AtomTreeSelectionModel* atom_tree_2nd_selection = new AtomTreeSelectionModel();
    atom_tree_2nd_selection->setModel(SecondModel);
    ui->left_tree_view_2nd->setSelectionModel(atom_tree_2nd_selection);
    ui->right_tree_view_2nd->setSelectionModel(atom_tree_2nd_selection);

    AtomTreeSelectionModel* atom_tree_3rd_selection = new AtomTreeSelectionModel();
    atom_tree_3rd_selection->setModel(ThirdModel);
    ui->left_tree_view_3rd->setSelectionModel(atom_tree_3rd_selection);
    ui->right_tree_view_3rd->setSelectionModel(atom_tree_3rd_selection);
    connect(atom_tree_2nd_selection, &QItemSelectionModel::selectionChanged, atom_tree_3rd_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_3rd_selection, &QItemSelectionModel::selectionChanged, atom_tree_2nd_selection, &AtomTreeSelectionModel::updateFromAnotherTree);

    AtomTreeSelectionModel* atom_tree_4th_selection = new AtomTreeSelectionModel();
    atom_tree_4th_selection->setModel(ScaleModel);
    ui->left_tree_view_4th->setSelectionModel(atom_tree_4th_selection);
    ui->right_tree_view_4th->setSelectionModel(atom_tree_4th_selection);
    connect(atom_tree_2nd_selection, &QItemSelectionModel::selectionChanged, atom_tree_4th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_4th_selection, &QItemSelectionModel::selectionChanged, atom_tree_2nd_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_3rd_selection, &QItemSelectionModel::selectionChanged, atom_tree_4th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_4th_selection, &QItemSelectionModel::selectionChanged, atom_tree_3rd_selection, &AtomTreeSelectionModel::updateFromAnotherTree);

    AtomTreeSelectionModel* atom_tree_5th_selection = new AtomTreeSelectionModel();
    atom_tree_5th_selection->setModel(LayoutModel);
    ui->left_tree_view_5th->setSelectionModel(atom_tree_5th_selection);
    ui->right_tree_view_5th->setSelectionModel(atom_tree_5th_selection);
    connect(atom_tree_2nd_selection, &QItemSelectionModel::selectionChanged, atom_tree_5th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_5th_selection, &QItemSelectionModel::selectionChanged, atom_tree_2nd_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_3rd_selection, &QItemSelectionModel::selectionChanged, atom_tree_5th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_5th_selection, &QItemSelectionModel::selectionChanged, atom_tree_3rd_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_4th_selection, &QItemSelectionModel::selectionChanged, atom_tree_5th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_5th_selection, &QItemSelectionModel::selectionChanged, atom_tree_4th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);

    AtomTreeSelectionModel* atom_tree_6th_selection = new AtomTreeSelectionModel();
    atom_tree_6th_selection->setModel(PositionModel);
    ui->left_tree_view_6th->setSelectionModel(atom_tree_6th_selection);
    ui->right_tree_view_6th->setSelectionModel(atom_tree_6th_selection);
    connect(atom_tree_2nd_selection, &QItemSelectionModel::selectionChanged, atom_tree_6th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_6th_selection, &QItemSelectionModel::selectionChanged, atom_tree_2nd_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_3rd_selection, &QItemSelectionModel::selectionChanged, atom_tree_6th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_6th_selection, &QItemSelectionModel::selectionChanged, atom_tree_3rd_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_4th_selection, &QItemSelectionModel::selectionChanged, atom_tree_6th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_6th_selection, &QItemSelectionModel::selectionChanged, atom_tree_4th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_5th_selection, &QItemSelectionModel::selectionChanged, atom_tree_6th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_6th_selection, &QItemSelectionModel::selectionChanged, atom_tree_5th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);

    AtomTreeSelectionModel* atom_tree_7th_selection = new AtomTreeSelectionModel();
    atom_tree_7th_selection->setModel(DrawnModel);
    ui->left_tree_view_7th->setSelectionModel(atom_tree_7th_selection);
    ui->right_tree_view_7th->setSelectionModel(atom_tree_7th_selection);
    connect(atom_tree_2nd_selection, &QItemSelectionModel::selectionChanged, atom_tree_7th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_7th_selection, &QItemSelectionModel::selectionChanged, atom_tree_2nd_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_3rd_selection, &QItemSelectionModel::selectionChanged, atom_tree_7th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_7th_selection, &QItemSelectionModel::selectionChanged, atom_tree_3rd_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_4th_selection, &QItemSelectionModel::selectionChanged, atom_tree_7th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_7th_selection, &QItemSelectionModel::selectionChanged, atom_tree_4th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_5th_selection, &QItemSelectionModel::selectionChanged, atom_tree_7th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_7th_selection, &QItemSelectionModel::selectionChanged, atom_tree_5th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_6th_selection, &QItemSelectionModel::selectionChanged, atom_tree_7th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);
    connect(atom_tree_7th_selection, &QItemSelectionModel::selectionChanged, atom_tree_6th_selection, &AtomTreeSelectionModel::updateFromAnotherTree);


    // draw debug rectangles on pixmap upon selection
    connect(atom_tree_7th_selection, &QItemSelectionModel::selectionChanged, main_window, &MainWindow::UpdateDebugRectangle);

}

TreeLogWindow::~TreeLogWindow()
{
    delete ui;
}

void TreeLogWindow::SetClassicColumnWidths(QTreeView* view){
    view->setColumnWidth(0, 250);
    view->setColumnWidth(1, 150);
    view->setColumnWidth(2, 30);
    view->setColumnWidth(3, 30);
    view->setColumnWidth(4, 30);
    view->setColumnWidth(5, 50);
    view->setColumnWidth(6, 50);
    view->setColumnWidth(7, 50);
}

void TreeLogWindow::SetClassicTokenColumnWidths(QTableView* view){
    view->setColumnWidth(0, 250);
    view->setColumnWidth(1, 150);
}

void TreeLogWindow::on_actionExit_triggered()
{
    QCoreApplication::quit();
}

void TreeLogWindow::CheckForUpdate(){
    // TODO
}

int TreeLogWindow::update_atom_tree(spelltex::Atom* atom_tree, QTreeWidget* widget){
    //widget->clear();
    //atom_tree->reset_iterators();
    //spelltex::Atom* atom_tree_iterator = atom_tree->next();
    //std::vector<std::string> parent_stack; // stack of Atom's _id, parent_stack.size == item_stack.size
    //std::vector<QTreeWidgetItem*> item_stack; // stack of items, corresponding to Atom's _id(s), parent_stack.size == item_stack.size

    /*while(atom_tree_iterator != NULL){
        QTreeWidgetItem * item = new QTreeWidgetItem();
        //prepare_tree_item(atom_tree_iterator, item);
        //add_item_to_tree(atom_tree_iterator, widget, item, parent_stack, item_stack);
        widget->expandItem(item);
        atom_tree_iterator = atom_tree->next();
    }*/

    //widget->header()->resizeSection(ATOM_TYPE_COLUMN_INDEX /*column index*/, 300 /*width*/);
    //widget->header()->resizeSection(ATOM_ID_COLUMN_INDEX /* column index*/, 150 /* width*/);
    //widget->header()->resizeSection(ATOM_OFFSET_X_COLUMN_INDEX /* column index*/, 10 /* width*/);
    //widget->header()->resizeSection(ATOM_OFFSET_Y_COLUMN_INDEX /* column index*/, 10 /* width*/);
    //widget->header()->resizeSection(ATOM_SCALE_COLUMN_INDEX /* column index*/, 10 /* width*/);
    //widget->header()->resizeSection(TREE_ID_COLUMN_INDEX /* column index*/, 10 /* width*/);

    return 0;
}


