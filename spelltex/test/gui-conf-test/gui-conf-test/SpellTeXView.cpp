#include "SpellTeXView.h"
//#include "controller.h"
#include <QDateTime>

#define ATOM_TYPE_COLUMN_INDEX 0
#define ATOM_ID_COLUMN_INDEX 1
#define ATOM_OFFSET_X_COLUMN_INDEX 2
#define ATOM_OFFSET_Y_COLUMN_INDEX 3
#define ATOM_SCALE_COLUMN_INDEX 4

/*void prepare_tree_item(spelltex::Atom* atom_tree_iterator, QTreeWidgetItem* item){
    // requires valid atom tree as first argument and empty buffer as second argument
    // expects item to be part of table with correct columns ATOM_TYPE_COLUMN_INDEX, ATOM_ID_COLUMN_INDEX, etc.
    // fills columns of item with atom-relevant data: ids, debug names, glyph position, scale, etc.
    QString item_text;
    std::string buffer = "Init";
    atom_tree_iterator->get_str_type(buffer);
    item_text = QString::fromStdString(buffer);
    item->setText(ATOM_TYPE_COLUMN_INDEX,item_text);
    QString atom_id_text = QString::fromStdString(atom_tree_iterator->_debug_atom_tree_id + ":" + atom_tree_iterator->_id);
    item->setText(ATOM_ID_COLUMN_INDEX, atom_id_text);
    std::stringstream ss_offset_x;
    std::stringstream ss_offset_y;
    ss_offset_x << std::fixed << std::setprecision(0) << atom_tree_iterator->_offset_x;
    ss_offset_y << std::fixed << std::setprecision(0) << atom_tree_iterator->_offset_y;
    QString atom_offset_x = QString::fromStdString(ss_offset_x.str());
    QString atom_offset_y = QString::fromStdString(ss_offset_y.str());
    item->setText(ATOM_OFFSET_X_COLUMN_INDEX, atom_offset_x);
    item->setText(ATOM_OFFSET_Y_COLUMN_INDEX, atom_offset_y);
    std::stringstream ss_scale;
    ss_scale << std::fixed << std::setprecision(2) << atom_tree_iterator->_scale;
    item->setText(ATOM_SCALE_COLUMN_INDEX, QString::fromStdString(ss_scale.str()));
}*/


/*void add_item_to_tree(spelltex::Atom* atom_tree_iterator, QTreeWidget* widget, QTreeWidgetItem* item, std::vector<std::string> &parent_stack, std::vector<QTreeWidgetItem*>&item_stack){
    // top level item, usually for PAGE ATOM(s)
    if(atom_tree_iterator->_parent == nullptr){
        parent_stack.clear();
        parent_stack.push_back(atom_tree_iterator->_id);
        item_stack.clear();
        item_stack.push_back(item);
        widget->addTopLevelItem(item);
        return;
    }

    // current atom is child of previous atom
    if(parent_stack.back().compare(atom_tree_iterator->_parent->_id) == 0){
        item_stack.back()->addChild(item); // ui->tree_2nd_step->addTopLevelItem(item);
        item_stack.push_back(item);
        parent_stack.push_back(atom_tree_iterator->_id);
        return;
    }

    // current atom is NOT child of previous atom, lets find its parent in stack
    bool found_parent_in_current_stack = false;
    for(int i = 0; i < parent_stack.size(); i++){ // current atom is uncle of previous one
        if(parent_stack[i].compare(atom_tree_iterator->_parent->_id) == 0){ // found parent in current stack
            for(int j = parent_stack.size()-1; j > i; j--){ // remove all parents BEFORE current parent from stack
                parent_stack.pop_back();
                item_stack.pop_back();
            }
            item_stack[i]->addChild(item);
            item_stack.push_back(item);
            parent_stack.push_back(atom_tree_iterator->_id);
            found_parent_in_current_stack = true;
        }
    }

    // current atom is NOT child of previous atom, NOR its parent was found in current stack
    if(found_parent_in_current_stack == false){
        item_stack.clear();
        parent_stack.clear();
        parent_stack.push_back(atom_tree_iterator->_id);
        item_stack.push_back(item);
        widget->addTopLevelItem(item);
    }

}*/


void update_canvas(QGraphicsPixmapItem * latex_image, QPixmap* latex_pixmap, QGraphicsScene *scene, QGraphicsView * gview){
    latex_image = new QGraphicsPixmapItem(*latex_pixmap);
    int imageWidth = latex_image->pixmap().width();
    int imageHeight = latex_image->pixmap().height();
    latex_image->setOffset(- imageWidth / 2, -imageHeight / 2);
    latex_image->setPos(0, 0);
    scene->addItem(latex_image);
    scene->setSceneRect(-imageWidth / 2, -imageHeight / 2, imageWidth, imageHeight);
    gview->setScene(scene);
}


