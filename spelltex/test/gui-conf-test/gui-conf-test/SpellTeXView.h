#ifndef SPELLTEXVIEW_H
#define SPELLTEXVIEW_H

//#include "atom.h"
#include "QTreeWidgetItem"
#include "QGraphicsPixmapItem"
#include "QPixmap"
#include "QGraphicsScene"
#include "QGraphicsView"



//void prepare_tree_item(spelltex::Atom* atom_tree_iterator, QTreeWidgetItem* item);
//void add_item_to_tree(spelltex::Atom* atom_tree_iterator, QTreeWidget* widget, QTreeWidgetItem* item, std::vector<std::string> &parent_stack, std::vector<QTreeWidgetItem*>&item_stack);
void update_canvas(QGraphicsPixmapItem * latex_image, QPixmap* latex_pixmap, QGraphicsScene *scene, QGraphicsView * gview);
void customMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

#endif // SPELLTEXVIEW_H
