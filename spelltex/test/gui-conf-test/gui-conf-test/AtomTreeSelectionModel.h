#ifndef ATOMTREESELECTIONMODEL_H
#define ATOMTREESELECTIONMODEL_H

#include <QItemSelectionModel>
#include <QItemSelection>

class AtomTreeSelectionModel : public QItemSelectionModel {
public slots:
           void updateFromAnotherTree(const QItemSelection &selected, const QItemSelection &deselected); // slot for SIGNAL selectionChanged()
};

#endif // ATOMTREESELECTIONMODEL_H
