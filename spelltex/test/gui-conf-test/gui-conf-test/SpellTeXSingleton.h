#ifndef SPELLTEXSINGLETON_H
#define SPELLTEXSINGLETON_H

#include <QString>
#include <QPixmap>
#include <QAbstractItemModel>
#include "ISingletonSubscriber.h"
#include "spelltex.h"

class SpellTeXSingleton{

public:
    SpellTeXSingleton();
    ~SpellTeXSingleton();

    void SubscribeForUpdates(ISingletonSubscriber* NewSubscriber);
    void RunSpellTeX();
    void SetLatexSource(const QString NewSource);
    void GetLatexSource(QString &buffer);
    spelltex::Atom* GetAtomTree(int TreeIndex);
    std::list<spelltex::token>* GetTokenList();
    QPixmap GetLatexPixmap();
    void GetDefaultPageDimensions(int &page_width, int &page_height, int &page_top_margin, int &page_left_margin, int &page_bottom_margin, int &page_right_margin);
    void SetPageDimensions(int &page_width, int &page_height, int &page_top_margin, int &page_left_margin, int &page_bottom_margin, int &page_right_margin);

    //   void InitializeFontOptions();
    //   void InitializePixelSizeOptions();
    //   void InitializeZoomOptions();

private:
    spelltex::SpellTeX* LatexInterpret;
    QPixmap* LatexPixmap; // main output from SpellTeX
    QString* LatexSource; // main input for SpellTeX
    std::vector<spelltex::Atom*> AtomTrees; // auxiliary debug-aimed output from SpellTeX

    std::vector<ISingletonSubscriber*> Subscribers; // all models, which wants to be updated after SpellTeX execution
    int page_width;
    int page_height;
    int page_top_margin;
    int page_left_margin;
    int page_bottom_margin;
    int page_right_margin;
};

#endif // SPELLTEXSINGLETON_H
