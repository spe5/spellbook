#include <QApplication>
#include "SpellTeXSingleton.h"
#include "ISingletonSubscriber.h"

#define DEFAULT_ZOOM 1.0

SpellTeXSingleton::SpellTeXSingleton(){
    // expects existing and valid atom tree from spelltex
    //tree_root_item = atom_tree_root;
    // initialize object pointers with variables

    page_width = 800; // [pixels]
    page_height = 800; // [pixels]
    page_top_margin = 20; // margin [pixels]
    page_left_margin = 20; // margin [pixels]
    page_bottom_margin = 20; // margin [pixels]
    page_right_margin = 20; // margin [pixels]

    AtomTrees.clear();

    QStringList* pixel_sizes; // textual pixel sizes available for selected font
    int selected_pixel_size_index; // index for pixel_sizes
    //QStringList* installed_fonts; // familiy names of available font names
    //int selected_font_index; // index for installed_fonts list

    LatexInterpret = new spelltex::SpellTeX(qApp);
    LatexSource = new QString("Hello World \\mathcal{W}"); // set default SpellTeX configuration and execute its main() function
    QFont* default_font = new QFont("Ubuntu", 12);
    QFont* selected_font = default_font;
    LatexPixmap = new QPixmap(page_width, page_height);
    LatexPixmap->fill(Qt::black);
    //QGraphicsPixmapItem * latex_image = new QGraphicsPixmapItem();

    LatexInterpret->set_font(*selected_font);
    LatexInterpret->set_zoom(DEFAULT_ZOOM);
    LatexInterpret->set_page_dimensions(page_width, page_height, page_top_margin, page_left_margin, page_bottom_margin, page_right_margin);

    RunSpellTeX();

}

SpellTeXSingleton::~SpellTeXSingleton(){
    // TODO cleanup here
}


void SpellTeXSingleton::RunSpellTeX(){

    AtomTrees.clear();
    LatexInterpret->main(LatexSource->toUtf8().toStdString(), LatexPixmap, AtomTrees);

    for (std::vector<ISingletonSubscriber*>::iterator it = Subscribers.begin() ; it != Subscribers.end(); ++it)
        (*it)->CheckForUpdate();
}

spelltex::Atom* SpellTeXSingleton::GetAtomTree(int TreeIndex){
    return AtomTrees[TreeIndex];
}

std::list<spelltex::token>* SpellTeXSingleton::GetTokenList(){
    return &LatexInterpret->my_tokens;
}

void SpellTeXSingleton::SubscribeForUpdates(ISingletonSubscriber* NewSubscriber){
    Subscribers.push_back(NewSubscriber);
}

void SpellTeXSingleton::GetLatexSource(QString &buffer){
    buffer = *LatexSource;
}

void SpellTeXSingleton::SetLatexSource(const QString NewSource){
    *LatexSource = NewSource;
}

QPixmap SpellTeXSingleton::GetLatexPixmap(){
    return *LatexPixmap;
}

void SpellTeXSingleton::GetDefaultPageDimensions(int &page_width, int &page_height, int &page_top_margin, int &page_left_margin, int &page_bottom_margin, int &page_right_margin){
    LatexInterpret->get_default_page_dimensions(page_width, page_height, page_top_margin, page_left_margin, page_bottom_margin, page_right_margin);
}

void SpellTeXSingleton::SetPageDimensions(int &page_width, int &page_height, int &page_top_margin, int &page_left_margin, int &page_bottom_margin, int &page_right_margin){
    LatexInterpret->set_page_dimensions(page_width, page_height, page_top_margin, page_left_margin, page_bottom_margin, page_right_margin);
}
