#ifndef ATOMTREEMVCMODEL_H
#define ATOMTREEMVCMODEL_H

#include <QApplication>
#include <QAbstractItemModel>
#include "SpellTeXSingleton.h"
#include "ISingletonSubscriber.h"
#include "atom.h"

class AtomTreeMVCModel : public QAbstractItemModel, public ISingletonSubscriber
{
    Q_OBJECT

public:
    explicit AtomTreeMVCModel(SpellTeXSingleton* SpellTeXSingleton, const int TreeNumber);
    ~AtomTreeMVCModel();

    QVariant data(const QModelIndex &index, int role) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    void CheckForUpdate() override; // used by singleton to inform subscribers about its data change
    QModelIndex find_index_by_atom_id(const QString remote_atom_id, QModelIndex parent_index = QModelIndex()); // used by Selection Model to cross-reference Atom ID(s)

    void SetLatexSource(const QString NewSource); // update Singleton's source and re-run SpellTeX
    void GetLatexSource(QString &buffer);
    QPixmap GetLatexPixmap();

    void GetDefaultPageDimensions(int &page_width, int &page_height, int &page_top_margin, int &page_left_margin, int &page_bottom_margin, int &page_right_margin);
    void SetPageDimensions(int &page_width, int &page_height, int &page_top_margin, int &page_left_margin, int &page_bottom_margin, int &page_right_margin);
    void RunSpellTeX();

private:
    SpellTeXSingleton* Singleton;
    spelltex::Atom *TreeRootItem;
    int TreeIndex; // tree idetificator, in range 0 to 6 incl.

};


#endif // ATOMTREEMVCMODEL_H
