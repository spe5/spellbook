#include <iostream>
#include <fstream>
#include <sstream>
#include <QGuiApplication>
#include <QApplication>
#include <QPixmap>
#include <QPainter>
#include <QFile>
#include "../../include/spelltex.h"
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>


#define STEX_SUCCESS 0
#define STEX_GENERAL_ERROR 1
#define STEX_INVALID_SYNTAX_ERROR 2
#define STEX_INPUT_FILE_NOT_EXISTS 3
#define STEX_INPUT_FILE_NOT_UTF8 4
#define DEFAULT_ZOOM 1
#define STEX_IMAGE_SAVE_FAILED 5

void log_rendering_pipeline(std::stringstream &draw_log_stream, const std::string log_filename){
	std::string std_draw_log_name(log_filename);
	QString qt_draw_log_name(std_draw_log_name.c_str());
	QFile draw_log(qt_draw_log_name);
	draw_log.open(QFile::WriteOnly);
	QTextStream draw_logstream(&draw_log);
	draw_logstream << draw_log_stream.str().c_str();
}

void log_tokens(std::list<spelltex::token> &my_tokens, const std::string latex_filename){
	// print the resulting list of tokens
	std::string std_token_log_name(latex_filename + ".01.tk.log");
	QString qt_token_log_name(std_token_log_name.c_str());
	QFile token_log(qt_token_log_name);
	token_log.open(QFile::WriteOnly);
	std::list<spelltex::token>::iterator it = my_tokens.begin();
	QTextStream token_logstream(&token_log);
	while(it != my_tokens.end()){
		token_logstream << "[INFO] (" << (cat_to_str(it->cat)).c_str() << "," << Qt::hex << print_chars(it->cat, it->chars).c_str() << Qt::dec << ")" << Qt::endl;
		it = std::next(it);
	}
}


void log_atom_tree(spelltex::Atom* my_atom_tree, const std::string latex_filename){
	// log atom tree
	std::stringstream atom_log_stream;
	my_atom_tree->print_me(atom_log_stream, false);
	std::string std_atom_log_name(latex_filename);
	QString qt_atom_log_name(std_atom_log_name.c_str());
	QFile atom_log(qt_atom_log_name);
	atom_log.open(QFile::WriteOnly);
	QTextStream atom_logstream(&atom_log);
	atom_logstream << atom_log_stream.str().c_str();
}


struct context{
        bool debug_flag; // true for more verbosity, false otherwise
	std::string tex_filename; // relative file name of input LaTeX source
	bool utf8_tex_file_flag; // true if given .tex file is UTF8, false otherwise
	std::string png_filename; // relative file name of output picture
	std::stringstream latex_stream; // content of .tex file
	QGuiApplication *app; // global object for all Qt-related GUI stuff
	int page_width;
	int page_height;
	int printable_left_top_x;
	int printable_left_top_y;
	int printable_right_bot_x;
	int printable_right_bot_y;
	QPixmap *img;
	double zoom;
	int pixel_size;
};


void set_default_context(int &argc, char* argv[], context &ctx){
	ctx.debug_flag = false;
        ctx.tex_filename = "";
	ctx.utf8_tex_file_flag = false; // NOT true until verified
        ctx.png_filename = "result.png";
	ctx.page_width = 800; // TODO configurable by user using cmd args
	ctx.page_height = 800; // TODO configurable by user using cmd args
	ctx.printable_left_top_x = 20; // TODO configurable by user using cmd args
	ctx.printable_left_top_y = 20; // TODO
	ctx.printable_right_bot_x = 780; // TODO
	ctx.printable_right_bot_y = 780; // TODO
        ctx.app = new QApplication (argc, argv); // initialize Qt first, in order to use all Qt-related stuff ASAP (like qDebug instead std::cout)
	ctx.img = new QPixmap(ctx.page_width, ctx.page_height);
	ctx.zoom = 1.0; // default zoom is 100%
	ctx.pixel_size = 16; // default pixel size of default font
}


void print_context_conditionally(context &ctx){
	if(ctx.debug_flag == false)
		return;

	qDebug() << "[INFO] ctx.debug_flag = " << ctx.debug_flag;
	qDebug() << "[INFO] ctx.tex_filename = '" << ctx.tex_filename.c_str() << "'";
	qDebug() << "[INFO] ctx.utf8_tex_file_flag = " << ctx.utf8_tex_file_flag;
	qDebug() << "[INFO] ctx.png_filename = '" << ctx.png_filename.c_str() << "'";
	qDebug() << "[INFO] ctx.page_width = " << ctx.page_width;
	qDebug() << "[INFO] ctx.page_height = " << ctx.page_height;
	qDebug() << "[INFO] ctx.printable_left_top_x = " << ctx.printable_left_top_x;
	qDebug() << "[INFO] ctx.printable_left_top_y = " << ctx.printable_left_top_y;
	qDebug() << "[INFO] ctx.printable_right_bot_x = " << ctx.printable_right_bot_x;
	qDebug() << "[INFO] ctx.printable_right_bot_y = " << ctx.printable_right_bot_y;
	qDebug() << "[INFO] ctx.app = " << ctx.app;
	qDebug() << "[INFO] ctx.img = " << ctx.img;
	qDebug() << "[INFO] Loaded " << ctx.latex_stream.str().size() << " characters from LaTeX source file";
}


void load_tex_source_from_file_to_context(std::string file, context &ctx){
	std::ifstream t(file.c_str());
	ctx.latex_stream << t.rdbuf();
	t.close();
}

int initialize_context(int &argc, char* argv[], context &ctx){
	// return STEX_SUCCESS (zero) on success, otherwise non-zero
	set_default_context(argc, argv, ctx);
	ctx.img->fill(Qt::white);

	print_context_conditionally(ctx);

        return STEX_SUCCESS;
}


spelltex::Atom* first_stage(context &ctx){
	// 1th step: parse the data from file, create list of tokens
	std::list<spelltex::token> my_tokens;
	spelltex::tokenize(ctx.latex_stream.str(), my_tokens);
	log_tokens(my_tokens, ctx.tex_filename);
	qDebug() << "[DONE] 1th step: tokenize";

        // 2nd step: convert token list to atom tree
        spelltex::Atom* my_atom_tree = new spelltex::PageAtom(ctx.page_width, ctx.page_height, ctx.printable_left_top_x, ctx.printable_left_top_y, ctx.printable_right_bot_x, ctx.printable_right_bot_y);
        std::string std_atomize_log_name(ctx.tex_filename + ".02.atomized.log");
        spelltex::atomize(my_tokens, my_atom_tree, std_atomize_log_name);
	log_atom_tree(my_atom_tree, ctx.tex_filename + ".02.atom-tree.log");
	my_atom_tree->set_debug_flag(ctx.debug_flag); // draw debug rectangles according to user settings
	qDebug() << "[DONE] 2nd step: atomize";

	// 3rd step: process all SpellTeX-macros
	if(evaluate(my_atom_tree) == spelltex::retval::ERROR){
		qDebug() << "[ERROR] Macro evaluation has failed.";
		return nullptr;
	}
	log_atom_tree(my_atom_tree, ctx.tex_filename + ".03.atom-tree-macros-evaluated.log");
	qDebug() << "[DONE] 3rd step: evaluate macros";

	return my_atom_tree;
}

spelltex::Atom* second_stage(spelltex::Atom* my_atom_tree, context &ctx){
	// 4th step: scale
	std::stringstream main_log_stream;
	my_atom_tree->get_log(main_log_stream);
	my_atom_tree->recalculate_scale(); // TODO zoom here
	my_atom_tree->set_pixel_size(ctx.pixel_size);
	log_atom_tree(my_atom_tree, ctx.tex_filename + ".04.atom-tree-scaled.log");
	qDebug() << "[DONE] 4th step: scaled";

//	// 5th step: layout
//	//spelltex::Atom* first_page = nullptr; // buffer for page tree
//	//spelltex::maximum_page_layout(my_atom_tree, first_page, ctx.page_width, ctx.page_height, ctx.printable_top_left_x, ctx.printable_top_left_y, ctx.printable_bot_right_x, ctx.printable_bot_right_y);
//	spelltex::Atom* first_page = nullptr; // buffer for page tree
//	spelltex::classic_page_layout(my_atom_tree, first_page);
//	first_page->set_debug_flag(ctx.debug_flag);  // choose additional info (rectangle draw for chosen atoms)
//	log_atom_tree(first_page, ctx.tex_filename + ".05.atom-tree-classic-layout.log");
//	my_atom_tree = first_page;
//	qDebug() << "[DONE] 5th step: layout";

	// 6th step: size & positions
	my_atom_tree->recalculate_size_and_position();
	log_atom_tree(my_atom_tree, ctx.tex_filename + ".06.atom-tree-size-n-position.log");
	qDebug() << "[DONE] 6th step: size & positions";

	//int page_penalty = spelltex::page_metrics(first_page);
	//std::cout << "[DEBUG] Example Main: Page metrics is " << page_penalty  << std::endl;

	return my_atom_tree;
}


spelltex::Atom* third_stage(spelltex::Atom* atom_tree, context &ctx){
	// 7th step: draw
	std::stringstream main_log_stream;
	spelltex::draw_atom_tree_on_pixmap(atom_tree, *ctx.img, DEFAULT_ZOOM, main_log_stream);
	log_rendering_pipeline(main_log_stream, ctx.tex_filename + ".07.renderized.log");
	qDebug() << "[DONE] 7th step: draw";
	return atom_tree;
}


void spelltex_test(const std::string latex_source_data, context &ctx){
	spelltex::Atom* my_atom_tree = first_stage(ctx); // create atom tree
	my_atom_tree = second_stage(my_atom_tree, ctx); // typeset the atom tree (calculate geometry)
	my_atom_tree = third_stage(my_atom_tree, ctx); // draw
	delete my_atom_tree;
}


int main(int argc, char* argv[]){

	const int number_of_columns = 3;
	const int column_width = 100; // [px]
	const int number_of_rows = 3;
	const int row_height = 60; // [px]
	const int x_margin = 20; // [px]
	const int y_margin = 40; // [px]

	context ctx; // user configuration & input LaTeX data for further SpellTeX processing
        if(initialize_context(argc, argv, ctx) != STEX_SUCCESS) // fill context with valid data
                return STEX_GENERAL_ERROR;

	for(int index_x = 0; index_x < number_of_columns; index_x++){
		for(int index_y = 0; index_y < number_of_rows; index_y++){
			ctx.page_width = column_width;
			ctx.page_height = row_height;
			ctx.printable_left_top_x = index_x * column_width + x_margin;
			ctx.printable_left_top_y = index_y * row_height + y_margin;
			ctx.printable_right_bot_x = (index_x+1) * column_width + x_margin;
			ctx.printable_right_bot_y = (index_y+1) * row_height + y_margin;
			{
			QRectF test_borders(ctx.printable_left_top_x, ctx.printable_left_top_y, ctx.page_width, ctx.page_height);
			QPainter painter(ctx.img);
			painter.drawRect(test_borders);
			}
			ctx.tex_filename = std::string("test_") + std::to_string(index_x) + std::to_string(index_y) + ".tex";
			qDebug() << "==== " << QString(ctx.tex_filename.c_str()) << " ====";
			ctx.latex_stream.str(""); // clear previous LaTeX source
			load_tex_source_from_file_to_context(ctx.tex_filename, ctx);
			// verify UTF-8 file format before parsing
			if(spelltex::valid_utf8_file(ctx.tex_filename.c_str()) != true){
				qDebug() << "[ERROR] Invalid UTF-8 file format in '" << ctx.tex_filename.c_str() << "'. Aborted.";
				return STEX_INPUT_FILE_NOT_UTF8;
			}else{
				ctx.utf8_tex_file_flag = true;
			}

			spelltex_test(ctx.latex_stream.str(), ctx);
		}
	}

	// save the picture to file
	auto ok = ctx.img->save(QString::fromStdString(ctx.png_filename));
	if (!ok) {
		qDebug() << "[ERROR] Image save to file '" << QString::fromStdString(ctx.png_filename) << "' has failed. Aborted.";
		return STEX_IMAGE_SAVE_FAILED;
        }
	//delete ctx.img;

	QGraphicsScene scene;
	QGraphicsView view(&scene);
	QGraphicsPixmapItem item(QPixmap(QString(ctx.png_filename.c_str())));
	scene.addItem(&item);
	view.show();
	return ctx.app->exec();
	//return STEX_SUCCESS;
}
