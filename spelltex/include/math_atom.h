#ifndef MATH_ATOM_H
#define MATH_ATOM_H

#include "atom.h"

namespace spelltex {

	class MathAtom : public Atom {
		public:
			MathAtom(SpellTeX* context, Atom * parent) : Atom(context, parent) {
				type = ATOM_MATH;
				std::string atom_name;
				atom_type2string(type, atom_name);

				if(parent == nullptr){
                                	_log << "[ERROR] " << atom_name << " has nullptr parent." << std::endl;
				}
                        	_parent = parent;
				if(_debug_color != nullptr)
					delete _debug_color;
				_debug_color = new QColor("green");

				_log << "[INFO] Created " << atom_name << " id=" << _id << ", parent_id=" << _parent->_id << std::endl;
			};

			MathAtom(Atom* original, Atom* parent) : Atom(original, parent) { // copy constructor
				// nothing special here, all done in Atom copy constructor
			}

			int copy_atom_tree(spelltex::Atom* parent){
				// create copy of itself and add it as child for given parent
				MathAtom* my_copy = new MathAtom(this, parent);
				my_copy->_debug_atom_tree_id = parent->_debug_atom_tree_id;
				std::list<spelltex::Atom*>::iterator it;
				for(it = this->_childs.begin(); it != this->_childs.end(); it++){
					(*it)->copy_atom_tree(my_copy);
				}
				return 0; // TODO STEX_SUCCESS retval
			}


			bool is_math_shift() override{
				if(_parent == nullptr)
					return true;
				if(_parent->is_math_shift() == true)
					return false;
				
				return true;
			}

			void recalculate_size(spelltex::atom_constraints ac) override{
				hbox_size_recalculation(ac);
			}

                        void recalculate_position() override {
                                horizontal_row_recalculation();
                        }


			double textual_width() override{
				// returns total width of text, NOT counting spaces
				// used for space scaling
				if(_deleted == true)
					return 0;
				if(_childs.size() == 0)
					return 0;

				double ret = 0;
				std::list<Atom*>::iterator it = _childs.begin();
				while(it != _childs.end()){
					ret += (*it)->width();
					it++;
				}
				return ret;
			};

			int count_spaces() override {
				if(_deleted == true)
					return 0;

				return 0; // completely ignore internal spaces, because math formulas has own rendering rules
			}

			Atom* space_signal() override{
                                return this;
                        }

			Atom* math_end_signal() override{
                                if(_parent == nullptr)
                                        return this;

                                return _parent;
                        }
	};
}

#endif // MATH_ATOM_H
