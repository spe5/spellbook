#ifndef ROOT_ATOM_H
#define ROOT_ATOM_H

#include "atom.h"

namespace spelltex{

	class RootAtom : public Atom {
		// root atom is convenience for encapsulating all SpellTeX pages into single atom tree
		// root atom is convenience to meet Qt's assumption that Qt function TODO must NOT return root item of tree
		// root atom can NOT be drawn, nor has any debug rectangle, because it is NOT child of any page, but rather page's parent
		
		public:
			RootAtom(spelltex::SpellTeX* context) : Atom(context, nullptr) {
				type = ATOM_ROOT;
				_debug_atom_tree_id = gen_random(3);
				_parent = nullptr;
				if(_debug_color != nullptr)
					delete _debug_color;
				_debug_color = new QColor("black");
			};

			RootAtom(const Atom* original) : Atom(original, nullptr) { // copy constructor
				// nothing special here, all done in Atom copy constructor
				_debug_atom_tree_id = gen_random(3);
			}

                        virtual void recalculate_size(){
                                // sets internal variables x_pos_in_parent and y_pos_in_parent
                                // some atoms already know its size based only on glyph dimensions (like char atoms)
                                // some atoms depend on its childs to finish the dimension calculation
                                // childs of root atom are pages, which are processed separately
				// request size calculation from all childs (pages)
		                for(auto it = _childs.begin(); it != _childs.end(); it++){
                		        (*it)->recalculate_size();
		                }
                        }

                        virtual void recalculate_size(const spelltex::atom_constraints ac){
                                // sets internal variables x_pos_in_parent and y_pos_in_parent
                                // some atoms already know its size based only on glyph dimensions (like char atoms)
                                // some atoms depend on its childs to finish the dimension calculation
				std::cout << "[WARN] Incorrect variant of function recalculate_size() on ROOT ATOM - argument ignored" << std::endl;
				recalculate_size(); // this function is NOT intended for common use, ignore the argument, redirect to another variant
                        }

			virtual void recalculate_position(){
				vertical_column_recalculation();
			}

			void draw() {
				// assumption: internal variables _offset_x,y and _width, _height, _log are already set correctly
				draw_all_childs();
			}
	
			int copy_atom_tree(spelltex::Atom* parent){
				// create copy of itself and add it as child for given parent
				std::cout << "[ERROR] Calling copy_atom_tree on ROOT ATOM. That is NOT allowed. Aborting." << std::endl;
				//RootAtom* my_copy = new RootAtom(this);
				//std::list<spelltex::Atom*>::iterator it;
				//for(it = this->_childs.begin(); it != this->_childs.end(); it++){
				//	(*it)->copy_atom_tree(my_copy);
				//}
				return 0; // TODO STEX_SUCCESS retval
			}
	};
}

#endif // ROOT_ATOM_H


