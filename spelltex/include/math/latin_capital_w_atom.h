#ifndef LATIN_CAPITAL_W_ATOM_H
#define LATIN_CAPITAL_W_ATOM_H

#include <iostream>
#include <sstream>
#include <list>
#include <QPixmap>
#include <QPainter>
#include <QFontDatabase>
#include <QFile>
#include "../utf8.h"
#include "../common.h"
#include "../atom.h"
#include "../letter_atom.h"

namespace spelltex {

	class LatinCapitalWAtom : public Atom {
		public:
			LatinCapitalWAtom(SpellTeX* context, Atom * parent) : Atom(context, parent) {
				type = ATOM_LETTER;
				if(parent == nullptr){
					std::string atom_name;
					atom_type2string(type, atom_name);
					std::cout << "[ERROR] " << atom_name << " has nullptr parent." << std::endl;
				}
				_parent = parent;

				_std_text = false;
				_ttf_glyph = true;
				_glyph_name = "LATIN_CAPITAL_W_ATOM";
				_font_file = new QString("cmmi10.ttf");
			        _raw_font = new QRawFont(*_font_file, _pixel_size, QFont::PreferDefaultHinting);
				_glyph_index = 58;

				get_glyph_geometry();
			}

			LatinCapitalWAtom(Atom* original, Atom* parent) : Atom(original, parent) { // copy constructor
				// nothing special here, all done in Atom copy constructor
			}

			int copy_atom_tree(spelltex::Atom* parent){
				// create copy of itself and add it as child for given parent
				LatinCapitalWAtom* my_copy = new LatinCapitalWAtom(this, parent);
				my_copy->_debug_atom_tree_id = parent->_debug_atom_tree_id;
				std::list<spelltex::Atom*>::iterator it;
				for(it = this->_childs.begin(); it != this->_childs.end(); it++){
					(*it)->copy_atom_tree(my_copy);
				}
				return 0; // TODO STEX_SUCCESS retval
			}

	};
}

#endif // LATIN_CAPITAL_W_ATOM_H
