#ifndef MATH_SYMBOL_VEC_ATOM_H
#define MATH_SYMBOL_VEC_ATOM_H

#include "../atom.h"

namespace spelltex {

	class MathSymbolVecAtom : public Atom {
		// special handling of width: vector changes its width according to its 1th argument and is placed above it, thus itself will NOT change line width in any way

		public:
			MathSymbolVecAtom(SpellTeX* context, Atom* parent) : Atom(context, parent) {
				_std_text = false;
				_ttf_glyph = true;
				_glyph_name = "MATH_SYMBOL_VEC_ATOM";
				_font_file = new QString("cmsy10.ttf");
				_glyph_index = 4;

                        	if(parent == nullptr){
					std::string atom_name;
					atom_type2string(type, atom_name);
                                	std::cout << "[ERROR] " << atom_name << ":" << _glyph_name << " has nullptr parent." << std::endl;
				}
                        	_parent = parent;

				// load glyph data from font file
			        _raw_font = new QRawFont(*_font_file, _pixel_size, QFont::PreferDefaultHinting);
				get_glyph_geometry();
                        };

			MathSymbolVecAtom(Atom* original, Atom* parent) : Atom(original, parent) { // copy constructor
				// nothing special here, all done in Atom copy constructor
			}

			int copy_atom_tree(spelltex::Atom* parent){
				// create copy of itself and add it as child for given parent
				MathSymbolVecAtom* my_copy = new MathSymbolVecAtom(this, parent);
				my_copy->_debug_atom_tree_id = parent->_debug_atom_tree_id;
				std::list<spelltex::Atom*>::iterator it;
				for(it = this->_childs.begin(); it != this->_childs.end(); it++){
					(*it)->copy_atom_tree(my_copy);
				}
				return 0; // TODO STEX_SUCCESS retval
			}


			void recalculate_size(spelltex::atom_constraints ac) override{
				hbox_size_recalculation(ac);
				// TODO add size for the arrow above all child atoms
			}


			void recalculate_position() override { // TBD - move this to evaluate() function?
                                // sets internal variables x_pos_in_parent and y_pos_in_parent
                                // some atoms already know its size based only on glyph dimensions (like char atoms)
                                // some atoms depend on its childs to finish the dimension calculation

				if(_deleted == true)
					return;

       				// control sequence \vec requires 1 argument in { } brackets
				std::list<spelltex::Atom*> later_siblings;
				_log << "[INFO] Evaluating " << _glyph_name << " id=" << _id << ", pid=" << _parent->_id << std::endl;
			        if(get_later_siblings(later_siblings) != 0){
					_log << "[ERROR] Unable to get_later_siblings for " << _glyph_name << " with id " << _id << std::endl;
					return;
				}

				if(later_siblings.size() < 1){
					_log << "[ERROR] Unable to get at least 1 siblings for " << _glyph_name << " with id " << _id << std::endl;
					return;
				}

				// keep only 1th argument as text for typesettings, all other arguments are not part of the macro, thus ignored
				std::list<spelltex::Atom*>::iterator it;
				std::list<spelltex::Atom*>::iterator it_1th_arg;
				//QFont *vec_font = new QFont("cmsy10", 16);
				//QFontInfo real_font(*mathcal_font);
				//std::cout << "[INFO] MathcalFont is actually " << real_font.family().toStdString() << " with size " << real_font.pointSize() << std::endl;
				//QFontMetricsF *mathcal_metrics = new QFontMetricsF(*mathcal_font);
				bool found = false;
				for(it = _parent->_childs.begin(); it != _parent->_childs.end(); it++){
					if(*it == this){
						found = true;
						it_1th_arg = std::next(it, 1);
						_offset_x = (*it_1th_arg)->_offset_x; // vector starts at the same x-position as its member below
						_width = (*it_1th_arg)->width(); // vector has the same width as its atom under it
						_offset_y = (*it_1th_arg)->_offset_y - (*it_1th_arg)->height();
						_log << "[INFO] " << _glyph_name << " w=" << _width << " h=" << _height << " ox=" << _offset_x << " oy=" << _offset_y << std::endl;
						break;
					}

				}
				if(!found)
					_log << "[ERROR] Unable to locate this " << _glyph_name << " (id=" << _id << ") between my parent childs." << std::endl;
				
			};
	};

}

#endif // MATH_SYMBOL_VEC_ATOM_H
