#ifndef GREEK_SMALL_OMEGA_ATOM_H
#define GREEK_SMALL_OMEGA_ATOM_H

#include <iostream>
#include <sstream>
#include <list>
#include <QPixmap>
#include <QPainter>
#include <QFontDatabase>
#include <QFile>
#include <QGlyphRun>
#include <QRawFont>
#include "../utf8.h"
#include "../common.h"
#include "../atom.h"
#include "../letter_atom.h"

namespace spelltex {

	class GreekSmallOmegaAtom : public Atom {
		public:

			GreekSmallOmegaAtom(SpellTeX* context, Atom * parent) : Atom(context, parent) {
				// unique identification, constants only
				_glyph_name = "GREEK_SMALL_OMEGA_ATOM";
				_font_file = new QString("cmmi10.ttf");
				_glyph_index = 4;
		
				type = ATOM_LETTER;
				if(parent == nullptr){
					std::string atom_name;
					atom_type2string(type, atom_name);
					std::cout << "[ERROR] " << atom_name << ":" << _glyph_name << " has nullptr parent." << std::endl;
				}
				_parent = parent;

				// renderer configuration
				_std_text = false;
				_ttf_glyph = true; // use TTF file with the glyph shape to draw

				// TTF glyph settings
			        _raw_font = new QRawFont(*_font_file, _pixel_size, QFont::PreferDefaultHinting);
				get_glyph_geometry(); // load _default_width, _width, ... from raw font file using the glyph index, NOT scaled yet

			}

			GreekSmallOmegaAtom(Atom* original, Atom* parent) : Atom(original, parent) { // copy constructor
				// nothing special here, all done in Atom copy constructor
			}

			int copy_atom_tree(spelltex::Atom* parent){
				// create copy of itself and add it as child for given parent
				GreekSmallOmegaAtom* my_copy = new GreekSmallOmegaAtom(this, parent);
				my_copy->_debug_atom_tree_id = parent->_debug_atom_tree_id;
				std::list<spelltex::Atom*>::iterator it;
				for(it = this->_childs.begin(); it != this->_childs.end(); it++){
					(*it)->copy_atom_tree(my_copy);
				}
				return 0; // TODO STEX_SUCCESS retval
			}

	};
}

#endif // GREEK_SMALL_OMEGA_ATOM_H
