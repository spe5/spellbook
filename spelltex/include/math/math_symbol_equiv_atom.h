#ifndef MATH_SYMBOL_EQUIV_ATOM_H
#define MATH_SYMBOL_EQUIV_ATOM_H

#include <iostream>
#include <sstream>
#include <list>
#include <QPixmap>
#include <QPainter>
#include <QFontDatabase>
#include <QFile>
#include "../utf8.h"
#include "../common.h"
#include "../atom.h"
#include "../letter_atom.h"

namespace spelltex {

	class MathSymbolEquivAtom : public Atom {
		public:
			MathSymbolEquivAtom(SpellTeX* context, Atom * parent) : Atom(context, parent) {
				// unique identification, constants only
				_std_text = false;
				_ttf_glyph = true;
				_glyph_name = "MATH_SYMBOL_EQUIV_ATOM";
				_font_file = new QString("cmsy10.ttf");
				_glyph_index = 116;

				type = ATOM_MATH_OPERATOR;

				if(parent == nullptr){
					std::string atom_name;
					atom_type2string(type, atom_name);
					std::cout << "[ERROR] " << atom_name << ":" << _glyph_name << " has nullptr parent." << std::endl;
				}
				_parent = parent;

				// load glyph data from font file
			        _raw_font = new QRawFont(*_font_file, _pixel_size, QFont::PreferDefaultHinting);
				get_glyph_geometry();
			}

			MathSymbolEquivAtom(Atom* original, Atom* parent) : Atom(original, parent) { // copy constructor
				// nothing special here, all done in Atom copy constructor
			}

			int copy_atom_tree(spelltex::Atom* parent){
				// create copy of itself and add it as child for given parent
				MathSymbolEquivAtom* my_copy = new MathSymbolEquivAtom(this, parent);
				my_copy->_debug_atom_tree_id = parent->_debug_atom_tree_id;
				std::list<spelltex::Atom*>::iterator it;
				for(it = this->_childs.begin(); it != this->_childs.end(); it++){
					(*it)->copy_atom_tree(my_copy);
				}
				return 0; // TODO STEX_SUCCESS retval
			}

	};
}

#endif // MATH_SYMBOL_EQUIV_ATOM_H
