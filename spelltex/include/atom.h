#ifndef ATOM_H
#define ATOM_H

#include "common.h"
#include<QRawFont>
#include<QGlyphRun>
#include "spelltex.h"


namespace spelltex {
	class SpellTeX; // break circular dependecy between SpellTeX and Atom classes

	class Atom {
		public:
			// context
			SpellTeX* _context;

			// tree-related variables
			Atom* _parent;
			std::list<Atom*> _childs;
			std::string _id; // mainly for debugging purposes
			std::string _debug_atom_tree_id; // same for atoms in the same atom tree, for debugging purposes, to distinct between copies of atom trees
			bool _deleted; // false if object is still valid, true if its content should NOT be used - workaround for child list invalidation during evaluation()

			// algorithm-related variables
			atom_type type; // helps atomizer with special cases and for debug purposes

			// global geometry limits for all atom childs, represents a page
			int _page_width;
			int _page_height;
			int _left_top_x;
			int _left_top_y;
			int _right_bot_x;
			int _right_bot_y;

			// default dimensions, before applying scale and other typesetting-related operations
			// used for rendering atom on canvas (row positions, calculating baseline, etc.)
			double _default_width; // positive value
			double _default_partial_height; // positive value, representing part of the glyph above baseline
			double _default_depth; // positive value, representing part of the glyph under baseline
			double _default_height;
			double _default_scale; // positive value, representing internal scaling of the atom, usually equals to 1.0 (except for sub/sup-scripts or page atom with user defined zoom)

			// scaled dimensions, ready for drawing
			double _width;
			double _height;
			double _partial_height;
			double _depth;
			double _ascent;
			bool _size_calculation_finished; // true if atom size calculation successfully finished & _width, _height, etc. contains final values, false if atom size is NOT known yet

			QColor* _debug_color; // color used to draw debug rectangles, for troubleshooting by SpellTeX developers
			QColor* _main_color; // color for typesetting text and symbols, requested by end-user

			double _offset_x = 0.0; // relative x-position of this atom in parent atom
			double _offset_y = 0.0; // relative y-position of this atom in parent atom

			// geometry-related variables
			int _pixel_size; // each atom is rendered with a pixel size
			double _current_point_size; // propoerty for _painter settings, based on pixel_size
			double _scale; // value between 0.0 - 1.0, representing scale applied to _pixel_size
			spelltex::glue_power _glue_power; // priority of this atom as glue - for spacing algorithms

			bool _debug_flag; // true if debug rectangles should be drawn

			std::stringstream _log; // pointer to log output

			// renderer settings
			QPainter* _painter;
			QPixmap* _pixmap;
			QRect _draw_rectangle;
			bool _ttf_glyph; // true if draw function shall use drawGlyphRun with internal glyph settings
			bool _std_text; // true if draw function shall use drawText with internal character settings from variable 'letter'

			// used for _std_text
			QFont* _default_main_font; // default font, before applying scale and other tyesetting-related operations
			QFont* _main_font; // scaled font, ready for rendering
			std::string _letter; // used for _std_text
			std::string _glyph_name; // used _ttf_glyph, for identification and debug purposes

			// used for _ttf_glyph
			QRawFont* _raw_font;
			int _glyph_index; 
			QString* _font_file; // file name for atoms that use glyphs stored in standalone font file

			// used for debug rectangles, rendered OUTSIDE of SpellTeX, in absolute values, filled during SpellTeX rendering stage
			double deb_rect_top_left_x; // [pixel], absolute value for given canvas / page
			double deb_rect_top_left_y; // [pixel], absolute value for given canvas / page
			double deb_rect_mid_right_x; // [pixel], absolute value for given canvas / page
			double deb_rect_mid_right_y; // [pixel], absolute value for given canvas / page
			double deb_rect_mid_left_x; // [pixel], absolute value for given canvas / page
			double deb_rect_mid_left_y; // [pixel], absolute value for given canvas / page
			double deb_rect_bot_right_x; // [pixel], absolute value for given canvas / page
			double deb_rect_bot_right_y; // [pixel], absolute value for given canvas / page

			Atom(spelltex::SpellTeX* context, Atom* parent);
			Atom(const Atom* original, Atom* parent); // copy constructor

			void set_pixel_size(int new_pixel_size);
			void class_id(std::string& buffer);

			void initialize_draw_context();
			void set_debug_brush();
			void set_main_brush();
			void draw_lower_debug_rectangle(const QPointF left_baseline_position);
			void draw_upper_debug_rectangle(const QPointF left_baseline_position);
			void get_glyph_geometry();
			void get_letter_geometry();
			void draw_std_letter_only(QPointF left_baseline_position);
			int draw_std_letter(QPointF left_baseline_position);
			void draw_glyph_only(QPointF left_baseline_position);
			int draw_glyph(QPointF left_baseline_position);
			void get_abs_offset(double &offset_x, double &offset_y);
			void set_draw_context(QPainter* painter, QPixmap* pixmap);
			void get_log(std::stringstream &log);
			void print_me(std::stringstream &log, const bool del);
			virtual void print_me(std::stringstream &log, const int level, const bool del);
			void set_debug_flag(const bool debug_flag);
			virtual void draw();
			void set_scaled_local_font();
			void set_scaled_raw_font();
			void draw_all_childs();
			double width();
			double height();
			double partial_height();
			double depth();
			virtual retval evaluate();
			int get_later_siblings(std::list<spelltex::Atom*> &later_siblings);
			virtual double textual_width();
			virtual void scale_spaces(double scale_factor);
			virtual int count_spaces();
			void update_subtree_font(QFont *new_font, QFontMetricsF *new_metrics);
			int get_atom_level();
			virtual bool is_math_shift();
			virtual Atom* space_signal();
			virtual Atom* math_end_signal();
			virtual Atom* group_end_signal();
			void recalculate_scale();

			void initialize_size();
			spelltex::glue_power get_the_highest_glue_power_of_childs();
			void recalculate_size_recursively(spelltex::atom_constraints ac);
			bool is_child_size_recalculation_done();
			virtual void recalculate_size(const spelltex::atom_constraints ac); // for call on child atoms
			virtual void recalculate_size(); // for call on whole atom tree
			void sum_up_size_of_childs_horizontally();
			void set_scale_of_the_most_powerful_spaces(double scale_of_spaces);
			void stretch_the_most_prominent_spaces(spelltex::atom_constraints ac);

			void initialize_position();
			virtual void recalculate_position();
			void horizontal_row_recalculation();
			void hbox_size_recalculation(const spelltex::atom_constraints ac);
			void vbox_size_recalculation(const spelltex::atom_constraints ac);
			void no_change_recalculation();
			void vertical_column_recalculation();
			virtual int copy_atom_tree(spelltex::Atom* parent) = 0;
			void get_str_type(std::string& buffer);

			// iterating atom tree
			int _iterator; // -1 for itself, 0 for 1th child, 1for 2nd child, etc. 
			void initialize_iterator();
			void reset_iterators();
			spelltex::Atom* next();
	};
}
#endif // ATOM_H
