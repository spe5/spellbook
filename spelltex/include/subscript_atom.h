#ifndef SUBSCRIPT_ATOM_H
#define SUBSCRIPT_ATOM_H

#include "common.h"
#include "atom.h"

namespace spelltex{
	class SubscriptAtom : public Atom {
		public:
			SubscriptAtom(SpellTeX* context, Atom * parent) : Atom(context, parent) {
				type = ATOM_SUBSCRIPT;
				_default_scale = 0.6;
				_scale = 0.6;
				if(_debug_color != nullptr)
					delete _debug_color;
				_debug_color = new QColor("blue");
			}


			SubscriptAtom(Atom* original, Atom* parent) : Atom(original, parent) { // copy constructor
				// nothing special here, all done in Atom copy constructor
			}


			int copy_atom_tree(spelltex::Atom* parent){
				// create copy of itself and add it as child for given parent
				SubscriptAtom* my_copy = new SubscriptAtom(this, parent);
				my_copy->_debug_atom_tree_id = parent->_debug_atom_tree_id;
				std::list<spelltex::Atom*>::iterator it;
				for(it = this->_childs.begin(); it != this->_childs.end(); it++){
					(*it)->copy_atom_tree(my_copy);
				}
				return 0; // TODO STEX_SUCCESS retval
			}


			void recalculate_size(const spelltex::atom_constraints ac) override{
				// TODO
			}


			virtual void recalculate_position() override{
	                        // sets internal variables offset_x and offset_y
        	                // some atoms already know its size based only on glyph dimensions (like char atoms)
                	        // some atoms depend on its childs to finish the dimension calculation

				no_change_recalculation();
			}
	};
}
#endif // SUBSCRIPT_ATOM_H
