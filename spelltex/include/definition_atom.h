#ifndef DEFINITION_ATOM_H
#define DEFINITION_ATOM_H

#include "atom.h"

namespace spelltex {

	class DefinitionAtom : public Atom {
		public:
			DefinitionAtom(SpellTeX* context, Atom* parent, const std::string chars) : Atom(context, parent) {
				type = ATOM_DEFINITION;
                        	if(parent == nullptr)
                                	std::cout << "[ERROR] Definition Atom has nullptr parent." << std::endl;
                        	_parent = parent;
				std::cout << "[INFO] Created DefinitionAtom id=" << _id << ", parent_id=" << _parent->_id << std::endl;
                        };

			DefinitionAtom(Atom* original, Atom* parent) : Atom(original, parent) { // copy constructor
				// nothing special here, all done in Atom copy constructor
			}

			int copy_atom_tree(spelltex::Atom* parent){
				// create copy of itself and add it as child for given parent
				DefinitionAtom* my_copy = new DefinitionAtom(this, parent);
				my_copy->_debug_atom_tree_id = parent->_debug_atom_tree_id;
				std::list<spelltex::Atom*>::iterator it;
				for(it = this->_childs.begin(); it != this->_childs.end(); it++){
					(*it)->copy_atom_tree(my_copy);
				}
				return 0; // TODO STEX_SUCCESS retval
			}

			retval evaluate() override {
				// control sequence \definition requires 4 arguments in { } brackets
				std::list<spelltex::Atom*> later_siblings;
				std::cout << "[INFO] Evaluating DefinitionAtom id=" << _id << ", parent_id=" << _parent->_id << std::endl;
			        if(get_later_siblings(later_siblings) != 0){
					std::cout << "[ERROR] Unable to get_later_siblings for DefinitionAtom with id " << _id << std::endl;
					return retval::ERROR;
				}

				if(later_siblings.size() < 4){
					std::cout << "[ERROR] Unable to get at least 4 siblings for DefinitionAtom with id " << _id << std::endl;
					return retval::ERROR;
				}

				// keep only 2nd argument as text for typesettings, all other arguments (#1, #3, #4) are metadata, currently ignored
				std::list<spelltex::Atom*>::iterator it;
				std::list<spelltex::Atom*>::iterator it1;
				std::list<spelltex::Atom*>::iterator it3;
				std::list<spelltex::Atom*>::iterator it4;
				for(it = _parent->_childs.begin(); it != _parent->_childs.end(); it++){
					if(*it == this){
						it1 = std::next(it, 1);
						it3 = std::next(it, 3);
						it4 = std::next(it, 4);
						(*it1)->_deleted = true;
						(*it3)->_deleted = true;
						(*it4)->_deleted = true;
						(*it)->_deleted = true;
						return retval::SUCCESS;
					}

				}
				std::cout << "[ERROR] Unable to locate this Atom (id=" << _id << ") between my parent childs." << std::endl;
				return retval::ERROR;
			};
	};

}

#endif // DEFINITION_ATOM_H
