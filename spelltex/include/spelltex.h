#ifndef SPELLTEX_H_INCLUDED
#define SPELLTEX_H_INCLUDED

#include <iostream>
#include <sstream>
#include <list>
#include <QPixmap>
#include <QPainter>
#include <QTextStream>
#include "utf8.h"

#include "common.h"
#include "atom.h"
//#include "letter_atom.h"
//#include "math/greek_small_varphi_atom.h"
//#include "math/greek_small_phi_atom.h"
//#include "math/greek_small_omega_atom.h"
//#include "math/latin_small_k_atom.h"
//#include "math/latin_small_v_atom.h"
//#include "math/latin_capital_w_atom.h"
//#include "math/math_symbol_equiv_atom.h"
//#include "math/math_symbol_vec_atom.h"
//#include "mathcal_atom.h"
//#include "space_atom.h"
//#include "page_atom.h"
//#include "vertical_atom.h"
//#include "horizontal_atom.h"
//#include "definition_atom.h"
//#include "math_atom.h"
//#include "word_atom.h"
//#include "group_atom.h"
//#include "subscript_atom.h"

namespace spelltex {
	class Atom;

	class SpellTeX {	
	        public:
	                bool debug_flag; // true for more verbosity, false otherwise
	                std::stringstream latex_stream; // input LaTeX source
	                //QPixmap *img; // output figure with rendered LaTeX
	                QApplication *app; // global object for all Qt-related GUI stuff
	                int _default_page_width;
	                int _default_page_height;
	                int _default_printable_left_top_x;
	                int _default_printable_left_top_y;
	                int _default_printable_right_bot_x;
	                int _default_printable_right_bot_y;
	                int _user_page_width;
	                int _user_page_height;
	                int _user_printable_left_top_x;
	                int _user_printable_left_top_y;
	                int _user_printable_right_bot_x;
	                int _user_printable_right_bot_y;
			
			double _zoom; // 1.0 for 100%, 1.5 for 150%, etc.
	
			QFont* _default_main_font;
			int _pixel_size;
			QColor* _main_color;

                        std::list<spelltex::token> my_tokens;
			spelltex::Atom* root; // copy of 2nd step in first stage - atomization, for debugging

	                QTextStream qts_tokenizer_log;
	                std::stringstream ss_atom_log;
	                std::stringstream ss_renderer_log;

			void update_font_type(QFont* new_font);
			void update_font_size(int pixel_size);
			void update_color(QColor* new_color);
	                void print_context_conditionally();
	                int tokenize(const std::string latex_source, std::list<spelltex::token>& tokens);
	                void log_tokens(std::list<spelltex::token> &my_tokens);
	                //void set_draw_context(QPainter* painter, QPixmap* pixmap);
	                void log_current_cursor(spelltex::Atom* current_cursor, std::stringstream &log); // auxiliary, used only in atomize()
	                void atomize(const std::list<spelltex::token> my_tokens, spelltex::Atom* my_atom_tree, std::stringstream &log);
	                void log_atom_tree(spelltex::Atom* my_atom_tree);
	                bool evaluate(spelltex::Atom* my_atom_tree);
	
	                spelltex::Atom* first_stage(std::vector<spelltex::Atom*>& atom_trees_buffer);
	                spelltex::Atom* second_stage(spelltex::Atom* my_atom_tree, std::vector<spelltex::Atom*>& atom_trees_buffer);
	                spelltex::Atom* third_stage(spelltex::Atom* atom_tree, std::vector<spelltex::Atom*>& atom_trees_buffer, QPixmap* pixmap_buffer);

			int copy_atom_tree(const spelltex::Atom* orig_root, spelltex::Atom** new_root);

	                bool valid_utf8_file(const char* file_name);
	                //void get_last_horizontal(spelltex::Atom* &cursor, spelltex::Atom* root);
	
	                void draw_atom_tree_on_pixmap(spelltex::Atom* my_atom_tree, QPixmap *my_pixmap, const double zoom, std::stringstream &log);
	
	                int horizontal_metrics(spelltex::Atom* horizontal_atom_tree, const float ideal_width);
	                int page_metrics(spelltex::Atom* page_atom_tree);
	                //void maximum_page_layout(spelltex::Atom* main_atom_tree, spelltex::Atom* &page_atom_tree);
	                //void classic_page_layout(spelltex::Atom* main_atom_tree, spelltex::Atom* &page_atom_tree);
	                //void finalize_horizontal_row(spelltex::Atom* horizontal_atom, double target_width);
	
		public:
	                SpellTeX(QApplication* qt_app);
			~SpellTeX();
	                int main(const std::string latex_source, QPixmap *rendered_image, std::vector<spelltex::Atom*>& atom_trees_buffer); // get latex source and configuration inside ctx, renders final image as ctx.img, returns STEX status
			int set_font(QFont& default_font);
			int set_zoom(double zoom);
			int set_page_dimensions(const int page_width, const int page_height, const int top_margin, const int left_margin, const int bottom_margin, const int right_margin);
			int get_default_page_dimensions(int &page_width, int &page_height, int &top_margin, int &left_margin, int &bottom_margin, int &right_margin);
			int get_effective_page_dimensions(int &page_width, int &page_height, int &top_margin, int &left_margin, int &bottom_margin, int &right_margin);
	
	};
}
#endif // SPELLTEX_H_INCLUDED
