#ifndef SPACE_ATOM_H
#define SPACE_ATOM_H

#include "common.h"
#include "atom.h"

namespace spelltex{
	class SpaceAtom : public Atom {
		public:
			constexpr static double _default_width = 5;
			double minimum_stretch;
			double maximum_stretch; // maximum space
			double default_stretch; // default space, used for first iteration
			int stretch_priority; // priority of the space regarding other space atoms during stretch: 0 for space consuming stretch as last
                        double current_stretch; // [width]

			SpaceAtom(SpellTeX* context, Atom * parent) : Atom(context, parent) {
				type = ATOM_SPACE;
				if(parent == nullptr){
					std::string atom_name;
					atom_type2string(type, atom_name);
					std::cout << "[ERROR] " << atom_name << " has nullptr parent." << std::endl;
				}
				_parent = parent;
				//_default_width = 5; // TODO fix this according to used font and its recommended minimum space
				_default_height = 0;
				_default_depth = 0;
				stretch_priority = 0;
				minimum_stretch = 3;
				maximum_stretch = 15;
				default_stretch = _default_width;
				current_stretch = default_stretch;

				_glue_power = spelltex::glue_power::SPACE_IN_TEXT;
			}

			SpaceAtom(Atom* original, Atom* parent) : Atom(original, parent) { // copy constructor
				// nothing special here, all done in Atom copy constructor
				stretch_priority = static_cast<SpaceAtom*>(original)->stretch_priority;
				minimum_stretch = static_cast<SpaceAtom*>(original)->minimum_stretch;
				maximum_stretch = static_cast<SpaceAtom*>(original)->maximum_stretch;
				default_stretch = static_cast<SpaceAtom*>(original)->default_stretch;
				current_stretch = static_cast<SpaceAtom*>(original)->current_stretch;
			}

			int copy_atom_tree(spelltex::Atom* parent){
				// create copy of itself and add it as child for given parent
				SpaceAtom* my_copy = new SpaceAtom(this, parent);
				my_copy->_debug_atom_tree_id = parent->_debug_atom_tree_id;
				std::list<spelltex::Atom*>::iterator it;
				for(it = this->_childs.begin(); it != this->_childs.end(); it++){
					(*it)->copy_atom_tree(my_copy);
				}
				return 0; // TODO STEX_SUCCESS retval
			}


			double textual_width() override{
				// space atom does NOT contain any text
				return 0;
			}


			void scale_spaces(double scale_factor) override{
				// modifyies all spaces to align the row
				if(_deleted == true)
					return;
				_log << "[WARN] scale_spaces is NOT implemented. Doing nothing." << std::endl;
				current_stretch = default_stretch * scale_factor;
			};


			int count_spaces() override{
				if(_deleted == true)
					return 0;
				_log << "[INFO] Space Found id=" << _id << std::endl;
				return 1;
			}


			void recalculate_size(const spelltex::atom_constraints ac) override{
				_width = _default_width * _scale;
				_partial_height = _default_partial_height * _scale;
				_depth = _default_depth * _scale;
				_height = _default_height * _scale;
				_size_calculation_finished = false; // not finished, need to scale space by row completion algos
			}


			void recalculate_position() override{
				horizontal_row_recalculation();
			}


			void draw() {
				// assumption: internal variables _offset_x,y and _width, _height, _log are already set correctly
				if(_deleted == true)
					return;

				int auxiliary_height_for_debug_rectangle = 2; // 2 pixels to make debug rectangle visible
				// used for debug rectangles, rendered OUTSIDE of SpellTeX, in absolute values, filled during SpellTeX rendering stage
				double abs_offset_x = 0;
				double abs_offset_y = 0;
				get_abs_offset(abs_offset_x, abs_offset_y);
				deb_rect_top_left_x = abs_offset_x; // [pixel], absolute value for given canvas / page
				deb_rect_top_left_y = abs_offset_y; // [pixel], absolute value for given canvas / page
				deb_rect_mid_right_x = abs_offset_x + _width; // [pixel], absolute value for given canvas / page
				deb_rect_mid_right_y = abs_offset_y - auxiliary_height_for_debug_rectangle; // [pixel], absolute value for given canvas / page
				deb_rect_mid_left_x = 0; // [pixel], absolute value for given canvas / page
				deb_rect_mid_left_y = 0; // [pixel], absolute value for given canvas / page
				deb_rect_bot_right_x = 0; // [pixel], absolute value for given canvas / page
				deb_rect_bot_right_y = 0; // [pixel], absolute value for given canvas / page
			}

	};
}
#endif // SPACE_ATOM_H
