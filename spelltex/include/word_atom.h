#ifndef WORD_ATOM_H
#define WORD_ATOM_H

#include <iostream>
#include <sstream>
#include <list>
#include <QPixmap>
#include <QPainter>
#include "utf8.h"

#include "common.h"
#include "atom.h"

namespace spelltex{

	class WordAtom : public Atom {
		public:
			WordAtom(SpellTeX* context, Atom * parent) : Atom(context, parent) {
				type = ATOM_WORD;
				if(parent == nullptr){
					std::string atom_name;
					atom_type2string(type, atom_name);
					_log << "[ERROR] " << atom_name << " has nullptr parent." << std::endl;
				}
				_parent = parent;
				_glue_power = spelltex::glue_power::NOT_A_GLUE;
			}

			WordAtom(Atom* original, Atom* parent) : Atom(original, parent) { // copy constructor
				// nothing special here, all done in Atom copy constructor
			}

			int copy_atom_tree(spelltex::Atom* parent){
				// create copy of itself and add it as child for given parent
				WordAtom* my_copy = new WordAtom(this, parent);
				my_copy->_debug_atom_tree_id = parent->_debug_atom_tree_id;
				std::list<spelltex::Atom*>::iterator it;
				for(it = this->_childs.begin(); it != this->_childs.end(); it++){
					(*it)->copy_atom_tree(my_copy);
				}
				return 0; // TODO STEX_SUCCESS retval
			}


			void recalculate_size(const spelltex::atom_constraints ac) override {
				hbox_size_recalculation(ac);
			}


			void recalculate_position() override {
				horizontal_row_recalculation();
			}


			void draw() {
				// assumption: internal variables _offset_x,y and _width, _height, _log are already set correctly
				if(_deleted == true)
					return;

				draw_all_childs();
	
				// calculate own size querying dimensions from childs
				std::list<spelltex::Atom*>::iterator it;
				//_width = 0;
				//_depth = 0;
				//_partial_height = 0;
				//_height = 0;
				double the_highest_child_of_word = 0;
				for(it = this->_childs.begin(); it != this->_childs.end(); it++){
					if(the_highest_child_of_word < (*it)->_height)
						the_highest_child_of_word = (*it)->_height;
				}
				_partial_height = the_highest_child_of_word;
				
				// used for debug rectangles, rendered OUTSIDE of SpellTeX, in absolute values, filled during SpellTeX rendering stage
				double abs_offset_x = 0;
				double abs_offset_y = 0;
				get_abs_offset(abs_offset_x, abs_offset_y);
				deb_rect_top_left_x = abs_offset_x; // [pixel], absolute value for given canvas / page
				deb_rect_top_left_y = abs_offset_y; // [pixel], absolute value for given canvas / page
				deb_rect_mid_right_x = abs_offset_x + _width; // [pixel], absolute value for given canvas / page
				deb_rect_mid_right_y = abs_offset_y - _partial_height; // [pixel], absolute value for given canvas / page
				deb_rect_mid_left_x = 0; // [pixel], absolute value for given canvas / page
				deb_rect_mid_left_y = 0; // [pixel], absolute value for given canvas / page
				deb_rect_bot_right_x = 0; // [pixel], absolute value for given canvas / page
				deb_rect_bot_right_y = 0; // [pixel], absolute value for given canvas / page
			}

	};
}

#endif // WORD_ATOM_H
