#ifndef PAGE_ATOM_H
#define PAGE_ATOM_H

#include "atom.h"

#define DEBUG_RECTANGLE_STROKE_THICKNESS_IN_PIXELS 1

namespace spelltex{

	class PageAtom : public Atom {
		public:
			PageAtom(spelltex::SpellTeX* context, spelltex::Atom* parent) : Atom(context, parent) {
				int page_width = 0;
				int page_height = 0;
				int top_margin = 0;
				int left_margin = 0;
				int bottom_margin = 0;
				int right_margin = 0;
				context->get_effective_page_dimensions(page_width, page_height, top_margin, left_margin, bottom_margin, right_margin);
				
				type = ATOM_PAGE;
				_parent = parent;
				_page_width = page_width;
				_page_height = page_height;
				_left_top_y = top_margin;
				_left_top_x = left_margin;
				_right_bot_y = page_height - bottom_margin;
				_right_bot_x = page_width - right_margin;
				_offset_y = top_margin;
				_offset_x = left_margin;
				if(_debug_color != nullptr)
					delete _debug_color;
				_debug_color = new QColor("green");

			}

			PageAtom(const Atom* original, Atom* parent) : Atom(original, parent) { // copy constructor
				// nothing special here, all done in Atom copy constructor
			}

			int copy_atom_tree(spelltex::Atom* parent){
				// create copy of itself and add it as child for given parent
				PageAtom* my_copy = new PageAtom(this, parent);
				my_copy->_debug_atom_tree_id = parent->_debug_atom_tree_id;
				std::list<spelltex::Atom*>::iterator it;
				for(it = this->_childs.begin(); it != this->_childs.end(); it++){
					(*it)->copy_atom_tree(my_copy);
				}
				return 0; // TODO STEX_SUCCESS retval
			}

                        void recalculate_size() override{
                                // sets internal variables x_pos_in_parent and y_pos_in_parent
                                // some atoms already know its size based only on glyph dimensions (like char atoms)
                                // some atoms depend on its childs to finish the dimension calculation
				spelltex::atom_constraints page_ac;
				page_ac.max_width = _right_bot_x - _left_top_x;
				page_ac.max_height = _right_bot_y - _left_top_y;
				vbox_size_recalculation(page_ac);
                        }

                        void recalculate_size(const spelltex::atom_constraints ac) override{
                                // sets internal variables x_pos_in_parent and y_pos_in_parent
                                // some atoms already know its size based only on glyph dimensions (like char atoms)
                                // some atoms depend on its childs to finish the dimension calculation
				// page atom implements its own atom constraints, thus ignore constraints from upper part of the atom tree
				recalculate_size();
                        }


			virtual void recalculate_position(){

				// calculate relative position between current atom and its parent
				double child_offset_y = _left_top_y; //_default_width * _scale;
				double child_offset_x = _left_top_x; //_default_height * _scale;
				double child_width = 0.0;
				double child_height = 0.0;
		
				for(auto it = _childs.begin(); it != _childs.end(); it++){
					(*it)->_offset_x = child_offset_x;
					(*it)->_offset_y = child_offset_y;
					(*it)->recalculate_position();
					child_width = (*it)->width();
					child_height = (*it)->height();
					child_offset_x += child_height;
				}
		
				double the_highest_y_coordinate = _default_width * _scale; // own width, in case without any childs
				double the_highest_x_coordinate = _default_height * _scale; // own height, in case without any childs;
				double the_highest_depth = 0.0;
				double the_highest_partial_height = 0.0;
		
				for(auto it = _childs.begin(); it != _childs.end(); it++){
					if(the_highest_y_coordinate < (*it)->_offset_y + (*it)->width())
						the_highest_y_coordinate = (*it)->_offset_y + (*it)->width();
					if(the_highest_x_coordinate < (*it)->_offset_x + (*it)->height())
						the_highest_x_coordinate = (*it)->_offset_x + (*it)->height();
					if(the_highest_depth < (*it)->_offset_x + (*it)->depth())
						the_highest_depth = (*it)->_offset_x + (*it)->depth();
					if(the_highest_partial_height < (*it)->_offset_x + (*it)->partial_height())
						the_highest_partial_height = (*it)->_offset_x + (*it)->partial_height();
				}
		
				//_width = the_highest_y_coordinate;
				//_height = the_highest_x_coordinate;
		
				// TODO _partial_height = the_highest_partial_height;
				// TODO _depth = the_highest_depth;
				// TODO ensure that following rule is valid: _height = partial_height() + depth();

			}

			void draw() {
				// assumption: internal variables _offset_x,y and _width, _height, _log are already set correctly
				if(_deleted == true)
					return;

				draw_all_childs();
	
				// calculate own size querying dimensions from childs
				std::list<spelltex::Atom*>::iterator it;
				//_width = 0;
				//_depth = 0;
				//_partial_height = 0;
				//_height = 0;
				double the_highest_child_of_word = 0;
				for(it = this->_childs.begin(); it != this->_childs.end(); it++){
					if(the_highest_child_of_word < (*it)->_height)
						the_highest_child_of_word = (*it)->_height;
				}
				_partial_height = the_highest_child_of_word;
				
				// used for debug rectangles, rendered OUTSIDE of SpellTeX, in absolute values, filled during SpellTeX rendering stage
				// in case of page, the debug rectangles show:
				// 1) outer borders of page, usually blank or reserved for footnotes, etc.
				// 2) inner borders of page, where paragraphs are placed
				//
				// first rectangle, inner page border, where paragraphs are placed
				deb_rect_top_left_x = _left_top_x; // [pixel], absolute value for given canvas / page
				deb_rect_top_left_y = _left_top_y; // [pixel], absolute value for given canvas / page
				deb_rect_mid_right_x = _right_bot_x; // [pixel], absolute value for given canvas / page
				deb_rect_mid_right_y = _right_bot_y; // [pixel], absolute value for given canvas / page

				// second rectangle, outer page borders
				// coordinate system for each page starts at coordinates (0,0) for left top corner
				deb_rect_mid_left_x = 0; // [pixel], absolute value for given canvas / page
				deb_rect_mid_left_y = 0; // [pixel], absolute value for given canvas / page
				deb_rect_bot_right_y = _page_height - DEBUG_RECTANGLE_STROKE_THICKNESS_IN_PIXELS; // [pixel], absolute value for given canvas / page
				deb_rect_bot_right_x = _page_width - DEBUG_RECTANGLE_STROKE_THICKNESS_IN_PIXELS; // [pixel], absolute value for given canvas / page
			}

	};
}

#endif // PAGE_ATOM_H


