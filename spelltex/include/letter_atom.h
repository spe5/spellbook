#ifndef LETTER_ATOM_H
#define LETTER_ATOM_H

#include <iostream>
#include <sstream>
#include <list>
#include <QPixmap>
#include <QPainter>
#include "utf8.h"

#include "common.h"
#include "atom.h"

namespace spelltex{

	class LetterAtom : public Atom {
		public:

			LetterAtom(SpellTeX* context, Atom * parent, const std::string chars) : Atom(context, parent) {
				type = ATOM_LETTER;
				if(parent == nullptr){
					std::string atom_name;
					atom_type2string(type, atom_name);
			        	std::cout << "[ERROR] " << atom_name << " has nullptr parent." << std::endl;
				}
				_parent = parent;
				_std_text = true;
				_letter = chars;

				_main_font = _parent->_main_font;
				get_letter_geometry();

				_glue_power = spelltex::glue_power::NOT_A_GLUE;
			}

			LetterAtom(SpellTeX* context, Atom * parent) : Atom(context, parent) {
				if(parent == nullptr)
					std::cout << "[ERROR] CharAtom has nullptr parent." << std::endl;
				_parent = parent;
			}

			LetterAtom(Atom* original, Atom* parent) : Atom(original, parent) { // copy constructor
				// nothing special here, all done in Atom copy constructor
				_std_text = true;
				_letter = (static_cast<LetterAtom*>(original))->_letter;
				_main_font = (static_cast<LetterAtom*>(original))->_main_font;
				get_letter_geometry();
			}

			int copy_atom_tree(spelltex::Atom* parent){
				// create copy of itself and add it as child for given parent
				LetterAtom* my_copy = new LetterAtom(this, parent);
				my_copy->_debug_atom_tree_id = parent->_debug_atom_tree_id;
				std::list<spelltex::Atom*>::iterator it;
				for(it = this->_childs.begin(); it != this->_childs.end(); it++){
					(*it)->copy_atom_tree(my_copy);
				}
				return 0; // TODO STEX_SUCCESS retval
			}


			void recalculate_size(spelltex::atom_constraints ac) override{
				// sets internal variables _width, _partial_height, _depth, _height
				// some atoms already know its size based only on glyph dimensions (like char atoms)
				// some atoms depend on its childs to finish the dimension calculation

				_width = _default_width * _scale;
				_partial_height = _default_partial_height * _scale;
				_depth = _default_depth * _scale;
				_height = _default_height * _scale;
				_size_calculation_finished = true;
				// TODO check for conflict between calculated dimensions and constraints, and propagate the error state upwards in the atom tree
			
			}

			void recalculate_position() override{
				// nothing to do
			}
	};
}

#endif // LETTER_ATOM_H
