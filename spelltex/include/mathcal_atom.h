#ifndef MATHCAL_ATOM_H
#define MATHCAL_ATOM_H

#include "atom.h"

namespace spelltex {

	class MathcalAtom : public Atom {
		public:

			MathcalAtom(SpellTeX* context, Atom* parent) : Atom(context, parent) {
				type = ATOM_MATHCAL;
				std::string atom_name;
				atom_type2string(type, atom_name);
                        	if(parent == nullptr)
                                	std::cout << "[ERROR] " << atom_name << " has nullptr parent." << std::endl;
                        	_parent = parent;
				_log << "[INFO] Created " << atom_name << " id=" << _id << ", pid=" << _parent->_id << std::endl;
				
				_glue_power = spelltex::glue_power::NOT_A_GLUE;
                        };

			MathcalAtom(Atom* original, Atom* parent) : Atom(original, parent) { // copy constructor
				// nothing special here, all done in Atom copy constructor
			}

			int copy_atom_tree(spelltex::Atom* parent){
				// create copy of itself and add it as child for given parent
				MathcalAtom* my_copy = new MathcalAtom(this, parent);
				my_copy->_debug_atom_tree_id = parent->_debug_atom_tree_id;
				std::list<spelltex::Atom*>::iterator it;
				for(it = this->_childs.begin(); it != this->_childs.end(); it++){
					(*it)->copy_atom_tree(my_copy);
				}
				return 0; // TODO STEX_SUCCESS retval
			}

			retval evaluate() override {
				// control sequence \mathcal requires 1 argument in { } brackets
				std::list<spelltex::Atom*> later_siblings;
				std::string atom_name;
				atom_type2string(type, atom_name);
				_log << "[INFO] Evaluating " << atom_name << " id=" << _id << ", pid=" << _parent->_id << std::endl;
			        if(get_later_siblings(later_siblings) != 0){
					_log << "[ERROR] Unable to get_later_siblings for MATHCAL_ATOM with id " << _id << std::endl;
					return retval::ERROR;
				}

				if(later_siblings.size() < 1){
					_log << "[ERROR] Unable to get at least 1 siblings for MATHCAL_ATOM with id " << _id << std::endl;
					return retval::ERROR;
				}

				// keep only 1th argument as text for typesettings, all other arguments (#1, #3, #4) are metadata, currently ignored
				std::list<spelltex::Atom*>::iterator it;
				std::list<spelltex::Atom*>::iterator it_1th_arg;
				QFont *mathcal_font = new QFont("cmsy10", 16);
				QFontInfo real_font(*mathcal_font);
				//std::cout << "[INFO] MathcalFont is actually " << real_font.family().toStdString() << " with size " << real_font.pointSize() << std::endl;
				QFontMetricsF *mathcal_metrics = new QFontMetricsF(*mathcal_font);
				bool found = false;
				for(it = _parent->_childs.begin(); it != _parent->_childs.end(); it++){
					if(*it == this){
						found = true;
						it_1th_arg = std::next(it, 1);
						(*it_1th_arg)->update_subtree_font(mathcal_font, mathcal_metrics);
						(*it)->_deleted = true;
					}

				}
				if(!found){
					_log << "[ERROR] Unable to locate this MATHCAL_ATOM (id=" << _id << ") between my parent childs." << std::endl;
					return retval::ERROR;
				}
                                retval ret = retval::SUCCESS;
                                for(auto child_it = _childs.begin(); child_it != _childs.end(); child_it++){
                                        ret = agg(ret, (*child_it)->evaluate());
                                }
                                return ret;
			};

			void recalculate_size(spelltex::atom_constraints ac) override{
				hbox_size_recalculation(ac);
			}

			virtual void recalculate_position(){
				// nothing to do
			}
	};

}

#endif // MATHCAL_ATOM_H
