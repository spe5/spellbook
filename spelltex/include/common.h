#ifndef COMMON_H
#define COMMON_H

#include <string>
#include <QFontDatabase>
#include <QFile>

namespace spelltex{
	
	// specified in Knuth's TeXBook 
	enum category{
		ESCAPE_CHARACTER = 0, // i.e. \ backslash
		BEGINNING_OF_GROUP = 1, // i.e. {
		END_OF_GROUP = 2, // i.e. }
		MATH_SHIFT = 3, // i.e. $
		ALIGNMENT_TAB = 4, // i.e. &
		END_OF_LINE = 5, // i.e. <return>
		PARAMETER = 6, // i.e. #
		SUPERSCRIPT = 7, // i.e. ^
		SUBSCRIPT = 8, // i.e. _
		IGNORED_CHARACTER = 9, // i.e. <null>
		SPACE = 10, // i.e. ' '
		LETTER = 11, // i.e. A-Z, a-z
		OTHER_CHARACTER = 12,
		ACTIVE_CHARACTER = 13, // i.e. ~
		COMMENT_CHARACTER = 14, // i.e. %
		INVALID_CHARACTER = 15, // i.e. <delete>
		CONTROL_SEQUENCE = 16, // composed item of ESCAPE_CHARACTER + LETTERS
		SPECIAL_CHARACTER = 17 // composed item of ESCAPE_CHARACTER + non-LETTER

	};

	enum atom_type {
		ATOM_GENERIC,
		ATOM_PAGE,
		ATOM_ROOT,
		ATOM_VERTICAL,
		ATOM_HORIZONTAL,
		ATOM_GROUP,
		ATOM_MATH,
		ATOM_WORD,
		ATOM_LETTER,
		ATOM_SPACE,
		ATOM_SUBSCRIPT,
		ATOM_DEFINITION,
		ATOM_MATHCAL,
		ATOM_MATH_OPERATOR
	};



	static void atom_type2string(atom_type type, std::string& buffer){
		switch(type){
			case ATOM_GENERIC:
				buffer.assign("GENERIC ATOM");
				return;
			case ATOM_PAGE:
				buffer.assign("PAGE ATOM");
				return;
			case ATOM_ROOT:
				buffer.assign("ROOT ATOM");
				return;
			case ATOM_VERTICAL:
				buffer.assign("VERTICAL ATOM");
				return;
			case ATOM_HORIZONTAL:
				buffer.assign("HORIZONTAL ATOM");
				return;
			case ATOM_GROUP:
				buffer.assign("GROUP ATOM");
				return;
			case ATOM_MATH:
				buffer.assign("MATH ATOM");
				return;
			case ATOM_WORD:
				buffer.assign("WORD ATOM");
				return;
			case ATOM_LETTER:
				buffer.assign("LETTER ATOM");
				return;
			case ATOM_SPACE:
				buffer.assign("SPACE ATOM");
				return;
			case ATOM_SUBSCRIPT:
				buffer.assign("SUBSCRIPT ATOM");
				return;
			case ATOM_DEFINITION:
				buffer.assign("DEFINITION ATOM");
				return;
			case ATOM_MATHCAL:
				buffer.assign("MATHCAL ATOM");
				return;
			case ATOM_MATH_OPERATOR:
				buffer.assign("MATH OPERATOR ATOM");
				return;
			default:
				buffer.assign("UNKNOWN ATOM");
		}
	};


	enum retval{
		SUCCESS = 1,
		ERROR = 0
	};

	retval agg(spelltex::retval first, spelltex::retval second);

	// settings for penalty calculation algorithms, mainly in page_metrics()
	#define POINTS_HBOX_OVERFLOW_SMALL 20
	#define POINTS_HBOX_OVERFLOW_BIG 40
	#define PENALTY_HBOX_OVERFLOW_SMALL 100
	#define PENALTY_HBOX_OVERFLOW_BIG 1000
	#define PENALTY_INFINITY 10000

	std::string print_chars(const spelltex::category cat, const std::string chars);
	std::string cat_to_str(const category cat);

	struct token{
		category cat;
		std::string chars;
	};

	static std::string gen_random(const int len){
		static const char alphanum[] =
	        "0123456789"
	        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	        "abcdefghijklmnopqrstuvwxyz";
		std::string tmp_s;
		tmp_s.reserve(len);
		
		for (int i = 0; i < len; ++i) {
			tmp_s += alphanum[rand() % (sizeof(alphanum) - 1)];
		}
		
		return tmp_s;
	};

	static void load_main_fonts(){
		{
			QFile fontFile("math/cmmi10.ttf");
		        fontFile.open(QFile::ReadOnly);
	        	QFontDatabase::addApplicationFontFromData(fontFile.readAll());
		}
		{
			QFile fontFile("math/cmsy10.ttf");
		        fontFile.open(QFile::ReadOnly);
	        	QFontDatabase::addApplicationFontFromData(fontFile.readAll());
		}
		//std::cout << "[INFO] Available Font Families" << std::endl;
		//QStringList available_fonts = QFontDatabase().families();
		//for (const QString &family : available_fonts) {
		//	std::cout << "[INFO]    " << family.toStdString() << std::endl;
		//}
	};

	struct atom_constraints{
		// used to define constraints and limits of atom - i.e. maximum width, etc.
		// used during atom size calculations, to send the constraints from parents to childs
		double max_width;
		double max_height;

	};

	enum glue_power{
		UNKNOWN = -2, // default value, causes error and abort for spacing algos
		NOT_A_GLUE = -1, // not a glue - i.e. letter
		FIXED_WIDTH = 0,  // non-strechable space
		SPACE_IN_TEXT = 1,  // typical space as part of text, between words
		BIG_SPACE_IN_TEXT = 2, // when requested bigger space in text, between words, etc.
		ALIGNMENT_SPACE = 3, // usually used in tables, end of paragraphs, etc.
		POWER_SPACE = 4
	};
}

#endif // COMMON_H
