#ifndef VERTICAL_ATOM_H
#define VERTICAL_ATOM_H

#include "atom.h"

namespace spelltex{

	class VerticalAtom : public Atom {
		public:
			VerticalAtom(SpellTeX* context, Atom * parent) : Atom(context, parent) {
				type = ATOM_VERTICAL;
				if(parent == nullptr){
					std::string atom_name;
					atom_type2string(type, atom_name);
					std::cout << "[ERROR] " << atom_name << " has nullptr parent." << std::endl;
				}
				_parent = parent;
				if(_debug_color != nullptr)
					delete _debug_color;
				_debug_color = new QColor("blue");
			};

			VerticalAtom(Atom* original, Atom* parent) : Atom(original, parent) { // copy constructor
				// nothing special here, all done in Atom copy constructor
			}

			int copy_atom_tree(spelltex::Atom* parent){
				// create copy of itself and add it as child for given parent
				VerticalAtom* my_copy = new VerticalAtom(this, parent);
				my_copy->_debug_atom_tree_id = parent->_debug_atom_tree_id;
				std::list<spelltex::Atom*>::iterator it;
				for(it = this->_childs.begin(); it != this->_childs.end(); it++){
					(*it)->copy_atom_tree(my_copy);
				}
				return 0; // TODO STEX_SUCCESS retval
			}


			void recalculate_size(const spelltex::atom_constraints ac) override{
				vbox_size_recalculation(ac);
			}

			virtual void recalculate_position(){
				//vertical_column_recalculation();

				// calculate relative position between current atom and its parent
				double child_offset_x = _default_width * _scale;
				double child_offset_y = _default_height * _scale;
				double child_width = 0.0;
				double child_height = 0.0;

				for(auto it = _childs.begin(); it != _childs.end(); it++){
					(*it)->_offset_x = child_offset_x;
					(*it)->_offset_y = child_offset_y;
					(*it)->recalculate_position();
					child_width = (*it)->width();
					child_height = (*it)->height();
					child_offset_x += child_height;
				}

				double the_highest_x_coordinate = _default_width * _scale; // own width, in case without any childs
				double the_highest_y_coordinate = _default_height * _scale; // own height, in case without any childs;
				double the_highest_depth = 0.0;
				double the_highest_partial_height = 0.0;

				for(auto it = _childs.begin(); it != _childs.end(); it++){
					if(the_highest_y_coordinate < (*it)->_offset_y + (*it)->width())
						the_highest_y_coordinate = (*it)->_offset_y + (*it)->width();
					if(the_highest_x_coordinate < (*it)->_offset_x + (*it)->height())
						the_highest_x_coordinate = (*it)->_offset_x + (*it)->height();
					if(the_highest_depth < (*it)->_offset_x + (*it)->depth())
						the_highest_depth = (*it)->_offset_x + (*it)->depth();
					if(the_highest_partial_height < (*it)->_offset_x + (*it)->partial_height())
						the_highest_partial_height = (*it)->_offset_x + (*it)->partial_height();
				}

				//_width = _parent->_page_width;
				_width = _parent->_right_bot_x - _left_top_x;

				_height = the_highest_y_coordinate;

				// TODO _partial_height = the_highest_partial_height;
				// TODO _depth = the_highest_depth;
				// TODO ensure that following rule is valid: _height = partial_height() + depth();

			}

			void draw() {
				// assumption: internal variables _offset_x,y and _width, _height, _log are already set correctly
				if(_deleted == true)
					return;

				draw_all_childs();
	
				// calculate own size querying dimensions from childs
				std::list<spelltex::Atom*>::iterator it;
				//_width = 0;
				//_depth = 0;
				//_partial_height = 0;
				//_height = 0;
				double the_highest_child_of_word = 0;
				for(it = this->_childs.begin(); it != this->_childs.end(); it++){
					if(the_highest_child_of_word < (*it)->_height)
						the_highest_child_of_word = (*it)->_height;
				}
				_partial_height = the_highest_child_of_word;
				
				// used for debug rectangles, rendered OUTSIDE of SpellTeX, in absolute values, filled during SpellTeX rendering stage
				double abs_offset_x = 0;
				double abs_offset_y = 0;
				get_abs_offset(abs_offset_x, abs_offset_y);
				deb_rect_top_left_x = abs_offset_x; // [pixel], absolute value for given canvas / page
				deb_rect_top_left_y = abs_offset_y; // [pixel], absolute value for given canvas / page
				deb_rect_mid_right_x = abs_offset_x + _width; // [pixel], absolute value for given canvas / page
				deb_rect_mid_right_y = abs_offset_y - _partial_height; // [pixel], absolute value for given canvas / page
				deb_rect_mid_left_x = 0; // [pixel], absolute value for given canvas / page
				deb_rect_mid_left_y = 0; // [pixel], absolute value for given canvas / page
				deb_rect_bot_right_x = 0; // [pixel], absolute value for given canvas / page
				deb_rect_bot_right_y = 0; // [pixel], absolute value for given canvas / page
			}
	};

}

#endif // VERTICAL_ATOM_H
