#ifndef GROUP_ATOM_H
#define GROUP_ATOM_H

#include "common.h"
#include <QRawFont>
#include "atom.h"


namespace spelltex {
	
	class GroupAtom : public Atom {
		// holds where we are in tree (on which depth-row), increased by BEGINNING_OF_GROUP, decreased by END_OF_GROUP
		public:
			GroupAtom(SpellTeX* context, Atom * parent) : Atom(context, parent) {
				type = ATOM_GROUP;

			}

			GroupAtom(Atom* original, Atom* parent) : Atom(original, parent) { // copy constructor
				// nothing special here, all done in Atom copy constructor
			}

			int copy_atom_tree(spelltex::Atom* parent){
				// create copy of itself and add it as child for given parent
				GroupAtom* my_copy = new GroupAtom(this, parent);
				my_copy->_debug_atom_tree_id = parent->_debug_atom_tree_id;
				std::list<spelltex::Atom*>::iterator it;
				for(it = this->_childs.begin(); it != this->_childs.end(); it++){
					(*it)->copy_atom_tree(my_copy);
				}
				return 0; // TODO STEX_SUCCESS retval
			}


			void recalculate_size(spelltex::atom_constraints ac) override{
				hbox_size_recalculation(ac);
			}
			
			void recalculate_position(){
                                // sets internal variables x_pos_in_parent and y_pos_in_parent
                                // some atoms already know its size based only on glyph dimensions (like char atoms)
                                // some atoms depend on its childs to finish the dimension calculation

                                horizontal_row_recalculation();

                        }

			Atom* space_signal() override{
                                return this;
                        }
	                Atom* group_end_signal() override{
                                if(_parent == nullptr)
                                        return this;

                                return _parent;
                        }

			void draw() {
				// assumption: internal variables _offset_x,y and _width, _height, _log are already set correctly
				if(_deleted == true)
					return;

				draw_all_childs();
	
				// calculate own size querying dimensions from childs
				std::list<spelltex::Atom*>::iterator it;
				//_width = 0;
				//_depth = 0;
				//_partial_height = 0;
				//_height = 0;
				double the_highest_child_of_word = 0;
				for(it = this->_childs.begin(); it != this->_childs.end(); it++){
					if(the_highest_child_of_word < (*it)->_height)
						the_highest_child_of_word = (*it)->_height;
				}
				_partial_height = the_highest_child_of_word;
				
				// used for debug rectangles, rendered OUTSIDE of SpellTeX, in absolute values, filled during SpellTeX rendering stage
				double abs_offset_x = 0;
				double abs_offset_y = 0;
				get_abs_offset(abs_offset_x, abs_offset_y);
				deb_rect_top_left_x = abs_offset_x; // [pixel], absolute value for given canvas / page
				deb_rect_top_left_y = abs_offset_y; // [pixel], absolute value for given canvas / page
				deb_rect_mid_right_x = abs_offset_x + _width; // [pixel], absolute value for given canvas / page
				deb_rect_mid_right_y = abs_offset_y - _partial_height; // [pixel], absolute value for given canvas / page
				deb_rect_mid_left_x = 0; // [pixel], absolute value for given canvas / page
				deb_rect_mid_left_y = 0; // [pixel], absolute value for given canvas / page
				deb_rect_bot_right_x = 0; // [pixel], absolute value for given canvas / page
				deb_rect_bot_right_y = 0; // [pixel], absolute value for given canvas / page
			}

	};

}
#endif // GROUP_ATOM_H
