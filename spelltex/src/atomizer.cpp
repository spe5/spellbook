#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include "../include/spelltex.h"
#include "letter_atom.h"
#include "math/greek_small_varphi_atom.h"
#include "math/greek_small_phi_atom.h"
#include "math/greek_small_omega_atom.h"
#include "math/latin_small_k_atom.h"
#include "math/latin_small_v_atom.h"
#include "math/latin_capital_w_atom.h"
#include "math/math_symbol_equiv_atom.h"
#include "math/math_symbol_vec_atom.h"
#include "mathcal_atom.h"
#include "space_atom.h"
#include "page_atom.h"
#include "root_atom.h"
#include "vertical_atom.h"
#include "horizontal_atom.h"
#include "definition_atom.h"
#include "math_atom.h"
#include "word_atom.h"
#include "group_atom.h"
#include "subscript_atom.h"



void spelltex::SpellTeX::log_current_cursor(spelltex::Atom* current_cursor, std::stringstream &log){
	log << "[INFO] ";
	std::string _atom_name;
	current_cursor->class_id(_atom_name);

	if(current_cursor->_childs.size() == 0){
		for(int z = 0; z < current_cursor->get_atom_level(); z++){
	                log << "  ";
		}
		if(current_cursor->_parent != nullptr){
			log << _atom_name << " (id=" << current_cursor->_id << ", parent=" << current_cursor->_parent->_id << ")" << std::endl;
		}else{
			log << _atom_name << " (id=" << current_cursor->_id << ")" << std::endl;
		}
	}else{
		for(int z = 0; z < current_cursor->_childs.back()->get_atom_level(); z++){
	                log << "  ";
		}
		std::string _child_name;
		current_cursor->_childs.back()->class_id(_child_name);
		log << _child_name << " (id=" << current_cursor->_childs.back()->_id << ", parent=" << current_cursor->_id << ")" << std::endl;
	}
}

void spelltex::SpellTeX::atomize(std::list<spelltex::token> my_tokens, spelltex::Atom* my_atom_tree, std::stringstream &log){
	// expects existing RootAtom in given my_atom_tree
	log.str(""); // clear previous data
	log << "[INFO] Atomization Start" << std::endl;
	spelltex::load_main_fonts();
	std::list<spelltex::token>::iterator it;
	spelltex::Atom* current_cursor = nullptr;
	log_current_cursor(my_atom_tree, log);

	spelltex::PageAtom* page_atom = new spelltex::PageAtom(this, my_atom_tree);
	my_atom_tree->_childs.push_back(page_atom);
	log_current_cursor(page_atom, log);

	spelltex::VerticalAtom* vertical_atom = new spelltex::VerticalAtom(this, page_atom);
	page_atom->_childs.push_back(vertical_atom);
	log_current_cursor(vertical_atom, log);

	spelltex::HorizontalAtom* horizontal_atom = new spelltex::HorizontalAtom(this, vertical_atom);
	vertical_atom->_childs.push_back(horizontal_atom);
	log_current_cursor(horizontal_atom, log);
	current_cursor = horizontal_atom;

	for(it = my_tokens.begin(); it != my_tokens.end(); it++){
                //log << log.str();
		log.str("");
		if((*it).cat == spelltex::category::LETTER){
			if(current_cursor->is_math_shift() == true && (*it).chars.compare("k") == 0){
				log << "[NEW ATOM] LATIN SMALL K ATOM (cat=LETTER & math_shift=true & chars=k)" << std::endl;
				spelltex::Atom* new_atom = new spelltex::LatinSmallKAtom(this, current_cursor);
				current_cursor->_childs.push_back(new_atom);
				log_current_cursor(current_cursor, log);
				continue;
			}
			if(current_cursor->is_math_shift() == true && (*it).chars.compare("v") == 0){
				log << "[NEW ATOM] LATIN SMALL V ATOM (cat=LETTER & math_shift=true & chars=v)" << std::endl;
				spelltex::Atom* new_atom = new spelltex::LatinSmallVAtom(this, current_cursor);
				current_cursor->_childs.push_back(new_atom);
				log_current_cursor(current_cursor, log);
				continue;
			}
			if(current_cursor->is_math_shift() == true && (*it).chars.compare("W") == 0){
				log << "[NEW ATOM] LATIN CAPITAL W ATOM (cat=LETTER & math_shift=true & chars=W)" << std::endl;
				spelltex::Atom* new_atom = new spelltex::LatinCapitalWAtom(this, current_cursor);
				current_cursor->_childs.push_back(new_atom);
				log_current_cursor(current_cursor, log);
				continue;
			}

			// ensure parent of every char atom is word
			if(current_cursor->type != spelltex::ATOM_WORD){
				spelltex::Atom* word_atom = new spelltex::WordAtom(this, horizontal_atom);
				horizontal_atom->_childs.push_back(word_atom);
				current_cursor = word_atom;
				log_current_cursor(current_cursor, log);
			}

			log << "[NEW ATOM] CHAR ATOM with chars=" << (*it).chars.c_str() << std::endl;
			spelltex::Atom* letter_atom = new spelltex::LetterAtom(this, current_cursor, (*it).chars.c_str());
			current_cursor->_childs.push_back(letter_atom);
			log_current_cursor(current_cursor, log);
			continue;

		}
		if((*it).cat == spelltex::category::OTHER_CHARACTER){
			log << "[NEW ATOM] CHAR ATOM with chars=" << (*it).chars.c_str() << "(cat=OTHER CHARACTER)" << std::endl;
			spelltex::Atom* letter_atom = new spelltex::LetterAtom(this, current_cursor, (*it).chars.c_str());
			current_cursor->_childs.push_back(letter_atom);
			log_current_cursor(current_cursor, log);
		}

		if((*it).cat == spelltex::category::SPACE){
			// end non-pair enviroments
			current_cursor = current_cursor->space_signal();

			log << "[NEW ATOM] SPACE ATOM" << std::endl;
			spelltex::Atom* space_atom = new spelltex::SpaceAtom(this, current_cursor);
			current_cursor->_childs.push_back(space_atom);
			log_current_cursor(space_atom, log);

			log << "[NEW ATOM] WORD ATOM" << std::endl;
			spelltex::Atom* new_word_atom = new spelltex::WordAtom(this, current_cursor);
			current_cursor->_childs.push_back(new_word_atom);
			current_cursor = new_word_atom;
			log_current_cursor(current_cursor, log);
		}
		if((*it).cat == spelltex::BEGINNING_OF_GROUP){
			spelltex::Atom* group_atom = new spelltex::GroupAtom(this, current_cursor);
			current_cursor->_childs.push_back(group_atom);
			current_cursor = group_atom;
			log_current_cursor(current_cursor, log);

			spelltex::Atom* new_word_atom = new spelltex::WordAtom(this, current_cursor);
			current_cursor->_childs.push_back(new_word_atom);
			current_cursor = new_word_atom;
			log_current_cursor(current_cursor, log);
		}
		if((*it).cat == spelltex::END_OF_GROUP){
			current_cursor = current_cursor->group_end_signal();
			current_cursor = current_cursor->space_signal();
		}
		if((*it).cat == spelltex::CONTROL_SEQUENCE){
			if((*it).chars.compare("\\definition") == 0){ // requires 4 arguments
				spelltex::Atom* new_atom = new spelltex::DefinitionAtom(this, current_cursor, (*it).chars.c_str());
				current_cursor->_childs.push_back(new_atom);
				log_current_cursor(current_cursor, log);
				continue;
			}
			if((*it).chars.compare("\\varphi") == 0){ // requires 0 arguments
				spelltex::Atom* new_atom = new spelltex::GreekSmallVarphiAtom(this, current_cursor);
				current_cursor->_childs.push_back(new_atom);
				log_current_cursor(current_cursor, log);
				continue;
			}
			if((*it).chars.compare("\\phi") == 0){ // requires 0 arguments
				spelltex::Atom* new_atom = new spelltex::GreekSmallPhiAtom(this, current_cursor);
				current_cursor->_childs.push_back(new_atom);
				log_current_cursor(current_cursor, log);
				continue;
			}
			if((*it).chars.compare("\\omega") == 0){ // requires 0 arguments
				spelltex::Atom* new_atom = new spelltex::GreekSmallOmegaAtom(this, current_cursor);
				current_cursor->_childs.push_back(new_atom);
				log_current_cursor(current_cursor, log);
				continue;
			}
			if((*it).chars.compare("\\mathcal") == 0){ // requires 1 argument, composed of atoms, whose font shall be switched to CMSY
				spelltex::Atom* new_atom = new spelltex::MathcalAtom(this, current_cursor);
				current_cursor->_childs.push_back(new_atom);
				log_current_cursor(current_cursor, log);
				continue;
			}
			if((*it).chars.compare("\\equiv") == 0){ // requires 0 arguments
				spelltex::Atom* new_atom = new spelltex::MathSymbolEquivAtom(this, current_cursor);
				current_cursor->_childs.push_back(new_atom);
				log_current_cursor(current_cursor, log);
				continue;
			}
			if((*it).chars.compare("\\vec") == 0){ // requires 0 arguments
				spelltex::Atom* new_atom = new spelltex::MathSymbolVecAtom(this, current_cursor);
				current_cursor->_childs.push_back(new_atom);
				log_current_cursor(current_cursor, log);
				continue;
			}
		}
		if((*it).cat == spelltex::MATH_SHIFT){
			if(current_cursor->is_math_shift() == false){
				spelltex::Atom* math_atom = new spelltex::MathAtom(this, current_cursor);
				current_cursor->_childs.push_back(math_atom);
				current_cursor = math_atom;
				log_current_cursor(current_cursor, log);
				continue;
			}
			if(current_cursor->is_math_shift() == true){
				current_cursor = current_cursor->math_end_signal();
				continue;
			}
		}
		if((*it).cat == spelltex::SPECIAL_CHARACTER){
			if((*it).chars.compare("\\[") == 0){
				// check for assumptions: group_level SHALL be 0 otherwise syntax problem

				vertical_atom = new spelltex::VerticalAtom(this, my_atom_tree);
				my_atom_tree->_childs.push_back(vertical_atom);
				current_cursor = vertical_atom;
				log_current_cursor(current_cursor, log);

				horizontal_atom = new spelltex::HorizontalAtom(this, current_cursor);
				current_cursor->_childs.push_back(horizontal_atom);
				current_cursor = horizontal_atom;
				log_current_cursor(current_cursor, log);

				spelltex::Atom* math_shift_atom = new spelltex::MathAtom(this, current_cursor);
				current_cursor->_childs.push_back(math_shift_atom);
				current_cursor = math_shift_atom;
				log_current_cursor(current_cursor, log);
				continue;

			}
			if((*it).chars.compare("\\]") == 0){
				// check for assumptions: group_level SHALL be 0 otherwise syntax problem

				vertical_atom = new spelltex::VerticalAtom(this, my_atom_tree);
				my_atom_tree->_childs.push_back(vertical_atom);
				current_cursor = vertical_atom;
				log_current_cursor(current_cursor, log);

				horizontal_atom = new spelltex::HorizontalAtom(this, current_cursor);
				current_cursor->_childs.push_back(horizontal_atom);
				current_cursor = horizontal_atom;
				log_current_cursor(current_cursor, log);
				continue;
			}

		}
		if((*it).cat == spelltex::SUBSCRIPT){
			if(current_cursor->_childs.empty()){
				log << "[ERROR] Subscript syntax violated" << std::endl;
				continue;
			}
		
			// subscript happens for last leaf, thus updating current cursor from parent to its last childs
			if(current_cursor->_childs.size() != 0){
				current_cursor = current_cursor->_childs.back();
			}

			// add Subscript atom as child for last leaf
			spelltex::Atom* new_atom = new spelltex::SubscriptAtom(this, current_cursor);
			current_cursor->_childs.push_back(new_atom);
			current_cursor = new_atom;
			log_current_cursor(current_cursor, log);

			// add Word atom as child of subscript
			spelltex::Atom* new_word_atom = new spelltex::WordAtom(this, current_cursor);
			current_cursor->_childs.push_back(new_word_atom);
			current_cursor = new_word_atom;
			log_current_cursor(current_cursor, log);
			continue;
		}
	}
        log << "[INFO] Atomization End" << std::endl;
}

spelltex::retval spelltex::agg(spelltex::retval first, spelltex::retval second){
	if((first == spelltex::retval::SUCCESS) and (second == spelltex::retval::SUCCESS)){
		return spelltex::retval::SUCCESS;
	}
	return spelltex::retval::ERROR;
}


bool spelltex::SpellTeX::evaluate(spelltex::Atom* my_atom_tree){
	if(my_atom_tree->evaluate() == spelltex::retval::SUCCESS){
		std::cout << "[INFO] Atom Tree evaluated SUCCESSFULLY." << std::endl;
		return true;
	}else{
		std::cout << "[ERROR] Atom Tree evaluated with ERROR." << std::endl;
		return false;
	}
}


