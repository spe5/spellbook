#include "common.h"
#include<QRawFont>
#include<QGlyphRun>
#include "atom.h"
#include "spelltex.h"

namespace spelltex {

	void Atom::initialize_size(){
		_default_width = 0;
		_default_height = 0;
		_default_depth = 0;
		_default_partial_height = 0;

		_width = 0;
		_partial_height = 0;
		_depth = 0;
		_height = 0;
		_ascent = 0.0;

		_size_calculation_finished = false;
	       	// true if atom size calculation successfully finished & _width, _height, etc. contains final values,
		// false if atom size is NOT known yet (or initialized)
	}


	void Atom::recalculate_size(const spelltex::atom_constraints ac){
		// sets internal variables _width, _partial_height, _depth, _height and _size_calculation_finished
		// invoke size recalculation on child atoms
		// example: for simple letter or glyph, it is _width = _default_width * _scale, for more complex composed atoms (equations, rows, pages) its different
	}


	void Atom::recalculate_size(){
		// sets internal variables _width, _partial_height, _depth, _height and _size_calculation_finished
		// invoke size recalculation on child atoms
		// example: for simple letter or glyph, it is _width = _default_width * _scale, for more complex composed atoms (equations, rows, pages) its different
	}


	void Atom::recalculate_size_recursively(spelltex::atom_constraints ac){
		// request size calculation from all childs
		for(auto it = _childs.begin(); it != _childs.end(); it++){
			(*it)->recalculate_size(ac);
		}
	}


	bool Atom::is_child_size_recalculation_done(){
		// return true if all childs have successfully calculated its size,
		// return false if some additional processing is needed
		for(auto it = _childs.begin(); it != _childs.end(); it++){
			if((*it)->_size_calculation_finished == false)
				return false;
		}
		return true;
	}


	spelltex::glue_power Atom::get_the_highest_glue_power_of_childs(){
		// find glues with highest priority - step 1: find highest glue priority
		spelltex::glue_power the_highest_priority_level = spelltex::glue_power::UNKNOWN;
		for(auto it = _childs.begin(); it != _childs.end(); it++){
			if(the_highest_priority_level < (*it)->_glue_power) // because definition of glue_power follows simple integer increment
				the_highest_priority_level = (*it)->_glue_power;
		}
		return the_highest_priority_level;
	}



	void Atom::sum_up_size_of_childs_horizontally(){
		for(auto it = _childs.begin(); it != _childs.end(); it++){
			_width += (*it)->_width;
			if(_partial_height < (*it)->_partial_height)
				_partial_height = (*it)->_partial_height;
			if(_depth < (*it)->_depth)
				_depth = (*it)->_depth;
		}

		// get total height, as composed from partial height and depth of different atoms
		_height = _partial_height + _depth;
	}
	

	void Atom::set_scale_of_the_most_powerful_spaces(double scale_of_spaces){
		// set scale of spaces
		for(auto it = _childs.begin(); it != _childs.end(); it++){
			if(get_the_highest_glue_power_of_childs() == (*it)->_glue_power){
				(*it)->_scale = scale_of_spaces;
				(*it)->_width = (*it)->_width * (*it)->_scale;
				(*it)->_size_calculation_finished = true;
			}
		}
	}


	void Atom::stretch_the_most_prominent_spaces(spelltex::atom_constraints ac){
		// calculate total width as sum of width of all childs, if size recalculation was successfully finished
		// additional algos to handle unresolved childs, namely space-like glue atoms
		if(is_child_size_recalculation_done())
			return;

		if(get_the_highest_glue_power_of_childs() < spelltex::glue_power::SPACE_IN_TEXT)
			return;

		// calculate current width of spaces with the highest priority
		double sum_of_space_widths = 0;
		for(auto it = _childs.begin(); it != _childs.end(); it++){
			if(get_the_highest_glue_power_of_childs() == (*it)->_glue_power){
				sum_of_space_widths += (*it)->_width;
			}
		}

		// calculate scale for spaces, based on remaining free space in line and number of spaces in it
		double scale_of_spaces = (ac.max_width - this->textual_width()) / sum_of_space_widths;

		set_scale_of_the_most_powerful_spaces(scale_of_spaces);
	}


	void Atom::hbox_size_recalculation(spelltex::atom_constraints ac){
		// template for recalculate_size()
		// sets internal variables _width, _partial_height, _depth, _height, _size_calculation_finished
		// some atoms already know its size based only on glyph dimensions (like char atoms)
		// some atoms depend on its childs to finish the dimension calculation
		// assumption: size of this atom depends ONLY on childs (no additional content added by this atom)

		initialize_size();

		recalculate_size_recursively(ac);

		stretch_the_most_prominent_spaces(ac);

		sum_up_size_of_childs_horizontally();

		// mark self as resolved (make sure that this atom always is resolved here!)
		if(is_child_size_recalculation_done())
			_size_calculation_finished = true;
	}

	void Atom::vbox_size_recalculation(spelltex::atom_constraints ac){
		// template for recalculate_size()
		// sets internal variables _width, _partial_height, _depth, _height, _size_calculation_finished
		// some atoms already know its size based only on glyph dimensions (like char atoms)
		// some atoms depend on its childs to finish the dimension calculation
		// assumption: size of this atom depends ONLY on childs (no additional content added by this atom)

		// buffers for _width, _partial_height, _depth, _height
		double vbox_width = 0.0;
		double vbox_height = 0.0;
		double vbox_partial_height = 0.0;
		double vbox_depth = 0.0;

		// request size calculation from all childs
		for(auto it = _childs.begin(); it != _childs.end(); it++){
			(*it)->recalculate_size(ac);
		}

		// calculate total width as sum of width of all childs, if size recalculation was successfully finished
		bool _all_child_size_calculation_finished = true;
		for(auto it = _childs.begin(); it != _childs.end(); it++){
			if((*it)->_size_calculation_finished == false){
				_all_child_size_calculation_finished = false;
			}
			vbox_height += (*it)->_height;
			if(vbox_width < (*it)->_width)
				vbox_width = (*it)->_width;
		}

		if(_all_child_size_calculation_finished == true)
			_size_calculation_finished = true;

		_width = vbox_width;
		_partial_height = vbox_height;
		_depth = 0; // it makes no sense for composed vbox to have "depth", thus setting default value of 0
		_height = vbox_height;
	}


}
