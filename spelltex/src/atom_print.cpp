#include "common.h"
#include "atom.h"
#include "spelltex.h"


#define TREE_COLUMN_ALIGNMENT 80
#define ID_COLUMN_ALIGNMENT 12
#define OFFSET_COLUMN_ALIGNMENT 4
#define SIZE_COLUMN_ALIGNMENT 4
#define DELETE_COLUMN_ALIGNMENT 3
#define SCALE_COLUMN_ALIGNMENT 4
#define FONT_COLUMN_ALIGNMENT 16

namespace spelltex {

	void Atom::get_str_type(std::string& buffer){
		atom_type2string(type, buffer);
	}


	void Atom::class_id(std::string& buffer){
		if(type == ATOM_LETTER)
			if(_std_text == true){
				std::string retval;
				atom_type2string(ATOM_LETTER, retval);
				buffer.assign(retval);
				buffer += ":";
				buffer += _letter;
				return;
			}
			if(_ttf_glyph == true){
				std::string retval;
				atom_type2string(ATOM_LETTER, retval);
				buffer.assign(retval);
				buffer += ":";
				buffer += _glyph_name;
				return;
			}
		// TODO if(me->type == ATOM_MATH_OPERATOR)
		std::string retval;
		atom_type2string(type, retval);
		buffer.assign(retval);
	}


	void Atom::get_log(std::stringstream &log){
		log << _log.str();
	        _log.str("");
		for(auto it = _childs.begin(); it != _childs.end(); it++){
			(*it)->get_log(log);
		}
	}


	void Atom::print_me(std::stringstream &log, const bool del){
		if(_deleted == true && del == false)
			return;

		std::stringstream buff;
		buff << std::left << std::setw(TREE_COLUMN_ALIGNMENT) << std::setfill('=') << "==== ATOM ====";
		buff << "|";
		buff << std::left << std::setw(ID_COLUMN_ALIGNMENT) << std::setfill('=') << "==== ID ====";
		buff << "|";
		buff << std::left << std::setw(ID_COLUMN_ALIGNMENT) << std::setfill('=') << " PARENT ID ";
		buff << "|";
		buff << std::left << std::setw(OFFSET_COLUMN_ALIGNMENT) << std::setfill('=') << " Y ";
		buff << "|";
		buff << std::left << std::setw(OFFSET_COLUMN_ALIGNMENT) << std::setfill('=') << " X ";
		buff << "|";
		buff << std::left << std::setw(OFFSET_COLUMN_ALIGNMENT) << std::setfill('=') << " AY ";
		buff << "|";
		buff << std::left << std::setw(OFFSET_COLUMN_ALIGNMENT) << std::setfill('=') << " AX ";
		buff << "|";
		buff << std::left << std::setw(SIZE_COLUMN_ALIGNMENT) << std::setfill('=') << " W ";
		buff << "|";
		buff << std::left << std::setw(SIZE_COLUMN_ALIGNMENT) << std::setfill('=') << " H ";
		buff << "|";
		buff << std::left << std::setw(SIZE_COLUMN_ALIGNMENT) << std::setfill('=') << " PH ";
		buff << "|";
		buff << std::left << std::setw(SIZE_COLUMN_ALIGNMENT) << std::setfill('=') << " D ";
		buff << "|";
		buff << std::left << std::setw(DELETE_COLUMN_ALIGNMENT) << std::setfill('=') << "DEL";
		buff << "|";
		buff << std::left << std::setw(SCALE_COLUMN_ALIGNMENT) << std::setfill('=') << " S ";
		buff << "|";
		buff << std::left << std::setw(FONT_COLUMN_ALIGNMENT) << std::setfill('=') << "==== FONT ";
		buff << "|";

		log << buff.str() << std::endl;

		print_me(log, 0, del);
	}


	void Atom::print_me(std::stringstream &log, const int level, const bool del) {
		if(_deleted == true && del == false)
			return;

		std::stringstream buff;
		for(int i = 0; i < level; i++)
			buff << " ";
		std::string parent_id("nullptr");
		if(_parent != nullptr)
			parent_id = _parent->_id;

		//std::string letter_buffer("nullptr");
		//if(_letter != nullptr)
		//	letter_buffer = _letter->toStdString();

		std::string atom_name;
		class_id(atom_name);
		buff << atom_name;
		log << std::left << std::setw(TREE_COLUMN_ALIGNMENT) << std::setfill(' ') << buff.str();
		log << "|" << std::left << std::setw(ID_COLUMN_ALIGNMENT) << std::setfill(' ') << _id;
		log << "|" << std::left << std::setw(ID_COLUMN_ALIGNMENT) << std::setfill(' ') << parent_id;
		double abs_offset_x, abs_offset_y;
		get_abs_offset(abs_offset_x,abs_offset_y);
		log << "|" << std::left << std::setw(OFFSET_COLUMN_ALIGNMENT) << std::setfill(' ') << _offset_y;
		log << "|" << std::left << std::setw(OFFSET_COLUMN_ALIGNMENT) << std::setfill(' ') << _offset_x;
		log << "|" << std::left << std::setw(OFFSET_COLUMN_ALIGNMENT) << std::setfill(' ') << abs_offset_y;
		log << "|" << std::left << std::setw(OFFSET_COLUMN_ALIGNMENT) << std::setfill(' ') << abs_offset_x;
		log << "|" << std::left << std::setw(SIZE_COLUMN_ALIGNMENT) << std::setfill(' ') << _width;
		log << "|" << std::left << std::setw(SIZE_COLUMN_ALIGNMENT) << std::setfill(' ') << _height;
		log << "|" << std::left << std::setw(SIZE_COLUMN_ALIGNMENT) << std::setfill(' ') << _partial_height;
		log << "|" << std::left << std::setw(SIZE_COLUMN_ALIGNMENT) << std::setfill(' ') << _depth;
		log << "|" << std::left << std::setw(DELETE_COLUMN_ALIGNMENT) << std::setfill(' ') << _deleted;
		log << "|" << std::left << std::setw(SCALE_COLUMN_ALIGNMENT) << std::setfill(' ') << _scale;
		log << "|" << std::left << std::setw(FONT_COLUMN_ALIGNMENT) << std::setfill(' ') << _default_main_font;
	        log << "|" << std::endl; // " l='" << letter << "'" << std::endl;

		for(auto it = _childs.begin(); it != _childs.end(); it++){
			(*it)->print_me(log, level + 2, del);
		}
	}

}
