#include "common.h"
#include<QRawFont>
#include<QGlyphRun>
#include "atom.h"
#include "spelltex.h"

namespace spelltex {

	void Atom::initialize_position(){
		// x axis is horizontal, y axis is vertical
		_offset_x = 0.0;
		_offset_y = 0.0;

		// used for debug rectangles, rendered OUTSIDE of SpellTeX, in absolute values, filled during SpellTeX rendering stage
		deb_rect_top_left_x = 0; // [pixel], absolute value for given canvas / page
		deb_rect_top_left_y = 0; // [pixel], absolute value for given canvas / page
		deb_rect_mid_right_x = 0; // [pixel], absolute value for given canvas / page
		deb_rect_mid_right_y = 0; // [pixel], absolute value for given canvas / page
		deb_rect_mid_left_x = 0; // [pixel], absolute value for given canvas / page
		deb_rect_mid_left_y = 0; // [pixel], absolute value for given canvas / page
		deb_rect_bot_right_x = 0; // [pixel], absolute value for given canvas / page
		deb_rect_bot_right_y = 0; // [pixel], absolute value for given canvas / page
	}

	void Atom::recalculate_position(){
	// sets internal variables _offset_x and _offset_y
	// some atoms already know its size based only on glyph dimensions (like char atoms)
	// some atoms depend on its childs to finish the dimension calculation
	// by default, do NOT use any geometry-specific recalculation (horizontal or vertical), but 'do nothing' instead
		no_change_recalculation();
	}


	void Atom::horizontal_row_recalculation(){
		// template for recalculate_size_and_position()
		// sets internal variables x_pos_in_parent and y_pos_in_parent
		// some atoms already know its size based only on glyph dimensions (like char atoms)
		// some atoms depend on its childs to finish the dimension calculation

		// calculate relative position between current atom and its parent
		double child_offset_x = 0.0;//_default_width * _scale;
		double child_offset_y = 0.0;//_default_height * _scale;
		double child_width = 0.0;
		double child_height = 0.0;

		for(auto it = _childs.begin(); it != _childs.end(); it++){
			(*it)->_offset_x = child_offset_x;
			(*it)->_offset_y = child_offset_y;
			(*it)->recalculate_position();
			child_width = (*it)->width();
			child_height = (*it)->height();
			child_offset_x += child_width;
		}
		//double the_highest_x_coordinate = _default_width * _scale; // own width, in case without any childs
		//double the_highest_y_coordinate = _default_height * _scale; // own height, in case without any childs;
		//double the_highest_depth = 0.0;
		//double the_highest_partial_height = 0.0;

		//for(auto it = _childs.begin(); it != _childs.end(); it++){
		//	if(the_highest_x_coordinate < (*it)->_offset_x + (*it)->width())
		//		the_highest_x_coordinate = (*it)->_offset_x + (*it)->width();
		//	if(the_highest_y_coordinate < (*it)->_offset_y + (*it)->height())
		//		the_highest_y_coordinate = (*it)->_offset_y + (*it)->height();
		//	if(the_highest_depth < (*it)->_offset_y + (*it)->depth())
		//		the_highest_depth = (*it)->_offset_y + (*it)->depth();
		//	if(the_highest_partial_height < (*it)->_offset_y + (*it)->partial_height())
		//		the_highest_partial_height = (*it)->_offset_y + (*it)->partial_height();
		//}

		//_width = the_highest_x_coordinate;
		//_height = the_highest_y_coordinate;

		// TODO _partial_height = the_highest_partial_height;
		// TODO _depth = the_highest_depth;
		// TODO ensure that following rule is valid: _height = partial_height() + depth();
	
	}


	void Atom::no_change_recalculation(){
		// template for recalculate_size_and_position()
		// sets internal variables x_pos_in_parent and y_pos_in_parent
		// some atoms already know its size based only on glyph dimensions (like char atoms)
		// some atoms depend on its childs to finish the dimension calculation

		// calculate relative position between current atom and its parent
		double child_offset_y = 0.0 * _scale; // do NOT consider own width and height - no change here
		double child_offset_x = 0.0 * _scale;
		double child_width = 0.0;
		double child_height = 0.0;

		for(auto it = _childs.begin(); it != _childs.end(); it++){
			(*it)->_offset_x = child_offset_x;
			(*it)->_offset_y = child_offset_y;
			(*it)->recalculate_position();
			// do NOT consider width and height of childs - no change
		}
		double the_highest_x_coordinate = _default_width * _scale; // own width, in case without any childs
		double the_highest_y_coordinate = _default_height * _scale; // own height, in case without any childs;
		double the_highest_depth = 0.0;
		double the_highest_partial_height = 0.0;

		for(auto it = _childs.begin(); it != _childs.end(); it++){
			if(the_highest_x_coordinate < (*it)->_offset_x + (*it)->width())
				the_highest_x_coordinate = (*it)->_offset_x + (*it)->width();
			if(the_highest_y_coordinate < (*it)->_offset_y + (*it)->height())
				the_highest_y_coordinate = (*it)->_offset_y + (*it)->height();
			if(the_highest_depth < (*it)->_offset_y + (*it)->depth())
				the_highest_depth = (*it)->_offset_y + (*it)->depth();
			if(the_highest_partial_height < (*it)->_offset_y + (*it)->partial_height())
				the_highest_partial_height = (*it)->_offset_y + (*it)->partial_height();
		}

		_width = the_highest_x_coordinate;
		_height = the_highest_y_coordinate;

		// TODO _partial_height = the_highest_partial_height;
		// TODO _depth = the_highest_depth;
		// TODO ensure that following rule is valid: _height = partial_height() + depth();
	
	}


	void Atom::vertical_column_recalculation(){

		// calculate relative position between current atom and its parent
		double child_offset_y = 0.0; //_default_width * _scale;
		double child_offset_x = 0.0; //_default_height * _scale;
		double child_width = 0.0;
		double child_height = 0.0;

		for(auto it = _childs.begin(); it != _childs.end(); it++){
			(*it)->_offset_x = child_offset_x;
			(*it)->_offset_y = child_offset_y;
			(*it)->recalculate_position();
			child_width = (*it)->width();
			child_height = (*it)->height();
			child_offset_x += child_height;
		}

		double the_highest_y_coordinate = _default_width * _scale; // own width, in case without any childs
		double the_highest_x_coordinate = _default_height * _scale; // own height, in case without any childs;
		double the_highest_depth = 0.0;
		double the_highest_partial_height = 0.0;

		for(auto it = _childs.begin(); it != _childs.end(); it++){
			if(the_highest_y_coordinate < (*it)->_offset_y + (*it)->width())
				the_highest_y_coordinate = (*it)->_offset_y + (*it)->width();
			if(the_highest_x_coordinate < (*it)->_offset_x + (*it)->height())
				the_highest_x_coordinate = (*it)->_offset_x + (*it)->height();
			if(the_highest_depth < (*it)->_offset_x + (*it)->depth())
				the_highest_depth = (*it)->_offset_x + (*it)->depth();
			if(the_highest_partial_height < (*it)->_offset_x + (*it)->partial_height())
				the_highest_partial_height = (*it)->_offset_x + (*it)->partial_height();
		}

		_width = the_highest_y_coordinate;
		_height = the_highest_x_coordinate;

		// TODO _partial_height = the_highest_partial_height;
		// TODO _depth = the_highest_depth;
		// TODO ensure that following rule is valid: _height = partial_height() + depth();
	}



}
