include <QPainter>
#include <iostream>
#include "../include/spelltex.h"
#include "space_atom.h"
#include "page_atom.h"
#include "vertical_atom.h"
#include "horizontal_atom.h"

void spelltex::SpellTeX::finalize_horizontal_row(spelltex::Atom* horizontal_atom, double target_width){
	// calculate scaling factor
	double scale_factor = 1.0;
	double unscaled_total_width = target_width; // horizontal_atom->width();
	double unscaled_textual_width = horizontal_atom->textual_width();
	int number_of_spaces = horizontal_atom->count_spaces() - 1; // ignore space at the and of the line
	scale_factor = (unscaled_total_width - unscaled_textual_width) / spelltex::SpaceAtom::_default_width / number_of_spaces;
	std::cout << "[INFO] Total Width " << unscaled_total_width << " Textual Width " << unscaled_textual_width << " Default Space Width " << spelltex::SpaceAtom::_default_width << " Spaces " << number_of_spaces << " Scale Factor " << scale_factor << std::endl;
	// scale all spaces to fill the whole row
	horizontal_atom->scale_spaces(scale_factor);
}


void spelltex::SpellTeX::classic_page_layout(spelltex::Atom* main_atom_tree, spelltex::Atom* &page_atom_tree){
	if(main_atom_tree == nullptr){
		std::cout << "[ERROR] Classic page layout: Empty main atom tree. Aborted." << std::endl;
		page_atom_tree = nullptr;
		return;
	}

	if(page_atom_tree != nullptr){
		std::cout << "[ERROR] Classic page layout: Got non-empty buffer - its internal bug. Aborted." << std::endl;
		return;
	}

	// create new stucture of the page
	spelltex::Atom* new_page = new spelltex::PageAtom(this, main_atom_tree->_page_width, main_atom_tree->_page_height, main_atom_tree->_left_top_x, main_atom_tree->_left_top_y, main_atom_tree->_right_bot_x, main_atom_tree->_right_bot_y);
	
	// analyze existing structure of given atom tree
	if(main_atom_tree->_childs.size() == 0){
		std::cout << "[ERROR] Maximum page layout: No vertical atom in main atom tree. Aborted." << std::endl;
		return;
	}

	spelltex::Atom* existing_vertical_atom = nullptr;
	for(auto vert_it = main_atom_tree->_childs.begin(); vert_it != main_atom_tree->_childs.end(); vert_it++){
		existing_vertical_atom = *vert_it;
		spelltex::Atom* new_vertical_atom = new spelltex::VerticalAtom(this, new_page);
		new_page->_childs.push_back(new_vertical_atom);
		spelltex::Atom* current_vertical_atom = new_vertical_atom;

		if(existing_vertical_atom->_childs.size() == 0){
			std::cout << "[WARN] Maximum page layout: No horizontal atom in vertical atom." << std::endl;
		}
		spelltex::Atom* existing_horizontal_atom = nullptr;
		for(auto hoz_it = existing_vertical_atom->_childs.begin(); hoz_it != existing_vertical_atom->_childs.end(); hoz_it++){
			existing_horizontal_atom = *hoz_it;
			spelltex::Atom* new_horizontal_atom = new spelltex::HorizontalAtom(this, current_vertical_atom);
			current_vertical_atom->_childs.push_back(new_horizontal_atom);
	
			// translate atoms from existing tree to new page tree
			//
			float current_x = new_page->_left_top_x;
			std::list<spelltex::Atom*>::iterator data_it;
			spelltex::Atom* current_horizontal_atom = new_horizontal_atom;
			for(data_it = existing_horizontal_atom->_childs.begin(); data_it != existing_horizontal_atom->_childs.end(); data_it++){
				spelltex::Atom* data_atom = (*data_it);
				if(current_x + data_atom->width() >= new_page->_right_bot_x){
					// align the text in horizontal row
					finalize_horizontal_row(current_horizontal_atom, new_page->_right_bot_x - new_page->_left_top_x);
		
					// add new horizontal row for next atoms
					current_horizontal_atom = new spelltex::HorizontalAtom(this, current_vertical_atom);
					current_vertical_atom->_childs.push_back(current_horizontal_atom);
					current_x = new_page->_left_top_x;
				}
				new_vertical_atom->_childs.back()->_childs.push_back(data_atom);
				data_atom->_parent = current_horizontal_atom;
				current_x += data_atom->width();
			}
			// do NOT align the text in horizontal row, because this is last row
			//finalize_horizontal_row(current_horizontal_atom, right_bot_x - left_top_x);

			// add new horizontal row for next atoms
			current_horizontal_atom = new spelltex::HorizontalAtom(this, current_vertical_atom);
			current_vertical_atom->_childs.push_back(current_horizontal_atom);
			current_x = new_page->_left_top_x;
		}

	}
	page_atom_tree = new_page;
}
