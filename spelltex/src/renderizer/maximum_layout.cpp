#include <QPainter>
#include <iostream>
#include "../include/spelltex.h"
#include "page_atom.h"
#include "vertical_atom.h"
#include "horizontal_atom.h"



/*void spelltex::SpellTeX::maximum_page_layout(spelltex::Atom* main_atom_tree, spelltex::Atom* &page_atom_tree){
	// set of atoms, which can cover whole page AND violate SpellTeX overfull-related rules
	// used as first iteration when looking for optimum set of atoms for the page
	// next iterations remove last atom and recalulcuate metrics
	if(main_atom_tree == nullptr){
		std::cout << "[ERROR] Maximum page layout: Empty main atom tree. Aborted." << std::endl;
		page_atom_tree = nullptr;
		return;
	}

	if(page_atom_tree != nullptr){
		std::cout << "[ERROR] Maximum page layout: Provided buffer for tree is NOT empty. Aborted." << std::endl;
		return;
	}

	// create new stucture of the page
	spelltex::Atom* new_page = new spelltex::PageAtom(this, main_atom_tree->_page_width, main_atom_tree->_page_height, main_atom_tree->_left_top_x, main_atom_tree->_left_top_y, main_atom_tree->_right_bot_x, main_atom_tree->_right_bot_y);
	
	spelltex::Atom* new_vertical_atom = new spelltex::VerticalAtom(this, new_page);
	new_page->_childs.push_back(new_vertical_atom);

	spelltex::Atom* new_horizontal_atom = new spelltex::HorizontalAtom(this, new_vertical_atom);
	new_vertical_atom->_childs.push_back(new_horizontal_atom);

	// analyze existing structure of given atom tree
	if(main_atom_tree->_childs.size() == 0){
		std::cout << "[ERROR] Maximum page layout: No vertical atom in main atom tree. Aborted." << std::endl;
		return;
	}
	spelltex::Atom* existing_vertical_atom = main_atom_tree->_childs.front();
	if(existing_vertical_atom->_childs.size() == 0){
		std::cout << "[ERROR] Maximum page layout: No horizontal atom in main atom tree. Aborted." << std::endl;
		return;
	}
	spelltex::Atom* existing_horizontal_atom = existing_vertical_atom->_childs.front();

	// translate atoms from existing tree to new page tree
	float current_x = new_page->_left_top_x;
	std::list<spelltex::Atom*>::iterator data_it;
	spelltex::Atom* current_horizontal_atom = new_horizontal_atom;
	spelltex::Atom* current_vertical_atom = new_vertical_atom;
	for(data_it = existing_horizontal_atom->_childs.begin(); data_it != existing_horizontal_atom->_childs.end(); data_it++){
		spelltex::Atom* data_atom = (*data_it);
		new_vertical_atom->_childs.back()->_childs.push_back(data_atom);
		current_x += data_atom->width();
		if(current_x >= new_page->_right_bot_x){
			current_horizontal_atom = new spelltex::HorizontalAtom(this, current_vertical_atom);
			current_vertical_atom->_childs.push_back(current_horizontal_atom);
			current_x = new_page->_left_top_x;
		}
	}

	page_atom_tree = new_page;
}*/


