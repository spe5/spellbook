#include <QPainter>
#include <iostream>
#include "../include/spelltex.h"


int spelltex::SpellTeX::horizontal_metrics(spelltex::Atom* horizontal_atom_tree, const float ideal_width){
	// expects structure of gival horizontal_atom_tree as: horizontal -> chars, spaces, etc.
	// follows rules from page_metrics
	float current_x = 0;
	int penalty = 0;
	int number_of_childs = 0;
	std::list<spelltex::Atom*>::iterator data_it;
	for(data_it = (*horizontal_atom_tree)._childs.begin(); data_it != (*horizontal_atom_tree)._childs.end(); data_it++){
		number_of_childs++;
		float atom_end_x = current_x + (*data_it)->width();
		int additional_penalty = 0;
		//float atom_end_y = current_y + leaf_atom->height;
		if(atom_end_x > ideal_width)
			if(atom_end_x <= ideal_width + (POINTS_HBOX_OVERFLOW_BIG - POINTS_HBOX_OVERFLOW_SMALL))
				additional_penalty += PENALTY_HBOX_OVERFLOW_SMALL;
			else
				additional_penalty += PENALTY_HBOX_OVERFLOW_BIG;
		std::cout << "[DEBUG] Horizontal metrics: New atom found in a horizontal atom at position " << current_x << " ending at " << atom_end_x << " and with additional penalty " << additional_penalty  << std::endl;
		current_x = atom_end_x;
		penalty += additional_penalty;
		if(penalty > PENALTY_INFINITY)
			penalty = PENALTY_INFINITY;
	}
	std::cout << "[DEBUG] Horizontal metrics: number of childs " << number_of_childs << ", total_penalty " << penalty << std::endl;
	return penalty;
}


int spelltex::SpellTeX::page_metrics(spelltex::Atom* page_atom_tree){
	// expectes structure of given page_atom_tree as: page -> vertical boxes -> horizontal boxes -> chars, spaces, etc.
	// penalty is 0 when all SpellTeX rules are met
	// penalty is PENALTY_HBOX_OVERFLOW_SMALL when last symbol of one row on page crosses border bigger than 0 AND less than POINTS_HBOX_OVERFLOW_SMALL points
	// penalty is PENALTY_HBOX_OVERFLOW_BIG when last symbol of one row on page crosses border more than POINTS_HBOX_OVERFLOW_SMALL points
	// penalty is PENALTY_INFINITY when last symbol of one row on page crosses border more than POINTS_HBOX_OVERFLOW_BIG points

	int penalty = 0;

	std::list<spelltex::Atom*>::iterator vert_it;
	float current_y = 0;
	float current_x = 0;
	float ideal_width = page_atom_tree->_page_width;
	float ideal_height = page_atom_tree->_page_height;
        for(vert_it = page_atom_tree->_childs.begin(); vert_it != page_atom_tree->_childs.end(); vert_it++){
		std::cout << "[DEBUG] Page metrics: New vertical atom found." << std::endl;
		spelltex::Atom* vertical_atom = (*vert_it);
		std::list<spelltex::Atom*>::iterator horiz_it;
		for(horiz_it = (*vertical_atom)._childs.begin(); horiz_it != (*vertical_atom)._childs.end(); horiz_it++){
			std::cout << "[DEBUG] Page metrics: New horizontal atom found." << std::endl;
			spelltex::Atom* horizontal_atom = (*horiz_it);
			penalty += horizontal_metrics(horizontal_atom, ideal_width);
		}
        }


	// penalty for horizontal overbox
	
	// penalty for horizontal underbox
	
	// 
	return penalty;
}


