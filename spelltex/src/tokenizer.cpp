#include <iostream>
#include <fstream>
#include <cassert>
#include <cstring>
#include "../include/spelltex.h"

int spelltex::SpellTeX::tokenize(const std::string latex_source, std::list<spelltex::token>& tokens){
	std::string source = latex_source;
	std::string::iterator it = source.begin();
	bool comment = false;
	bool control_sequence = false;
	bool single_special_char = false;
	bool process_char_again = false;
	std::string control_sequence_name = "";
	char buffer[4];
	std::string s_buffer;
	while(it != source.end()){ // && counter < 10){
		if(process_char_again == false){
			uint32_t utf8_char = utf8::next(it, source.end());
			std::memcpy(buffer, &utf8_char, 4);
		}else{
			process_char_again = false;
		}
		spelltex::category cat = spelltex::category::OTHER_CHARACTER; // default char type

		if(buffer[0] >= 65 and buffer[0] <= 90){
			s_buffer = buffer;
			cat = spelltex::category::LETTER;
		}
		if(buffer[0] >= 97 and buffer[0] <= 122){
			s_buffer = buffer;
			cat = spelltex::category::LETTER;
		}

		if(control_sequence == true){
			if(cat == spelltex::category::LETTER){ // chars of longer control sequence
				control_sequence_name += buffer;
				single_special_char = false;
				continue;
			}else{
				if(single_special_char == true){ // escaped single-char 
					control_sequence_name += buffer;
					control_sequence = false;
					single_special_char = false;
					s_buffer = control_sequence_name;
					cat = spelltex::category::SPECIAL_CHARACTER;

				}else{ // ending of a control sequence
					control_sequence = false;
					process_char_again = true;
					s_buffer = control_sequence_name;
					cat = spelltex::category::CONTROL_SEQUENCE;
				}
			}
		}else{
	
			if(buffer[0] == '{'){
				s_buffer = buffer;
				cat = spelltex::category::BEGINNING_OF_GROUP;
			}
			if(buffer[0] == '}'){
				s_buffer = buffer;
				cat = spelltex::category::END_OF_GROUP;
			}
			if(buffer[0] == 32){
				s_buffer = buffer;
				cat = spelltex::category::SPACE;
			}
			if(buffer[0] == '$'){
				s_buffer = buffer;
				cat = spelltex::category::MATH_SHIFT;
			}
			if(buffer[0] == '_'){
				s_buffer = buffer;
				cat = spelltex::category::SUBSCRIPT;
			}
			if(buffer[0] == '^'){
				s_buffer = buffer;
				cat = spelltex::category::SUPERSCRIPT;
			}
		
			// skip all tokens between comment symbol and end of line
			if(buffer[0] == '%'){
				comment = true;
			}
			if(buffer[0] == '\n'){
				comment = false;
				s_buffer = buffer;
				cat = spelltex::category::END_OF_LINE;
			}
			if(comment == true){
				continue;
			}
	
			if(buffer[0] == '\\'){
				cat = spelltex::category::ESCAPE_CHARACTER;
				control_sequence = true;
				control_sequence_name = "\\";
				single_special_char = true;
				continue;
			}
			s_buffer = buffer;
		}

		spelltex::token this_token;
		this_token.cat = cat;
		this_token.chars = s_buffer;
		tokens.push_back(this_token);
	}

	std::cout << "[INFO] Tokenize" << std::endl;
	return 0;
}

bool spelltex::SpellTeX::valid_utf8_file(const char* file_name){
	std::ifstream ifs(file_name);
	if (!ifs)
		return false; // even better, throw here

	std::istreambuf_iterator<char> it(ifs.rdbuf());
	std::istreambuf_iterator<char> eos;

	return utf8::is_valid(it, eos);
}

std::string spelltex::print_chars(const spelltex::category cat, const std::string chars){
	if(cat == spelltex::category::LETTER)
		return chars;
	if(cat == spelltex::category::END_OF_LINE)
		return "<return>";
	if(cat == spelltex::category::ESCAPE_CHARACTER)
		return "\\";
	if(cat == spelltex::category::BEGINNING_OF_GROUP)
		return "{";
	if(cat == spelltex::category::END_OF_GROUP)
		return "}";
	if(cat == spelltex::category::SPACE)
		return " ";
	if(chars[0] > 32 && chars[0] < 65) // !, ", #, $, %, &, ', (, ), *, +, ,, /, 0..9, :, ;, <, =, >, ?, @,
		return chars;
	if(cat == spelltex::category::MATH_SHIFT)
		return "$";
	if(cat == spelltex::category::SUBSCRIPT)
		return "_";
	if(cat == spelltex::category::SUPERSCRIPT)
		return "^";
	if(chars[0] > 90 && chars[0] < 97) // [, \, ], ^, _, `
		return chars;

	return chars;
}

std::string spelltex::cat_to_str(const spelltex::category cat){
	if(cat == spelltex::ESCAPE_CHARACTER)
		return "ESCAPE_CHARACTER";
	if(cat == spelltex::BEGINNING_OF_GROUP)
		return "BEGINNING_OF_GROUP";
	if(cat == spelltex::END_OF_GROUP)
		return "END_OF_GROUP";
	if(cat == spelltex::MATH_SHIFT)
		return "MATH_SHIFT";
	if(cat == spelltex::ALIGNMENT_TAB)
		return "ALIGNMENT_TAB";
	if(cat == spelltex::END_OF_LINE)
		return "END_OF_LINE";
	if(cat == spelltex::PARAMETER)
		return "PARAMETER";
	if(cat == spelltex::SUPERSCRIPT)
		return "SUPERSCRIPT";
	if(cat == spelltex::SUBSCRIPT)
		return "SUBSCRIPT";
	if(cat == spelltex::IGNORED_CHARACTER)
		return "IGNORED_CHARACTER";
	if(cat == spelltex::SPACE)
		return "SPACE";
	if(cat == spelltex::LETTER)
		return "LETTER";
	if(cat == spelltex::OTHER_CHARACTER)
		return "OTHER_CHARACTER";
	if(cat == spelltex::ACTIVE_CHARACTER)
		return "ACTIVE_CHARACTER";
	if(cat == spelltex::COMMENT_CHARACTER)
		return "COMMENT_CHARACTER";
	if(cat == spelltex::INVALID_CHARACTER)
		return "INVALID_CHARACTER";
	if(cat == spelltex::CONTROL_SEQUENCE)
		return "CONTROL_SEQUENCE";
	if(cat == spelltex::SPECIAL_CHARACTER)
		return "SPECIAL_CHARACTER";

	return "UNKNOWN";
}

