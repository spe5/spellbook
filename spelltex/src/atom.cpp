#include "common.h"
#include<QRawFont>
#include<QGlyphRun>
#include "atom.h"
#include "spelltex.h"

namespace spelltex {

	Atom::Atom(spelltex::SpellTeX* context, Atom* parent){
		if(context == nullptr){
			std::cout << "[ERROR] Atom Constructor: Got NULL instead of SpellTeX object, this is bug!" << std::endl;
			return;
		}

		_context = context;
		type = ATOM_GENERIC; // default value when atomizer does NOT need any help when creating atom tree
		_deleted = false;
		_id = gen_random(12);
		_debug_atom_tree_id = ""; // default atom tree has empty ID, set to non-empty when making atom copy
		_parent = parent;

		initialize_position();

		initialize_size();

		initialize_draw_context();

		_font_file = nullptr; // indicate that atom does NOT use a glyph stored in standalone font file
		_pixel_size = 16; // default pixel size must be non-zero, otherwise undefined behaviour may happen in case of a bug
		_scale = 1.0; // default 100% scale of _pixel_size
		_default_scale = 1.0;
		_glue_power = spelltex::glue_power::UNKNOWN;
		_debug_flag = false;
		_ttf_glyph = false;
		_std_text = false;
		_current_point_size = -1;
		//_letter = nullptr;
		_letter = "";
		_glyph_index = -1;
		_raw_font = nullptr;
		
		_default_main_font = _context->_default_main_font;
		_main_font = _default_main_font;

		initialize_iterator();		
	}

	Atom::Atom(const Atom* original, Atom* parent){ // copy constructor
		if(original == nullptr){
			std::cout << "[ERROR] Atom Copy Constructor: Got NULL instead of original Atom object, this is bug!" << std::endl;
			return;
		}

		_context = original->_context;
		type = original->type;
		_deleted = original->_deleted;
		_id = original->_id;
		//_id = gen_random(12); // id is unique for each Atom, copy has own unique ID, independent of original atom
		_debug_atom_tree_id = ""; // default atom tree has empty ID, set to non-empty when making atom copy
		_default_width = original->_default_width;
		_default_height = original->_default_height;
		_default_depth = original->_default_depth;
		_default_partial_height = original->_default_partial_height;
		_parent = parent; // do NOT take original parent unless explicitly told so, use given parent instead
		if(parent != NULL){ // registr self as new parent's child
			_parent->_childs.push_back(this);
		}

		_offset_x = original->_offset_x;
		_offset_y = original->_offset_y;

		_width = original->_width;
		_height = original->_height;
		_partial_height = original->_partial_height;
		_depth = original->_depth;
		_ascent = original->_ascent;

		_font_file = original->_font_file;
		_pixel_size = original->_pixel_size;
		_scale = original->_scale;
		_default_scale = original->_default_scale;
		_glue_power = original->_glue_power;
		_debug_flag = original->_debug_flag;
		_draw_rectangle = original->_draw_rectangle;
		_ttf_glyph = original->_ttf_glyph;
		_std_text = original->_std_text;
		_debug_color = original->_debug_color;
		_current_point_size = original->_current_point_size;
		_letter = original->_letter;
		_glyph_index = original->_glyph_index;
		_raw_font = original->_raw_font;
		
		_default_main_font = original->_default_main_font;
		_main_font = original->_main_font;
		_main_color = original->_main_color;

		initialize_iterator();

		_size_calculation_finished = original->_size_calculation_finished; // true if atom size calculation successfully finished & _width, _height, etc. contains final values


				
		// used for debug rectangles, rendered OUTSIDE of SpellTeX, in absolute values, filled during SpellTeX rendering stage
		deb_rect_top_left_x = original->deb_rect_top_left_x; // [pixel], absolute value for given canvas / page
		deb_rect_top_left_y = original->deb_rect_top_left_y; // [pixel], absolute value for given canvas / page
		deb_rect_mid_right_x = original->deb_rect_mid_right_x; // [pixel], absolute value for given canvas / page
		deb_rect_mid_right_y = original->deb_rect_mid_right_y; // [pixel], absolute value for given canvas / page
		deb_rect_mid_left_x = original->deb_rect_mid_left_x; // [pixel], absolute value for given canvas / page
		deb_rect_mid_left_y = original->deb_rect_mid_left_y; // [pixel], absolute value for given canvas / page
		deb_rect_bot_right_x = original->deb_rect_bot_right_x; // [pixel], absolute value for given canvas / page
		deb_rect_bot_right_y = original->deb_rect_bot_right_y; // [pixel], absolute value for given canvas / page

	}


	void Atom::set_pixel_size(int new_pixel_size){
		_pixel_size = new_pixel_size;
		for(std::list<Atom*>::iterator it = _childs.begin(); it != _childs.end(); it = std::next(it)){
			(*it)->set_pixel_size(new_pixel_size);
		}
	}

	
	void Atom::get_glyph_geometry(){
	        QRectF full_debug_rectangle = _raw_font->boundingRect(_glyph_index);
		_depth = abs(full_debug_rectangle.bottomLeft().y());
		_default_depth = _depth;
		_width = abs(full_debug_rectangle.topRight().x() - full_debug_rectangle.bottomLeft().x());
		_default_width = _width;
		_height = abs(full_debug_rectangle.topRight().y() - full_debug_rectangle.bottomLeft().y());
		_default_height = _height;
		_partial_height = abs(_height - _depth);
		_default_partial_height = _partial_height;
	}
	
	
	void Atom::get_letter_geometry(){
		if(_main_font == nullptr){
	       		_log << "[WARN] Main Font NOT set for Atom " << _id << ", using zeroes for dimensions: width=0,height=0,etc" << std::endl;
	       	        _width = 0;
	       	        _default_width = 0;
	       	        _height = 0;
	       	        _default_height = 0;
	       	        _depth = 0;
	       	        _default_depth = 0;
	       	        _partial_height = 0;
	       	        _default_partial_height = 0;
	       	        _ascent = 0;
	       	        return;
	       	}
	       	QFontMetrics metrics(*_main_font);
	       	QRectF box = metrics.tightBoundingRect(QString::fromStdString(_letter));
	       	_width = abs(box.width());
	       	_default_width = _width;
	       	_height = abs(box.height());
	       	_default_height = _height;
	       	_ascent = metrics.ascent();
	       	QPointF bot_left = box.bottomLeft();
		QPointF top_right = box.topRight();
		_depth = abs(box.bottomLeft().y());
		_default_depth = _depth;
		_partial_height = abs(_height - _depth);
		_default_partial_height = _partial_height;
	}
	
	

	void Atom::get_abs_offset(double &offset_x, double &offset_y){
		if(_parent == nullptr){
			offset_x = _offset_x;
			offset_y = _offset_y;
			return;
		}
		_parent->get_abs_offset(offset_x, offset_y);
		offset_x += _offset_x;
		offset_y += _offset_y;
	}


	void Atom::set_debug_flag(const bool debug_flag){
		_debug_flag = debug_flag;
		for(auto it = _childs.begin(); it != _childs.end(); it++){
			(*it)->set_debug_flag(debug_flag);
		}
	}


	void Atom::set_scaled_local_font(){
		if(_scale == 1){
			_main_font = _default_main_font;
			_painter->setFont(*_main_font);
			return;
		}

		_current_point_size = _default_main_font->pointSizeF();
		if(_current_point_size == -1){
			_log << "[ERROR] Main Font have Pixels instead of Points. Fix this." << std::endl;
			_main_font = new QFont (*_default_main_font);
			_main_font->setPixelSize(std::floor(_default_main_font->pixelSize() * _scale));
		}else{
			_main_font = new QFont (*_default_main_font);
			_main_font->setPointSizeF(_default_main_font->pointSizeF() * _scale);
		}
		_painter->setFont(*_main_font);
	}


	void Atom::set_scaled_raw_font(){
		// TODO check existence and validity of _font_file
		if(_font_file == nullptr){
			_log << "[ERROR] Invalid font file" << std::endl;
			return;
		}
		_raw_font = new QRawFont(*_font_file, std::floor(_pixel_size * _scale), QFont::PreferDefaultHinting);
	}


	double Atom::width(){
		if(_deleted == true)
			return 0;

		return _width;
	}


	double Atom::height(){
		if(_deleted == true)
			return 0;

		return _height;
	}

	
	double Atom::partial_height(){
		if(_deleted == true)
			return 0;

		return _partial_height;
	}


	double Atom::depth(){
		if(_deleted == true)
			return 0;

		return _depth;
	}


	retval Atom::evaluate(){
		// by default do NOT modify current atom tree
                retval ret = retval::SUCCESS;
                for(auto child_it = _childs.begin(); child_it != _childs.end(); child_it++){
                        ret = agg(ret, (*child_it)->evaluate());
                }
                return ret;
	}


	int Atom::get_later_siblings(std::list<spelltex::Atom*> &later_siblings){
        	if(_parent == nullptr){
			std::cout << "[WARN] Trying to get siblings of root atom with id=" << _id << std::endl;
                	return -1;
		}

                std::list<spelltex::Atom*>::iterator child_it;
                bool after_me = false; // true for siblings after me
	        for(child_it = _parent->_childs.begin(); child_it != _parent->_childs.end(); child_it++){
        	        if(after_me == true)
                	        later_siblings.push_back(*child_it);
                        if((*child_it) == this){
                                after_me = true;
	                }
        	}
                return 0;
        };

	double Atom::textual_width(){
		// returns total width of text, NOT counting spaces
		// used for space scaling
		if(_deleted == true)
			return 0;

		double ret = _default_width;
		std::list<Atom*>::iterator it = _childs.begin();
		while(it != _childs.end()){
			ret += (*it)->textual_width();
			it++;
		}
		return ret * _scale;

	};

	void Atom::scale_spaces(double scale_factor){
		// modifyies all spaces to align the row
		if(_deleted == true)
			return;
		if(_childs.size() == 0)
			return;

		std::list<Atom*>::iterator it = _childs.begin();
		while(it != _childs.end()){
			(*it)->scale_spaces(scale_factor);
			it++;
		}
		return;
	};


	int Atom::count_spaces(){
		if(_deleted == true)
			return 0;
		if(_childs.size() == 0)
			return 0;

		int number_of_spaces = 0;
		std::list<Atom*>::iterator it = _childs.begin();
		while(it != _childs.end()){
			number_of_spaces += (*it)->count_spaces();
			it++;
		}
		return number_of_spaces;

	}


	void Atom::update_subtree_font(QFont *new_font, QFontMetricsF *new_metrics){
		_default_main_font = new_font;
		for(auto it = _childs.begin(); it != _childs.end(); it++){
			(*it)->update_subtree_font(new_font, new_metrics);
		}
	}


	int Atom::get_atom_level(){
		// return general level in atom-tree
		if(_parent == nullptr)
			return 0;
		return _parent->get_atom_level() + 1;
	}


	bool Atom::is_math_shift(){
		// return number of subscript atoms in parent path in atom-tree
		if(_parent == nullptr)
			return false;
		return _parent->is_math_shift();
	}


	Atom* Atom::space_signal(){
		if(_parent == nullptr)
			return this;

		return _parent->space_signal();
	}


	Atom* Atom::math_end_signal(){
		if(_parent == nullptr)
			return this;

		return _parent->math_end_signal();
	}


	Atom* Atom::group_end_signal(){
		if(_parent == nullptr)
			return this;

		return _parent->group_end_signal();
	}


	void Atom::recalculate_scale(){
		// default value of each atom is stored in _default_scale
		if(_parent == nullptr)
			_scale = _default_scale * _context->_zoom;
		else
			_scale = _default_scale * _parent->_scale;
	
		// invoke recalculation for all own childs
		for(auto it = _childs.begin(); it != _childs.end(); it++){
			(*it)->recalculate_scale();
		}
	}


}
