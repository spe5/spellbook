#include "common.h"
#include<QRawFont>
#include<QGlyphRun>
#include "atom.h"
#include "spelltex.h"

namespace spelltex {

	void Atom::initialize_draw_context(){
		_draw_rectangle = QRect(); // default is NULL rectangle
		_debug_color = new QColor("red");
		_main_color = _context->_main_color;
	}


	void Atom::set_draw_context(QPainter* painter, QPixmap* pixmap){
		_painter = painter;
		_pixmap = pixmap;
		for(auto it = _childs.begin(); it != _childs.end(); it++){
			(*it)->set_draw_context(painter, pixmap);
		}
	}


	void Atom::set_debug_brush(){
	        QPen current_pen = _painter->pen();
	        QBrush current_brush = current_pen.brush();
	        current_brush.setColor(*_debug_color);
	        current_pen.setBrush(current_brush);
	        _painter->setPen(current_pen);
	}
		
		
	void Atom::set_main_brush(){
	        QPen current_pen = _painter->pen();
	        QBrush current_brush = current_pen.brush();
	        current_brush.setColor(*_main_color);
	        current_pen.setBrush(current_brush);
	        _painter->setPen(current_pen);
	}
		
		
	void Atom::draw_lower_debug_rectangle(const QPointF left_baseline_position){
	        QRectF lower_debug_rectangle = QRectF(0, 0, _width , _depth);
		set_debug_brush();
		lower_debug_rectangle.moveTopLeft(left_baseline_position);
		if(_debug_flag)
			_painter->drawRect(lower_debug_rectangle);

		//save debug rectangle points for later processing, outside of SpellTeX
		deb_rect_mid_left_x = lower_debug_rectangle.topLeft().x();
		deb_rect_mid_left_y = lower_debug_rectangle.topLeft().y();
		deb_rect_bot_right_x = lower_debug_rectangle.bottomRight().x();
		deb_rect_bot_right_y = lower_debug_rectangle.bottomRight().y();
	}
		
		
	void Atom::draw_upper_debug_rectangle(const QPointF left_baseline_position){
	        QRectF upper_debug_rectangle = QRectF(0, 0, _width , _partial_height);
		set_debug_brush();
		upper_debug_rectangle.moveBottomLeft(left_baseline_position);
		if(_debug_flag)
			_painter->drawRect(upper_debug_rectangle);
		
		//save debug rectangle points for later processing, outside of SpellTeX
		deb_rect_top_left_x = upper_debug_rectangle.topLeft().x();
		deb_rect_top_left_y = upper_debug_rectangle.topLeft().y();
		deb_rect_mid_right_x = upper_debug_rectangle.bottomRight().x();
		deb_rect_mid_right_y = upper_debug_rectangle.bottomRight().y();
	}

	void Atom::draw_std_letter_only(QPointF left_baseline_position){
		set_main_brush();
	        //_painter->setFont(*main_font);
		QPointF top_left_corner(left_baseline_position.x(), left_baseline_position.y() - _ascent); // convert left baseline position point to top left corner point
	        const QRect _remaining_page_space = QRect(top_left_corner.x(), top_left_corner.y(), 800, 800); // QRectF(qreal x, qreal y, qreal width, qreal height)
		_painter->drawText(_remaining_page_space, Qt::AlignLeft, QString::fromStdString(_letter), nullptr);
	}
	
	
	int Atom::draw_std_letter(QPointF left_baseline_position){
	
		// draw debug rectangle
		get_letter_geometry();
	        draw_std_letter_only(left_baseline_position);
		draw_upper_debug_rectangle(left_baseline_position);
		draw_lower_debug_rectangle(left_baseline_position);
	
	        std::cout << "STD " << _letter << " (x=" << left_baseline_position.x() << " y=" << left_baseline_position.y() << " w=" << _width << " h=" << _height;
		std::cout << " ph=" << _partial_height << " d=" << _depth << " a=" << _ascent << " s=" << _scale << ")" << std::endl;
		
		return 0;
	}
	
	
	void Atom::draw_glyph_only(QPointF left_baseline_position){
	        QVector<quint32> my_glyphs;
	        my_glyphs.push_back(_glyph_index);
	
	        QVector<QPointF> my_positions;
	        QPointF first_position(0,0);
	        my_positions.push_back(first_position);
	
	        QGlyphRun my_glyph_run;
	        my_glyph_run.setRawFont(*_raw_font);
	        my_glyph_run.setGlyphIndexes(my_glyphs);
	        my_glyph_run.setPositions(my_positions);
		set_main_brush();
	        _painter->drawGlyphRun(left_baseline_position, my_glyph_run);
	}
	
		
	int Atom::draw_glyph(QPointF left_baseline_position){
	
		get_glyph_geometry();
		draw_glyph_only(left_baseline_position);
		draw_lower_debug_rectangle(left_baseline_position);
		draw_upper_debug_rectangle(left_baseline_position);
	
	        std::cout << "TTF GLYPH " << " gid=" << _glyph_index << " (x=" << left_baseline_position.x() << " y=" << left_baseline_position.y();
		std::cout << " w=" << _width << " h=" << _height << " ph=" << _partial_height << " d=" << _depth << ")" << std::endl;
	        return 0;
	}

	void Atom::draw() {
		// assumption: internal variables _offset_x,y and _width, _height, _log are already set correctly
		if(_deleted == true)
			return;

		if(_std_text){
			double x, y = 0.0;
			get_abs_offset(x, y);
			QPointF left_baseline_position(x, y);
			set_scaled_local_font();
			draw_std_letter(left_baseline_position);
		}

		if(_ttf_glyph){
			double x, y = 0.0;
			get_abs_offset(x, y);
			QPointF left_baseline_position(x, y);
			draw_glyph(left_baseline_position);
		}

		draw_all_childs();
	}

	void Atom::draw_all_childs(){
		for(auto it = _childs.begin(); it != _childs.end(); it++){
			(*it)->draw();
		}
	}



}
