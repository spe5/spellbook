#include "common.h"
#include "atom.h"
#include "spelltex.h"

namespace spelltex {


	void Atom::initialize_iterator(){
		_iterator = -1; // -1 for itself, 0 for 1th child, 1 for 2nd child, etc.
	}


	void Atom::reset_iterators(){
		_iterator = -1; // -1 for itself, 0 for 1th child, 1 for 2nd child, etc.
		for(std::list<Atom*>::iterator it = _childs.begin(); it != _childs.end(); it = std::next(it)){
			(*it)->reset_iterators();
		}
	}


	Atom* Atom::next(){
		if(_iterator == -1){
			_iterator++;
			return this;
		}
		if(_iterator >= _childs.size())
			return NULL;

		std::list<Atom*>::iterator it = _childs.begin();
		std::advance(it, _iterator);
		Atom* next_in_current_child = (*it)->next();
		while(next_in_current_child == NULL){
			_iterator++;
			if(_iterator >= _childs.size())
				break;
			std::advance(it, 1);
			next_in_current_child = (*it)->next();
		}
		return next_in_current_child;
	}

}

