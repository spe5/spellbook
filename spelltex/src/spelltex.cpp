#include <iostream>
#include <fstream>
#include <sstream>
#include <QGuiApplication>
#include <QPixmap>
#include <QPainter>
#include <QFile>
#include "../include/spelltex.h"

#define STEX_SUCCESS 0
#define STEX_GENERAL_ERROR 1
#define STEX_INVALID_SYNTAX_ERROR 2
#define STEX_INPUT_FILE_NOT_EXISTS 3
#define STEX_INPUT_FILE_NOT_UTF8 4
#define DEFAULT_ZOOM 1
#define STEX_IMAGE_SAVE_FAILED 5
#define STEX_ASSUMPTION_VIOLATION 6

//#include "letter_atom.h"
//#include "math/greek_small_varphi_atom.h"
//#include "math/greek_small_phi_atom.h"
//#include "math/greek_small_omega_atom.h"
//#include "math/latin_small_k_atom.h"
//#include "math/latin_small_v_atom.h"
//#include "math/latin_capital_w_atom.h"
//#include "math/math_symbol_equiv_atom.h"
//#include "math/math_symbol_vec_atom.h"
//#include "mathcal_atom.h"
//#include "space_atom.h"
//#include "page_atom.h"
#include "root_atom.h"
//#include "vertical_atom.h"
//#include "horizontal_atom.h"
//#include "definition_atom.h"
//#include "math_atom.h"
//#include "word_atom.h"
//#include "group_atom.h"
//#include "subscript_atom.h"

#define MINIMUM_PIXEL_SIZE 6

namespace spelltex {

	SpellTeX::SpellTeX(QApplication* qt_app){
	        debug_flag = false;
	        _default_page_width = 800; // TODO configurable by user using cmd args
	        _default_page_height = 800; // TODO configurable by user using cmd args
	        _default_printable_left_top_x = 20; // TODO configurable by user using cmd args
	        _default_printable_left_top_y = 20; // TODO
	        _default_printable_right_bot_x = 780; // TODO
	        _default_printable_right_bot_y = 780; // TODO
						      
		_user_page_width = -1; // -1 for NOT initialized value, when _default_page_width SHALL be used instead, otherwise positive value
		_user_page_height = -1; // -1 for NOT initialized value, when _default_page_width SHALL be used instead, otherwise positive value
		_user_printable_left_top_x = -1; // -1 for NOT initialized value, when _default_page_width SHALL be used instead, otherwise positive value
		_user_printable_left_top_y = -1; // -1 for NOT initialized value, when _default_page_width SHALL be used instead, otherwise positive value
		_user_printable_right_bot_x = -1; // -1 for NOT initialized value, when _default_page_width SHALL be used instead, otherwise positive value
		_user_printable_right_bot_y = -1; // -1 for NOT initialized value, when _default_page_width SHALL be used instead, otherwise positive value

	        app = qt_app;
	        //img = nullptr;
	        //app = new QGuiApplication (argc, argv); // initialize Qt first, in order to use all Qt-related stuff ASAP (like qDebug instead std::cout)
	        //img = new QPixmap(_page_width, _page_height);
	        //img->fill(Qt::white);
	
		QString empty_string("");
	        qts_tokenizer_log.setString(&empty_string);
	        ss_atom_log.str("");
	        ss_renderer_log.str("");

		_pixel_size = 16;
		_default_main_font = new QFont("Ubuntu", _pixel_size);
		_main_color = new QColor("black");
		_zoom = 1.0; // default zoom 100%

	        print_context_conditionally(); // only if debug flag set
		qDebug() << "Greeting from SpellTeX object";
	}
	
	
	SpellTeX::~SpellTeX(){
	        // cleanup dynamically allocated internal variables
	        //delete img;
	}


	int SpellTeX::main(const std::string latex_source, QPixmap *rendered_image, std::vector<spelltex::Atom*>& atom_trees_buffer){
	        // returns rendered image in private variable img
		// expects existing pixmap instance in buffer rendered_image
	
		if(rendered_image == nullptr){
			qDebug() << "[ERROR] No pixamp to draw provided. Abort.";
			return STEX_ASSUMPTION_VIOLATION;
		}

	        latex_stream.str(latex_source);
		rendered_image->fill(Qt::white);

	        spelltex::Atom* my_atom_tree = first_stage(atom_trees_buffer); // create atom tree
	        my_atom_tree = second_stage(my_atom_tree, atom_trees_buffer); // typeset the atom tree (calculate geometry)
	        my_atom_tree = third_stage(my_atom_tree, atom_trees_buffer, rendered_image); // draw
	
	        return STEX_SUCCESS;
	}


	void SpellTeX::update_font_type(QFont* new_font){
		if(new_font != nullptr)
			_default_main_font = new_font;
	}
	

	void SpellTeX::update_font_size(int pixel_size){
		if(pixel_size >= MINIMUM_PIXEL_SIZE)
			_pixel_size = pixel_size;
	}
	

	void SpellTeX::update_color(QColor* new_color){
		if(new_color != nullptr)
			_main_color = new_color;
	}


	int SpellTeX::set_font(QFont &default_font){
		_default_main_font = &default_font;
		return STEX_SUCCESS;
	}


	int SpellTeX::set_zoom(double zoom){
		_zoom = zoom;
		return STEX_SUCCESS;
	}

	int SpellTeX::set_page_dimensions(const int page_width, const int page_height, const int top_margin, const int left_margin, const int bottom_margin, const int right_margin){
		// set up user-defined page size and internal rectangle for paragraph typesetting
		// check for valid values first
		if((page_width == 0) or (page_height == 0))
			return STEX_ASSUMPTION_VIOLATION; 
		_user_page_width = page_width;
		_user_page_height = page_height;
		_user_printable_left_top_y = top_margin;
		_user_printable_left_top_x = left_margin;
		_user_printable_right_bot_y = page_height - bottom_margin;
		_user_printable_right_bot_x = page_width - right_margin;
		return STEX_SUCCESS;
	}

	int SpellTeX::get_default_page_dimensions(int &page_width, int &page_height, int &top_margin, int &left_margin, int &bottom_margin, int &right_margin){
		page_width = _default_page_width;
		page_height = _default_page_height;
		top_margin = _default_printable_left_top_y;
		left_margin = _default_printable_left_top_x;
		bottom_margin = _default_page_height - _default_printable_right_bot_y;
		right_margin = _default_page_width - _default_printable_right_bot_x;
		return STEX_SUCCESS;
	}

	int SpellTeX::get_effective_page_dimensions(int &page_width, int &page_height, int &top_margin, int &left_margin, int &bottom_margin, int &right_margin){
		if(_user_page_width != -1)
			page_width = _user_page_width;
		else
			page_width = _default_page_width;

		if(_user_page_height != -1)
			page_height = _user_page_height;
		else
			page_height = _default_page_height;

		if(_user_printable_left_top_y != -1)
			top_margin = _user_printable_left_top_y;
		else
			top_margin = _default_printable_left_top_y;

		if(_user_printable_left_top_x != -1)
			left_margin = _user_printable_left_top_x;
		else
			left_margin = _default_printable_left_top_x;

		if(_user_printable_right_bot_y != -1)
			if(_user_page_height != -1)
				bottom_margin = _user_page_height - _user_printable_right_bot_y;
			else
				bottom_margin = _default_page_height - _user_printable_right_bot_y;
		else
			if(_user_page_height != -1)
				bottom_margin = _user_page_height - _default_printable_right_bot_y;
			else
				bottom_margin = _default_page_height - _default_printable_right_bot_y;

		if(_user_printable_right_bot_x != -1)
			if(_user_page_width != -1)
				right_margin = _user_page_width - _user_printable_right_bot_x;
			else
				right_margin = _default_page_width - _user_printable_right_bot_x;
		else
			if(_user_page_width != -1)
				right_margin = _user_page_width - _default_printable_right_bot_x;
			else
				right_margin = _default_page_width - _default_printable_right_bot_x;

		return STEX_SUCCESS;
	}

	void SpellTeX::print_context_conditionally(){
	        if(debug_flag == false)
	                return;

		int page_width = -1;
	        int page_height = -1;
		int top_margin = -1;
		int left_margin = -1;
		int bottom_margin = -1;
		int right_margin = -1;
		get_effective_page_dimensions(page_width, page_height, top_margin, left_margin, bottom_margin, right_margin);

	        qDebug() << "[INFO] SpellTeX debug_flag = " << debug_flag;
	        qDebug() << "[INFO] SpellTeX page_width = " << page_width;
	        qDebug() << "[INFO] SpellTeX page_height = " << page_height;
	        qDebug() << "[INFO] SpellTeX printable_left_top_y = " << top_margin;
	        qDebug() << "[INFO] SpellTeX printable_left_top_x = " << left_margin;
	        qDebug() << "[INFO] SpellTeX printable_right_bot_y = " << page_height - bottom_margin;
	        qDebug() << "[INFO] SpellTeX printable_right_bot_x = " << page_width - right_margin;
	        qDebug() << "[INFO] SpellTeX app = " << app;
	        //qDebug() << "[INFO] SpellTeX img = " << img;
	        qDebug() << "[INFO] SpellTeX latex_stream holds " << latex_stream.str().size() << " characters";
	}
	
	
	void SpellTeX::log_tokens(std::list<spelltex::token> &my_tokens){
		QString empty_string("");
	        qts_tokenizer_log.setString(&empty_string); // clear buffer
	        std::list<spelltex::token>::iterator it = my_tokens.begin();
	        while(it != my_tokens.end()){
	                qts_tokenizer_log << "[INFO] (" << (cat_to_str(it->cat)).c_str() << "," << Qt::hex << print_chars(it->cat, it->chars).c_str() << Qt::dec << ")" << Qt::endl;
	                it = std::next(it);
	        }
	}

	int SpellTeX::copy_atom_tree(const spelltex::Atom* orig_root, spelltex::Atom** new_root){
		// gets existing atom tree and buffer for new atom tree, which fills with tree copy
		if(orig_root == nullptr){
			return -1; // error assumptions not met
		}
		if(new_root == nullptr){
			return -1; // error, new_root is expected to be an pointer, pointing to no valid object
		}
		
		(*new_root) = new spelltex::RootAtom(orig_root);
		std::list<spelltex::Atom*>::const_iterator it;
		for(it = orig_root->_childs.begin(); it != orig_root->_childs.end(); it++){
			(*it)->copy_atom_tree(*new_root);
		}
		return 0; // return STEX_SUCCESS TODO
	}
	
	
	void SpellTeX::log_atom_tree(spelltex::Atom* my_atom_tree){
	        ss_atom_log.str(""); // clear previous data
	        my_atom_tree->print_me(ss_atom_log, false);
	}
	
	
	spelltex::Atom* SpellTeX::first_stage(std::vector<spelltex::Atom*>& atom_trees_buffer){
	        // 1th step: parse the data from file, create list of tokens
		my_tokens.clear();
	        SpellTeX::tokenize(latex_stream.str(), my_tokens);
	        log_tokens(my_tokens);
	        qDebug() << "[DONE] 1th step: tokenize";
	
	        // 2nd step: convert token list to atom tree
		// take page configurations from user: page_width, page_height, printable_left_top_x, printable_left_top_y, printable_right_bot_x, printable_right_bot_y
	        spelltex::Atom* my_atom_tree = new spelltex::RootAtom(this);
	        SpellTeX::atomize(my_tokens, my_atom_tree, ss_atom_log);
	        log_atom_tree(my_atom_tree);
	        my_atom_tree->set_debug_flag(debug_flag); // draw debug rectangles according to user settings
		spelltex::Atom* copy_of_atom_tree = nullptr;
		copy_atom_tree(my_atom_tree, &copy_of_atom_tree);
		atom_trees_buffer.push_back(copy_of_atom_tree);

	        qDebug() << "[DONE] 2nd step: atomize";
	
	//      // 3rd step: process all SpellTeX-macros
	//      if(evaluate(my_atom_tree) == spelltex::retval::ERROR){
	//              qDebug() << "[ERROR] Macro evaluation has failed.";
	//              return nullptr;
	//      }
	//      log_atom_tree(my_atom_tree);
	//	copy_of_atom_tree = nullptr;
	//	copy_atom_tree(my_atom_tree, copy_of_atom_tree);
		atom_trees_buffer.push_back(copy_of_atom_tree);

	//      qDebug() << "[DONE] 3rd step: evaluate macros";
	
	        return my_atom_tree;
	}
	
	spelltex::Atom* SpellTeX::second_stage(spelltex::Atom* my_atom_tree, std::vector<spelltex::Atom*>& atom_trees_buffer){
	        // 4th step: scale
		std::stringstream main_log_stream;
		my_atom_tree->get_log(main_log_stream);
		my_atom_tree->recalculate_scale();
		log_atom_tree(my_atom_tree);
		spelltex::Atom* copy_of_atom_tree = nullptr;
		copy_atom_tree(my_atom_tree, &copy_of_atom_tree);
		atom_trees_buffer.push_back(copy_of_atom_tree);
		qDebug() << "[DONE] 4th step: scaled";
	
		// 5th step: size
		my_atom_tree->recalculate_size();
		copy_of_atom_tree = nullptr;
		copy_atom_tree(my_atom_tree, &copy_of_atom_tree);
		atom_trees_buffer.push_back(copy_of_atom_tree);
		qDebug() << "[DONE] 5th step: sizes";
	
		// 6th step: positions
		my_atom_tree->recalculate_position();
		log_atom_tree(my_atom_tree);
		copy_of_atom_tree = nullptr;
		copy_atom_tree(my_atom_tree, &copy_of_atom_tree);
		atom_trees_buffer.push_back(copy_of_atom_tree);
		qDebug() << "[DONE] 6th step: positions";
	
		//int page_penalty = spelltex::page_metrics(first_page);
		//std::cout << "[DEBUG] Example Main: Page metrics is " << page_penalty  << std::endl;
	
	        return my_atom_tree;
	}
	
	
	spelltex::Atom* SpellTeX::third_stage(spelltex::Atom* atom_tree, std::vector<spelltex::Atom*>& atom_trees_buffer, QPixmap *pixmap){
	        // 7th step: draw
	        draw_atom_tree_on_pixmap(atom_tree, pixmap, DEFAULT_ZOOM, ss_renderer_log);
		spelltex::Atom* copy_of_atom_tree = nullptr;
	        copy_atom_tree(atom_tree, &copy_of_atom_tree);
	        atom_trees_buffer.push_back(copy_of_atom_tree);

	        qDebug() << "[DONE] 7th step: draw";
	        return atom_tree;
	}
	
}
