#include <QPainter>
#include <iostream>
#include "../include/spelltex.h"

void spelltex::SpellTeX::draw_atom_tree_on_pixmap(spelltex::Atom* my_atom_tree, QPixmap *my_pixmap, const double zoom, std::stringstream &log){

	log << "[INFO] Starting to draw atom tree on pixmap..." << std::endl;
	std::cout << "[INFO] Starting to draw atom tree on pixmap..." << std::endl;

        QPainter my_painter(my_pixmap);
        my_painter.setRenderHint(QPainter::Antialiasing, true);
	my_atom_tree->_default_scale = zoom; // user-defined zoom for page atom
	//std::cout << "[INFO] Recalculation..." << std::endl;
	//my_atom_tree->recalculate_scale();
	my_atom_tree->set_draw_context(&my_painter, my_pixmap);
	//std::cout << "[INFO] Drawing..." << std::endl;
	my_atom_tree->draw();
	my_atom_tree->get_log(log);
	log << "[INFO] Finished draw of atom tree to pixmap" << std::endl;
	std::cout << "[INFO] Finished draw of atom tree to pixmap" << std::endl;
}


