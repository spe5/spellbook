const { Pool } = require("pg");

const sql = new Pool({
    user: 'nextjs',
    database: 'nextjs-dashboard-postgres',
    password: 'hello-world',
    port: 5432,
    host: 'localhost',
})

module.exports = { sql };

//export default (callback = null) => {
  // We'll handle our connection to PostgreSQL here...
//};
