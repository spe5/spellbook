#!/bin/bash
# Backup complete Spellbook database
# Written by Lukas Sedlak in 2021 under BSD license

BACKUP_DIR=$HOME/.spellbook/backup/
TAG=`date +%Y-%m-%d_%H-%M-%S`
POSTGRES_USER="wizard"
SPELLBOOK_DB="spellbook"

mkdir -p $BACKUP_DIR
pg_dump -c -f ${BACKUP_DIR}/${TAG}_${USER}_${SPELLBOOK_DB}.sql --if-exists -U ${POSTGRES_USER} -h localhost -W -- ${SPELLBOOK_DB}
echo 'pg_dump -c -f '${BACKUP_DIR}'/'${TAG}'_'${USER}'_'${SPELLBOOK_DB}'.sql --if-exists -U '${POSTGRES_USER}' -h localhost -W -- '${SPELLBOOK_DB}



