#!/bin/bash
# Restore complete Spellbook database into PostgreSQL
# Written by Lukas Sedlak in 2021 under BSD license

BACKUP_DIR=$HOME/.backup/spellbook/
POSTGRES_USER="wizard"
SPELLBOOK_DB="spellbook"

# Ensure a database-related tag is given by user
TAG=$1
TAG_REGEX=^[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{2}-[0-9]{2}-[0-9]{2}$
if [[ $TAG =~ $TAG_REGEX ]]; then
        TAG=$1
else
        echo "ERROR: Wrong syntax: missing first argument as timestamp of backup to restore from, e.g. 2021-06-20_18-55-38"
        echo "INFO: Backup files are then ${BACKUP_DIR}2021-06-20_18-55-38_${USER}_${SPELLBOOK_DB}.sql"
        echo "INFO: Backup files are then ${BACKUP_DIR}2021-06-20_18-55-38_${USER}_${SPELLBOOK_DB}.sql"
	echo "INFO: Available backups are:"
	ls ${BACKUP_DIR}
        exit 1;
fi


echo "dropdb -U ${POSTGRES_USER} -h localhost -W -- ${SPELLBOOK_DB}'"
dropdb -U ${POSTGRES_USER} -h localhost -W -- ${SPELLBOOK_DB}

echo "createdb -U ${POSTGRES_USER} -h localhost -W -- ${SPELLBOOK_DB}"
createdb -U ${POSTGRES_USER} -h localhost -W -- ${SPELLBOOK_DB}

echo "psql -f ${BACKUP_DIR}/${TAG}_${USER}_${SPELLBOOK_DB}.sql -U ${POSTGRES_USER} -h localhost -W -- ${SPELLBOOK_DB}"
psql -f ${BACKUP_DIR}/${TAG}_${USER}_${SPELLBOOK_DB}.sql -U ${POSTGRES_USER} -h localhost -W -- ${SPELLBOOK_DB}




