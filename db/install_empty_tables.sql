BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "author" (
	"author_id"	TEXT NOT NULL UNIQUE,
	"first_name"	TEXT,
	"second_name"	TEXT,
	"billing_number"	TEXT,
	PRIMARY KEY("author_id")
);
CREATE TABLE IF NOT EXISTS "spell" (
	"spell_id"	TEXT NOT NULL UNIQUE,
	"spell_type"	INTEGER NOT NULL,
	"name"		TEXT,
	"description"	TEXT NOT NULL,
	"use_cost"	TEXT NOT NULL,
	"use_cost_currency"	TEXT NOT NULL,
	PRIMARY KEY("spell_id")
);
CREATE TABLE IF NOT EXISTS "sdepmap" (
	"primary_spell_id"	TEXT NOT NULL,
	"secondary_spell_id"	TEXT NOT NULL,
	"relation"		INTEGER NOT NULL,
	PRIMARY KEY("primary_spell_id", "secondary_spell_id", "relation")
);
CREATE TABLE IF NOT EXISTS "authorship" (
	"spell_id"	TEXT NOT NULL,
	"author_id"	TEXT NOT NULL,
	PRIMARY KEY("spell_id","author_id"),
	CONSTRAINT FK_1 FOREIGN KEY(spell_id) REFERENCES spell(spell_id)
);
CREATE TABLE IF NOT EXISTS "code" (
	"code_id"	TEXT NOT NULL UNIQUE,
	"filename"	TEXT NOT NULL,
	"use_cost"	TEXT NOT NULL,
	"use_cost_currency"	TEXT NOT NULL,
	"accuracy"	TEXT,
	"complexity"	TEXT,
	PRIMARY KEY("code_id")
);
CREATE TABLE IF NOT EXISTS "implementation" (
	"spell_id"	TEXT NOT NULL,
	"code_id"	TEXT NOT NULL,
	PRIMARY KEY("spell_id","code_id"),
	CONSTRAINT FK_1 FOREIGN KEY(spell_id) REFERENCES spell(spell_id),
	CONSTRAINT FK_2 FOREIGN KEY(code_id) REFERENCES code(code_id)
);
CREATE TABLE IF NOT EXISTS "geometry" (
	"geometry_id"	TEXT NOT NULL UNIQUE,
	"filename"	TEXT NOT NULL,
	PRIMARY KEY("geometry_id")
);
CREATE TABLE IF NOT EXISTS "extended" (
	"extended_id"	TEXT NOT NULL UNIQUE,
	"geometry_id"	TEXT NOT NULL,
	"code_preconditions"	TEXT,
	"what_to_calculate"	TEXT,
	FOREIGN KEY(geometry_id) REFERENCES geometry(geometry_id),
	PRIMARY KEY("extended_id")
);
CREATE TABLE IF NOT EXISTS "calculation" (
	"extended_id"	TEXT NOT NULL,
	"code_id"	TEXT NOT NULL,
	PRIMARY KEY("extended_id","code_id"),
	FOREIGN KEY(extended_id) REFERENCES extended(extended_id),
	FOREIGN KEY(code_id) REFERENCES code(code_id)
);
CREATE TABLE IF NOT EXISTS "prediction" (
	"prediction_id"	TEXT NOT NULL UNIQUE,
	"extended_id"	TEXT NOT NULL,
	"maximum_resources"	TEXT,
	"maximum_cost"		TEXT,
	"result"	TEXT,
	FOREIGN KEY(extended_id) REFERENCES extended(extended_id),
	PRIMARY KEY("prediction_id")
);
CREATE TABLE IF NOT EXISTS "experiment" (
	"experiment_id"	TEXT NOT NULL UNIQUE,
	"name"		TEXT NOT NULL,
	"author"	TEXT,
	"license"	TEXT,
	"filename"	TEXT NOT NULL,
	"cost"		TEXT,
	"is_book"	INT,
	"has_experiment" INT,
	PRIMARY KEY("experiment_id")
);
CREATE TABLE IF NOT EXISTS "verification" (
	"verification_id" TEXT NOT NULL,
	"prediction_id"	TEXT NOT NULL,
	"experiment_id"	TEXT NOT NULL,
	"comparision"	TEXT,
	"good_enough"	TEXT,
	PRIMARY KEY("verification_id"),
	FOREIGN KEY(prediction_id) REFERENCES prediction(prediction_id),
	FOREIGN KEY(experiment_id) REFERENCES experiment(experiment_id)
);
CREATE TABLE IF NOT EXISTS "tag" (
	"tag_id" TEXT NOT NULL,
	"tag_name" TEXT NOT NULL,
	PRIMARY KEY("tag_id")
);
CREATE TABLE IF NOT EXISTS "tag_article" (
	"experiment_id" TEXT NOT NULL,
	"tag_id" TEXT NOT NULL,
	PRIMARY KEY("experiment_id","tag_id"),
	FOREIGN KEY(experiment_id) REFERENCES experiment(experiment_id),
	FOREIGN KEY(tag_id) REFERENCES tag(tag_id)
);
CREATE TABLE IF NOT EXISTS "spellarticlemap" (
	"spell_id" TEXT NOT NULL,
	"article_id" TEXT NOT NULL,
	"relation" INTEGER NOT NULL,
	PRIMARY KEY("spell_id", "article_id")
);
COMMIT;
