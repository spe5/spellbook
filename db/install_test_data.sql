INSERT INTO author VALUES ('3958dc9e-712f-4377-85e9-fec4b6a6442a', 'Delba', 'de Oliveira', '322-84720535222/0300');
INSERT INTO author VALUES ('3958dc9e-742f-4377-85e9-fec4b6a6442a', 'Lee', 'Robinson', '86024588526/0700');
INSERT INTO author VALUES ('3958dc9e-737f-4377-85e9-fec4b6a6442a', 'Hector', 'Simpson', '28593222566/0100');
INSERT INTO author VALUES ('50ca3e18-62cd-11ee-8c99-0242ac120002', 'Steven', 'Tey', '522-997884311/0100');
INSERT INTO author VALUES ('3958dc9e-787f-4377-85e9-fec4b6a6442a', 'Steph', 'Dietz', '111-23456789/0330');
INSERT INTO author VALUES ('76d65c26-f784-44a2-ac19-586678f7c2f2', 'Michael', 'Novotny', '112-86233750/0600');
INSERT INTO author VALUES ('d6e15727-9fe1-4961-8c5b-ea44a9bd81aa', 'Evil', 'Rabbit', '987-333665582/0500');
INSERT INTO author VALUES ('126eed9c-c90c-4ef6-a4a8-fcf7408d3c66', 'Emil', 'Kowalski', '124-7859200512/0100');
INSERT INTO author VALUES ('CC27C14A-0ACF-4F4A-A6C9-D45682C144B9', 'Amy', 'Burns', '725-8552265842/0100');
INSERT INTO author VALUES ('13D07535-C59E-4157-A011-F8D2EF4E0CBB', 'Balazs', 'Orban', '203-0059800245/0330');

INSERT INTO spell VALUES ('019d71b3-bd9f-4522-86d7-891587fa867d', '0', 'First Definition', '\textsl{First Definition} $ d_1 $ is defined for X and Y as their ratio \[ d_1 \equiv \frac{X}{Y} \]', 0.1, 'EUR');
INSERT INTO spell VALUES ('0760aff8-d03d-4e07-b131-dc4d5f252ba3', '1', 'First Theorem', 'Lets assume $ X = Y/2 $. Then \[ d_1 = \frac{1}{2} \]', 0.2, 'EUR');
INSERT INTO spell VALUES ('6f4bad0b-3be1-4a5c-a815-0fe11c793314', '2', 'First Proof', '\[ d_1 \equiv \frac{X}{Y} \qquad X = \frac{Y}{2} \] \[ d_1 = \frac{1}{2} \qquad Q.E.D \]', 0.3, 'EUR');
INSERT INTO spell VALUES ('6d4c5d9f-f49a-4b13-a6bd-b81963bce5ce', '0', 'Second Definition', '\textsl{Second Definition} $ d_2 $ is defined for X and Y as their ratio \[ d_1 \equiv \frac{X^2}{Y^2} \]', 0.11, 'EUR');
INSERT INTO spell VALUES ('8e5dd1ee-8cf2-47eb-a95b-df862d407c30', '1', 'Second Theorem', '\textsl{Second Theorem} Lets assume $ d_2 $ and $ X = Y $. Then $ d_2 = 1 $', 0.15, 'EUR');
INSERT INTO spell VALUES ('ff24cf4d-d471-462f-83ec-20f21a9eaaed', '2', 'Second Proof', '\[ d_2 \equiv \frac{X^2}{Y^2} \qquad X = Y \] \[ d_2 = \frac{X^2}{X^2} = 1 \qquad Q.E.D \]', 0.26, 'EUR');

INSERT INTO authorship VALUES ('019d71b3-bd9f-4522-86d7-891587fa867d','3958dc9e-712f-4377-85e9-fec4b6a6442a');
INSERT INTO authorship VALUES ('0760aff8-d03d-4e07-b131-dc4d5f252ba3', '3958dc9e-742f-4377-85e9-fec4b6a6442a');
INSERT INTO authorship VALUES ('6f4bad0b-3be1-4a5c-a815-0fe11c793314', '3958dc9e-737f-4377-85e9-fec4b6a6442a');
INSERT INTO authorship VALUES ('6d4c5d9f-f49a-4b13-a6bd-b81963bce5ce', '50ca3e18-62cd-11ee-8c99-0242ac120002');
INSERT INTO authorship VALUES ('8e5dd1ee-8cf2-47eb-a95b-df862d407c30', '3958dc9e-787f-4377-85e9-fec4b6a6442a');
INSERT INTO authorship VALUES ('ff24cf4d-d471-462f-83ec-20f21a9eaaed', '76d65c26-f784-44a2-ac19-586678f7c2f2');

INSERT INTO sdepmap VALUES ('0760aff8-d03d-4e07-b131-dc4d5f252ba3', '019d71b3-bd9f-4522-86d7-891587fa867d', 0);
INSERT INTO sdepmap VALUES ('8e5dd1ee-8cf2-47eb-a95b-df862d407c30', '6d4c5d9f-f49a-4b13-a6bd-b81963bce5ce', 0);
INSERT INTO sdepmap VALUES ('6f4bad0b-3be1-4a5c-a815-0fe11c793314', '0760aff8-d03d-4e07-b131-dc4d5f252ba3', 1);
INSERT INTO sdepmap VALUES ('ff24cf4d-d471-462f-83ec-20f21a9eaaed', '8e5dd1ee-8cf2-47eb-a95b-df862d407c30', 1);




