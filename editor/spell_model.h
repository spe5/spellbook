#ifndef SPELL_MODEL_H
#define SPELL_MODEL_H

#include <QObject>
#include <QMainWindow>

#define SPELL_ID_COLUMN 0
#define SPELL_TYPE_COLUMN 1
#define SPELL_NAME_COLUMN 2
#define SPELL_COST_COLUMN 3
#define SPELL_CURRENCY_COLUMN 4
#define SPELL_DESCRIPTION 5

#define SPELL_COLS 6

enum spell_type{
    UNSET = 0,
    DEFINITION = 1,
    THEOREM = 2,
    PROOF = 3,
    UNKNOWN = 255
};

struct spell_item{
    QString spell_id;
    int spell_type;
    QString name;
    QString description;
    QString use_cost;
    QString use_cost_currency;
};

class spell_model : public QObject
{
    Q_OBJECT
public:
    explicit spell_model(QObject *parent = nullptr);

    int count_spells(const bool definitions, const bool theorems, const bool proofs, const QString like_pattern);
    int count_spells();
    int create_spell(const spell_item new_spell);
    int read_spell(QString spell_id, spell_item *buffer);
    int update_spell(const spell_item existing_spell);
    int delete_spell(QString spell_id);
    int read_spells(int limit, int offset, std::list<spell_item*> &buffer);
    int read_spells_2021_10_10(int limit, int offset, std::list<spell_item*> &buffer);

    void generate_random_id(QString &buffer);
    QString base64_encode(QString string);
    QString base64_decode(QString string);
    int migration_2021_10_10();

    spell_type int_to_spell_type(const int spell_type);
    int spell_type_to_int(const spell_type spell_type);
    spell_type string_to_spell_type(const std::string spell_type);
    std::string spell_type_to_string(const spell_type spell_type);

    int visible_rows;
    int offset;


signals:
    void update_spell_table();

};

#endif // SPELL_MODEL_H
