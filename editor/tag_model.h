#ifndef TAG_MODEL_H
#define TAG_MODEL_H

#include <QObject>

struct tag_item{
    QString tag_id;
    QString tag_name;
};

class tag_model : public QObject
{
    Q_OBJECT
public:
    explicit tag_model(QObject *parent = nullptr);

    int count_tags(const QString like_pattern);
    int count_tags();
    int create_tag(const tag_item new_tag);
    int read_tag(QString tag_id, tag_item *buffer);
    int update_tag(const tag_item existing_tag);
    int delete_tag(QString tag_id);
    int read_tags(int limit, int offset, std::list<tag_item*> &buffer);

    int visible_rows;
    int offset;

signals:

};

#endif // TAG_MODEL_H
