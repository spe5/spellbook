#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidgetItem>
#include "spell_model.h"
#include "article_model.h"
#include "author_model.h"
#include "tag_model.h"
#include "code_model.h"
#include "authorship_model.h"
#include "sdepmap_model.h"
#include "impmap_model.h"
#include "calmap.h"
#include "geometry_model.h"
#include "extended_model.h"
#include "prediction_model.h"
#include "verimap_model.h"
#include "tagart_model.h"

#define SPE5_SUCCESS 1
#define SPE5_PSQL_ERROR 0
#define SPE5_ERROR -1

#define LATEX_VIEW_WIDTH 900
#define LATEX_VIEW_HEIGHT 900

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    //TexGuard * my_tex_guard;

    spell_model * my_spell_model;
    article_model * my_article_model;
    author_model * my_author_model;
    tag_model * my_tag_model;
    code_model * my_code_model;
    authorship_model * my_authorship_model;
    sdepmap_model * my_sdepmap_model;
    impmap_model * my_impmap_model;
    calmap_model * my_calmap_model;
    geometry_model * my_geometry_model;
    extended_model * my_extended_model;
    prediction_model * my_prediction_model;
    verimap_model * my_verimap_model;
    tagart_model * my_tagart_model;

    void create_spell_table();
    void create_article_table();
    void create_author_table();
    void create_tag_table();
    void create_code_table();
    void create_authorship_table();
    void create_sdepmap_table();
    void create_impmap_table();
    void create_calmap_table();
    void create_geometry_table();
    void create_extended_table();
    void create_prediction_table();
    void create_verimap_table();
    void create_tagart_table();

    int load_conf_file();

public slots:
    void update_spell_table();
    void update_article_table();
    void update_tag_table();
    void update_code_table();
    void update_authorship_table();
    void update_sdepmap_table();
    void update_impmap_table();
    void update_calmap_table();
    void update_geometry_table();
    void update_extended_table();
    void update_prediction_table();
    void update_verimap_table();
    void update_tagart_table();
    void on_spell_table_itemDoubleClicked(QTableWidgetItem *item);


private slots:
    void on_spell_v_scroll_valueChanged(int value);
    void on_article_v_scroll_valueChanged(int value);
    void on_tag_v_scroll_valueChanged(int value);
    void on_code_v_scroll_valueChanged(int value);
    void on_authorship_v_scroll_valueChanged(int value);
    void on_sdepmap_v_scroll_valueChanged(int value);
    void on_impmap_v_scroll_valueChanged(int value);
    void on_calmap_v_scroll_valueChanged(int value);
    void on_geometry_v_scroll_valueChanged(int value);
    void on_extended_v_scroll_valueChanged(int value);
    void on_prediction_v_scroll_valueChanged(int value);
    void on_verimap_v_scroll_valueChanged(int value);
    void on_tagart_v_scroll_valueChanged(int value);

    void on_btn_spell_create_clicked();

    void delete_spell();

    void on_spell_table_itemClicked(QTableWidgetItem *item);

private:
    Ui::MainWindow *ui;
    QString db_driver;
    QString db_host_name;
    QString db_name;
    QString db_user;
    QString db_pswd;
    QString articles_path;
    QString latex_engine; // "on" if enabled, otherwise not enabled ("off")
    QString latex_spell_macros_file; // if empty, no file specified, if starts '/' then absolute filename, otherwise relative filename to user's HOME

    void initialize_latex_engine();
};
#endif // MAINWINDOW_H
