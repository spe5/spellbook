#include "calmap.h"
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>

calmap_model::calmap_model(QObject *parent) : QObject(parent)
{

}

int calmap_model::count_calmaps(const QString like_pattern){
    // return -1 on error, otherwise return number of tags, which match given filters
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM calculation;";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            int found_calmaps = query.value(0).toInt();
            return found_calmaps;
        }
        return -1;
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int calmap_model::count_calmaps(){
    // return number of all calmaps (all filters permissive) or -1 on error
    return count_calmaps("");
}


int calmap_model::create_calmap(const calmap_item new_calmap){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "INSERT INTO calculation VALUES ('" + new_calmap.ext_id + "','" + new_calmap.code_id + "');";
        qDebug() << sql_command;
        db.exec(sql_command);
    }
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM calculation WHERE extended_id='" + new_calmap.ext_id + "', code_id='" + new_calmap.code_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        if(query.isActive()){
            int found_calmaps = query.value(0).toInt();
            if(found_calmaps == 1)
                return 1; // creation succeeded

            return 0; // creation failed inside PostgreSQL
        }
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int calmap_model::read_calmap(QString ext_id, QString code_id, calmap_item *buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT * FROM calculation WHERE extended_id='" + ext_id + "' AND code_id='" + code_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            if(query.size() != 6)
                return 0; // read failed inside PostgreSQL
            QString ext_id = query.value(0).toString();
            QString code_id = query.value(1).toString();
            qDebug() << "Got calmap with ext_id=" + ext_id + ", code_id=" + code_id;
            buffer->ext_id = ext_id;
            buffer->code_id = code_id;
            return 1; // read succeeded
        }
    }
    return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int calmap_model::update_calmap(const calmap_item existing_calmap){

}

int calmap_model::delete_calmap(QString ext_id, QString code_id){

}

int calmap_model::read_calmaps(int limit, int offset, std::list<calmap_item*> &buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(!db.isValid())
        return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.

    QString sql_command = "SELECT * FROM calculation LIMIT " + QString::number(limit) + " OFFSET " + QString::number(offset) + ";";
    qDebug() << sql_command;
    QSqlQuery query(sql_command, db);
    query.first();
    for(int row = 0; row < limit; row++){
        if(!query.isActive())
            return -1; // read failed inside PostgreSQL
        //if(query.size() != 6)
        //    return 0; // read failed inside PostgreSQL
        if(!query.isValid())
            return 1; // if query is not valid, then all records must be already in buffer, lets return success
        QString ext_id = query.value(0).toString();
        QString code_id = query.value(1).toString();
        qDebug() << "Got calmap with ext_id=" + ext_id + ", code_id=" + code_id;
        calmap_item* another_calmap = new calmap_item();
        another_calmap->ext_id = ext_id;
        another_calmap->code_id = code_id;
        buffer.push_back(another_calmap);
        query.next();

    }
    return 1; // read succeeded
}

