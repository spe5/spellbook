#ifndef EXTENDED_MODEL_H
#define EXTENDED_MODEL_H

#include <QObject>

struct extended_item{
    QString extended_id;
    QString geometry_id;
    QString code_preconditions;
    QString what_to_calculate;
};

class extended_model : public QObject
{
    Q_OBJECT
public:
    explicit extended_model(QObject *parent = nullptr);

    int count_extendeds(const QString like_pattern);
    int count_extendeds();
    int create_extended(const extended_item new_extended);
    int read_extended(QString extended_id, extended_item *buffer);
    int update_extended(const extended_item existing_extended);
    int delete_extended(QString extended_id);
    int read_extendeds(int limit, int offset, std::list<extended_item*> &buffer);

    int visible_rows;
    int offset;

signals:

};

#endif // EXTENDED_MODEL_H
