#ifndef SDEPMAP_MODEL_H
#define SDEPMAP_MODEL_H

#include <QObject>

struct sdepmap_item{
    QString primary_spell_id;
    QString secondary_spell_id;
    int relation;
};

class sdepmap_model : public QObject
{
    Q_OBJECT
public:
    explicit sdepmap_model(QObject *parent = nullptr);

    int count_sdepmaps(const QString like_pattern);
    int count_sdepmaps();
    int create_sdepmap(const sdepmap_item new_sdepmap);
    int read_sdepmap(QString primary_spell_id, QString secondary_spell_id, sdepmap_item *buffer);
    int update_sdepmap(const sdepmap_item existing_sdepmap);
    int delete_sdepmap(QString primary_spell_id, QString secondary_spell_id);
    int read_sdepmaps(int limit, int offset, std::list<sdepmap_item*> &buffer);

    int visible_rows;
    int offset;

signals:

};

#endif // SDEPMAP_MODEL_H
