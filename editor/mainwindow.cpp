#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QSqlDatabase>
#include <QDir>
#include <QFile>
#include <QTableWidget>
#include "dialog_spell_edit.h"
#include <QShortcut>
#include <QTableWidgetSelectionRange>

#include <QGuiApplication>
#include <QFile>
#include <QCommandLineParser>
#include <QDebug>
#include <QPainter>
#include <QPixmap>
#include <QTimer>


int MainWindow::load_conf_file(){
    // loads user configuration from various places (global conf file, local conf file, command line args, etc)
    // returns 0 on success, 1 on error

    db_driver = "";
    db_host_name = "";
    db_name = "";
    db_user = "";
    db_pswd = "";
    articles_path = "";
    latex_engine = "on"; // by default, all features are enabled
    latex_spell_macros_file = ".spellbook.tex"; // default value, no file specified

    // local conf file is expected at HOME/.spellbook.conf and its example can be found in REPO_HOME/editor/.spellbook.conf
    QString user_home = QDir::homePath();
    QString local_conf_file = user_home + "/.spellbook.conf";
    if(QFile::exists(local_conf_file) == false){
        qDebug() << "[ERROR] Local configuration file " + local_conf_file + "does NOT exists.";
        return 1;
    }
    QFile file(local_conf_file);
    if (!file.open(QIODevice::ReadOnly)){
        qDebug() << "[ERROR] Failed to open configuration file " + local_conf_file + " for reading.";
        return 1;
    }
    QTextStream in(&file);
    while(!in.atEnd()) {
        QString line = in.readLine();
        QStringList fields = line.split("=");
        qDebug() << fields;
        if(fields[0].compare("database_engine") == 0){
            if(fields[1].compare("postgres") == 0)
                db_driver = "QPSQL"; // string defined by Qt documentation in class QSqlDatabase
            else{
                qDebug() << "[ERROR] Unknown database engine in configuration file " + local_conf_file + " as '" + fields[1] + "'";
                file.close();
                return 1;
            }
        }
        if(fields[0].compare("database_host") == 0){
            db_host_name = fields[1];
        }
        if(fields[0].compare("database_name") == 0){
            db_name = fields[1];
        }
        if(fields[0].compare("database_user") == 0){
            db_user = fields[1];
        }
        if(fields[0].compare("database_pswd") == 0){
            db_pswd = fields[1];
        }
        if(fields[0].compare("articles_path") == 0){
            articles_path = fields[1];
        }
        if(fields[0].compare("latex_engine") == 0){
            if(fields[1].compare("on") == 0)
                latex_spell_macros_file = "on";
            else
                latex_spell_macros_file = "off";
        }
        if(fields[0].compare("latex_macros") == 0){
            latex_spell_macros_file = fields[1];
        }
    }

    if(db_driver.compare("") == 0){
        file.close();
        return 1;
    }
    if(db_host_name.compare("") == 0){
        file.close();
        return 1;
    }
    if(db_name.compare("") == 0){
        file.close();
        return 1;
    }
    if(db_user.compare("") == 0){
        file.close();
        return 1;
    }
    if(db_pswd.compare("") == 0){
        file.close();
        return 1;
    }

    file.close();
    return 0;
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    if(load_conf_file() != 0){
        qDebug() << "[ERROR] Failed to load user configuration. Exiting.";
        exit(EXIT_FAILURE);
        return;
    }
    QSqlDatabase db = QSqlDatabase::addDatabase(db_driver);
    db.setHostName(db_host_name);
    db.setDatabaseName(db_name);
    db.setUserName(db_user);
    db.setPassword(db_pswd);
    bool ok = db.open();
    if(!ok){
        qDebug() << "[ERROR] Failed to connect to the database. Exiting.";
        exit(EXIT_FAILURE);
        return;
    }

    create_spell_table();
    create_article_table();
    create_author_table();
    create_tag_table();
    create_code_table();
    create_authorship_table();
    create_sdepmap_table();
    create_impmap_table();
    create_calmap_table();
    create_geometry_table();
    create_extended_table();
    create_prediction_table();
    create_verimap_table();
    create_tagart_table();

    update_spell_table();
    update_article_table();
    update_tag_table();
    update_code_table();
    update_authorship_table();
    update_sdepmap_table();
    update_impmap_table();
    update_calmap_table();
    update_geometry_table();
    update_extended_table();
    update_prediction_table();
    update_verimap_table();
    update_tagart_table();

    //my_spell_model->migration_2021_10_10(); // already executed one, do NOT execute it again !!!

    this->showMaximized();

    bool success = QObject::connect(my_spell_model, SIGNAL(update_spell_table()), this, SLOT(update_spell_table()));
    Q_ASSERT(success);
    //success = QObject::connect(ui->spell_table, &QTableWidget::itemDoubleClicked, this, &MainWindow::on_spell_table_itemDoubleClicked);
    //Q_ASSERT(success);
    QShortcut *shortcut = new QShortcut(QKeySequence("Del"), this);
    QObject::connect(shortcut, &QShortcut::activated, this, &MainWindow::delete_spell);

    initialize_latex_engine();
}

MainWindow::~MainWindow()
{
    //delete my_tex_guard;
    delete ui;
}

void MainWindow::initialize_latex_engine(){
    // create singleton of the LaTeX engine and load all Spellbook-specific LaTeX macros
    if(latex_engine.compare("on") != 0){
        qDebug() << "[INFO] LaTeX engine is turned off.";
        return;
    }
    //my_tex_guard = new TexGuard();
    if(latex_spell_macros_file.compare("") == 0){
        qDebug() << "[WARN] No file with latex macros specified, but LaTeX engine initialized. LaTeX rendering will probably fail.";
        return;
    }
    if(latex_spell_macros_file[0] != '/'){ // convert relative filename to absolute
        QString user_home = QDir::homePath();
        latex_spell_macros_file = user_home + "/" + latex_spell_macros_file;
    }
    if(QFile::exists(latex_spell_macros_file) == false){
        qDebug() << "[ERROR] File with LaTeX spell-related macros does NOT exists: " + latex_spell_macros_file;
        return;
    }
    QFile file(latex_spell_macros_file);
    if (file.open(QIODevice::ReadOnly) == false){
        qDebug() << "[ERROR] Failed to open file for reading: " + latex_spell_macros_file;
        return;
    }
    QTextStream in(&file);
    QString latex_macros = in.readAll();
    //tex::LaTeX::parse(latex_macros.toStdWString(), 500, 20, 10, tex::BLACK);
}

void MainWindow::create_spell_table(){
    my_spell_model = new spell_model();
    ui->spell_table->setColumnWidth(0, 250); // column with spell_id, which is 32 byte long hash
    ui->spell_table->setColumnWidth(2, 500); // column with spell name, which can be quite large
    //ui->spell_table->setColumnHidden(0, true);
    //ui->spell_table->setColumnHidden(3, true);

    my_spell_model->visible_rows = 20; // todo VIEW
    my_spell_model->offset = 0; // todo VIEW
    ui->spell_table->setRowCount(my_spell_model->visible_rows);
    //tableWidget->setColumnCount(5);
    std::list<spell_item*> my_buffer;
    my_spell_model->read_spells(my_spell_model->visible_rows, my_spell_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " spells";
    for(int row = 0; row < my_spell_model->visible_rows; row++){
        QTableWidgetItem *newSpellIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->spell_id));
        ui->spell_table->setItem(row, 0, newSpellIDItem);
        QTableWidgetItem *newSpellTypeItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->spell_type));
        ui->spell_table->setItem(row, 1, newSpellTypeItem);
        QTableWidgetItem *newSpellNameItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->name));
        ui->spell_table->setItem(row, 2, newSpellNameItem);
        QTableWidgetItem *newSpellCostItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->use_cost));
        ui->spell_table->setItem(row, 3, newSpellCostItem);
        QTableWidgetItem *newSpellCurrencyItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->use_cost_currency));
        ui->spell_table->setItem(row, 4, newSpellCurrencyItem);
        my_buffer.pop_front();
    }
    ui->spell_v_scroll->setMinimum(0);
    int max_candidate1 = my_spell_model->count_spells() - my_spell_model->visible_rows;
    int max_candidate2 = 0;
    if(max_candidate1 < max_candidate2)
        ui->spell_v_scroll->setMaximum(max_candidate2);
    else
        ui->spell_v_scroll->setMaximum(max_candidate1);
    ui->spell_v_scroll->setPageStep(my_spell_model->visible_rows);
    ui->spell_v_scroll->setSingleStep(1);
    //ComboBoxItemDelegate* cbid = new ComboBoxItemDelegate(ui->spell_table);
    // ComboBoxItemDelegate* cbid = new ComboBoxItemDelegate(this);
    // ComboBox only in column 2
    //ui->spell_table->setItemDelegateForColumn(1, cbid);
    //connect(spell_table_model,SIGNAL(dataChanged(QModelIndex&,QModelIndex&)),ui->spell_table,SLOT(update(QModelIndex&)));
    //ui->spell_table->show();
}

void MainWindow::create_article_table(){
    my_article_model = new article_model();
    ui->article_table->setColumnWidth(0, 280); // column with spell_id, which is 32 byte long hash
    ui->article_table->setColumnWidth(1, 400); // column with spell name, which can be quite large
    ui->article_table->setColumnWidth(2, 150); // column with spell name, which can be quite large
    ui->article_table->setColumnWidth(3, 100); // column with spell name, which can be quite large
    ui->article_table->setColumnWidth(4, 550); // column with spell name, which can be quite large
    ui->article_table->setColumnWidth(5, 100); // column with spell name, which can be quite large
    ui->article_table->setColumnWidth(6, 50); // column with spell name, which can be quite large
    ui->article_table->setColumnWidth(7, 50); // column with spell name, which can be quite large

    my_article_model->visible_rows = 20; // todo VIEW
    my_article_model->offset = 0; // todo VIEW
    ui->article_table->setRowCount(my_article_model->visible_rows);
    std::list<article_item*> my_buffer2;
    my_article_model->read_articles(my_article_model->visible_rows, my_article_model->offset, my_buffer2);
    qDebug() << "Got " + QString::number(my_buffer2.size()) + " articles";
    for(int row = 0; row < my_article_model->visible_rows; row++){
        QTableWidgetItem *newArticleIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer2.front()->article_id));
        ui->article_table->setItem(row, 0, newArticleIDItem);
        QTableWidgetItem *newArticleNameItem = new QTableWidgetItem(tr("%1").arg(my_buffer2.front()->name));
        ui->article_table->setItem(row, 1, newArticleNameItem);
        QTableWidgetItem *newArticleAuthorItem = new QTableWidgetItem(tr("%1").arg(my_buffer2.front()->author));
        ui->article_table->setItem(row, 2, newArticleAuthorItem);
        QTableWidgetItem *newArticleLicenseItem = new QTableWidgetItem(tr("%1").arg(my_buffer2.front()->license));
        ui->article_table->setItem(row, 3, newArticleLicenseItem);
        QTableWidgetItem *newArticleFilenameItem = new QTableWidgetItem(tr("%1").arg(my_buffer2.front()->filename));
        ui->article_table->setItem(row, 4, newArticleFilenameItem);
        QTableWidgetItem *newArticleCostItem = new QTableWidgetItem(tr("%1").arg(my_buffer2.front()->cost));
        ui->article_table->setItem(row, 5, newArticleCostItem);
        QTableWidgetItem *newArticleIsBookItem = new QTableWidgetItem(tr("%1").arg(QString::number(my_buffer2.front()->is_book)));
        ui->article_table->setItem(row, 6, newArticleIsBookItem);
        QTableWidgetItem *newArticleHasExperimentItem = new QTableWidgetItem(tr("%1").arg(QString::number(my_buffer2.front()->has_experiment)));
        ui->article_table->setItem(row, 7, newArticleHasExperimentItem);
        my_buffer2.pop_front();
    }
    ui->article_v_scroll->setMinimum(0);
    int max_candidate1 = my_article_model->count_articles() - my_article_model->visible_rows;
    int max_candidate2 = 0;
    if(max_candidate1 < max_candidate2)
        ui->article_v_scroll->setMaximum(max_candidate2);
    else
        ui->article_v_scroll->setMaximum(max_candidate1);
    ui->article_v_scroll->setMaximum(my_article_model->count_articles() - my_article_model->visible_rows);
    ui->article_v_scroll->setPageStep(my_article_model->visible_rows);
    ui->article_v_scroll->setSingleStep(1);
}

void MainWindow::create_author_table(){
    my_author_model = new author_model();
    ui->author_table->setColumnWidth(0, 280); // column with spell_id, which is 32 byte long hash
    ui->author_table->setColumnWidth(1, 400); // column with spell name, which can be quite large
    ui->author_table->setColumnWidth(2, 150); // column with spell name, which can be quite large
    ui->author_table->setColumnWidth(3, 100); // column with spell name, which can be quite large

    my_author_model->visible_rows = 20; // todo VIEW
    my_author_model->offset = 0; // todo VIEW
    ui->author_table->setRowCount(my_author_model->visible_rows);
    std::list<author_item*> my_buffer3;
    my_author_model->read_authors(my_author_model->visible_rows, my_author_model->offset, my_buffer3);
    qDebug() << "Got " + QString::number(my_buffer3.size()) + " authors";
    for(int row = 0; row < my_author_model->visible_rows; row++){
        if(!my_buffer3.empty()){
            QTableWidgetItem *newAuthorIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer3.front()->author_id));
            ui->author_table->setItem(row, 0, newAuthorIDItem);
            QTableWidgetItem *newAuthorFirstNameItem = new QTableWidgetItem(tr("%1").arg(my_buffer3.front()->first_name));
            ui->author_table->setItem(row, 1, newAuthorFirstNameItem);
            QTableWidgetItem *newAuthorSecondNameItem = new QTableWidgetItem(tr("%1").arg(my_buffer3.front()->second_name));
            ui->author_table->setItem(row, 2, newAuthorSecondNameItem);
            QTableWidgetItem *newAuthorBillingNumberItem = new QTableWidgetItem(tr("%1").arg(my_buffer3.front()->billing_number));
            ui->author_table->setItem(row, 3, newAuthorBillingNumberItem);
            my_buffer3.pop_front();
        }else{
            QTableWidgetItem *newAuthorIDItem = new QTableWidgetItem();
            ui->author_table->setItem(row, 0, newAuthorIDItem);
            QTableWidgetItem *newAuthorFirstNameItem = new QTableWidgetItem();
            ui->author_table->setItem(row, 1, newAuthorFirstNameItem);
            QTableWidgetItem *newAuthorSecondNameItem = new QTableWidgetItem();
            ui->author_table->setItem(row, 2, newAuthorSecondNameItem);
            QTableWidgetItem *newAuthorBillingNumberItem = new QTableWidgetItem();
            ui->author_table->setItem(row, 3, newAuthorBillingNumberItem);
        }
    }
    ui->author_v_scroll->setMinimum(0);
    int max_candidate1 = my_author_model->count_authors() - my_author_model->visible_rows;
    int max_candidate2 = 0;
    if(max_candidate1 < max_candidate2)
        ui->author_v_scroll->setMaximum(max_candidate2);
    else
        ui->author_v_scroll->setMaximum(max_candidate1);
    ui->author_v_scroll->setPageStep(my_author_model->visible_rows);
    ui->author_v_scroll->setSingleStep(1);
}

void MainWindow::create_tag_table(){
    my_tag_model = new tag_model();
    ui->tag_table->setColumnWidth(0, 280); // column with spell_id, which is 32 byte long hash
    ui->tag_table->setColumnWidth(1, 400); // column with spell name, which can be quite large

    my_tag_model->visible_rows = 20; // todo VIEW
    my_tag_model->offset = 0; // todo VIEW
    ui->tag_table->setRowCount(my_tag_model->visible_rows);
    std::list<tag_item*> my_buffer4;
    my_tag_model->read_tags(my_tag_model->visible_rows, my_tag_model->offset, my_buffer4);
    qDebug() << "Got " + QString::number(my_buffer4.size()) + " tags";
    for(int row = 0; row < my_tag_model->visible_rows; row++){
        if(!my_buffer4.empty()){
            QTableWidgetItem *newTagIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer4.front()->tag_id));
            ui->tag_table->setItem(row, 0, newTagIDItem);
            QTableWidgetItem *newTagNameItem = new QTableWidgetItem(tr("%1").arg(my_buffer4.front()->tag_name));
            ui->tag_table->setItem(row, 1, newTagNameItem);
            my_buffer4.pop_front();
        }else{
            QTableWidgetItem *newTagIDItem = new QTableWidgetItem();
            ui->tag_table->setItem(row, 0, newTagIDItem);
            QTableWidgetItem *newTagNameItem = new QTableWidgetItem();
            ui->tag_table->setItem(row, 1, newTagNameItem);
        }
    }
    ui->tag_v_scroll->setMinimum(0);
    int max_candidate1 = my_tag_model->count_tags() - my_tag_model->visible_rows;
    int max_candidate2 = 0;
    if(max_candidate1 < max_candidate2)
        ui->tag_v_scroll->setMaximum(max_candidate2);
    else
        ui->tag_v_scroll->setMaximum(max_candidate1);
    ui->tag_v_scroll->setPageStep(my_tag_model->visible_rows);
    ui->tag_v_scroll->setSingleStep(1);
}

void MainWindow::create_code_table(){
    my_code_model = new code_model();
    ui->code_table->setColumnWidth(0, 280); // column with spell_id, which is 32 byte long hash
    ui->code_table->setColumnWidth(1, 400); // column with spell name, which can be quite large

    my_code_model->visible_rows = 20; // todo VIEW
    my_code_model->offset = 0; // todo VIEW
    ui->code_table->setRowCount(my_code_model->visible_rows);
    std::list<code_item*> my_buffer5;
    my_code_model->read_codes(my_code_model->visible_rows, my_code_model->offset, my_buffer5);
    qDebug() << "Got " + QString::number(my_buffer5.size()) + " codes";
    for(int row = 0; row < my_code_model->visible_rows; row++){
        if(!my_buffer5.empty()){
            QTableWidgetItem *newCodeIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer5.front()->code_id));
            ui->code_table->setItem(row, 0, newCodeIDItem);
            QTableWidgetItem *newCodeFileNameItem = new QTableWidgetItem(tr("%1").arg(my_buffer5.front()->filename));
            ui->code_table->setItem(row, 1, newCodeFileNameItem);
            QTableWidgetItem *newUseCostItem = new QTableWidgetItem(tr("%1").arg(my_buffer5.front()->use_cost));
            ui->code_table->setItem(row, 2, newUseCostItem);
            QTableWidgetItem *newCurrencyItem = new QTableWidgetItem(tr("%1").arg(my_buffer5.front()->use_cost_currency));
            ui->code_table->setItem(row, 3, newCurrencyItem);
            QTableWidgetItem *newAccuracyItem = new QTableWidgetItem(tr("%1").arg(my_buffer5.front()->accuracy));
            ui->code_table->setItem(row, 4, newAccuracyItem);
            QTableWidgetItem *newComplexityItem = new QTableWidgetItem(tr("%1").arg(my_buffer5.front()->complexity));
            ui->code_table->setItem(row, 5, newComplexityItem);
            my_buffer5.pop_front();
        }else{
            QTableWidgetItem *newCodeIDItem = new QTableWidgetItem();
            ui->code_table->setItem(row, 0, newCodeIDItem);
            QTableWidgetItem *newCodeFileNameItem = new QTableWidgetItem();
            ui->code_table->setItem(row, 1, newCodeFileNameItem);
            QTableWidgetItem *newUseCostItem = new QTableWidgetItem();
            ui->code_table->setItem(row, 2, newUseCostItem);
            QTableWidgetItem *newCurrencyItem = new QTableWidgetItem();
            ui->code_table->setItem(row, 3, newCurrencyItem);
            QTableWidgetItem *newAccuracyItem = new QTableWidgetItem();
            ui->code_table->setItem(row, 4, newAccuracyItem);
            QTableWidgetItem *newComplexityItem = new QTableWidgetItem();
            ui->code_table->setItem(row, 5, newComplexityItem);
        }
    }
    ui->code_v_scroll->setMinimum(0);
    int max_candidate1 = my_code_model->count_codes() - my_code_model->visible_rows;
    int max_candidate2 = 0;
    if(max_candidate1 < max_candidate2)
        ui->code_v_scroll->setMaximum(max_candidate2);
    else
        ui->code_v_scroll->setMaximum(max_candidate1);
    ui->code_v_scroll->setPageStep(my_code_model->visible_rows);
    ui->code_v_scroll->setSingleStep(1);
}

void MainWindow::create_authorship_table(){
    my_authorship_model = new authorship_model();
    ui->authorship_table->setColumnWidth(0, 280); // column with spell_id, which is 32 byte long hash
    ui->authorship_table->setColumnWidth(1, 400); // column with spell name, which can be quite large

    my_authorship_model->visible_rows = 20; // todo VIEW
    my_authorship_model->offset = 0; // todo VIEW
    ui->authorship_table->setRowCount(my_authorship_model->visible_rows);
    std::list<authorship_item*> my_buffer6;
    my_authorship_model->read_authorships(my_authorship_model->visible_rows, my_authorship_model->offset, my_buffer6);
    qDebug() << "Got " + QString::number(my_buffer6.size()) + " authorship";
    for(int row = 0; row < my_authorship_model->visible_rows; row++){
        if(!my_buffer6.empty()){
            QTableWidgetItem *newSpellIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer6.front()->spell_id));
            ui->authorship_table->setItem(row, 0, newSpellIDItem);
            QTableWidgetItem *newAuthorIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer6.front()->author_id));
            ui->authorship_table->setItem(row, 1, newAuthorIDItem);
            my_buffer6.pop_front();
        }else{
            QTableWidgetItem *newSpellIDItem = new QTableWidgetItem();
            ui->authorship_table->setItem(row, 0, newSpellIDItem);
            QTableWidgetItem *newAuthorIDItem = new QTableWidgetItem();
            ui->authorship_table->setItem(row, 1, newAuthorIDItem);
        }
    }
    ui->authorship_v_scroll->setMinimum(0);
    int max_candidate1 = my_authorship_model->count_authorships() - my_authorship_model->visible_rows;
    int max_candidate2 = 0;
    if(max_candidate1 < max_candidate2)
        ui->authorship_v_scroll->setMaximum(max_candidate2);
    else
        ui->authorship_v_scroll->setMaximum(max_candidate1);
    ui->authorship_v_scroll->setPageStep(my_authorship_model->visible_rows);
    ui->authorship_v_scroll->setSingleStep(1);
}

void MainWindow::create_sdepmap_table(){
    my_sdepmap_model = new sdepmap_model();
    ui->sdepmap_table->setColumnWidth(0, 280); // column with spell_id, which is 32 byte long hash
    ui->sdepmap_table->setColumnWidth(1, 280); // column with spell name, which can be quite large

    my_sdepmap_model->visible_rows = 20; // todo VIEW
    my_sdepmap_model->offset = 0; // todo VIEW
    ui->sdepmap_table->setRowCount(my_sdepmap_model->visible_rows);
    std::list<sdepmap_item*> my_buffer7;
    my_sdepmap_model->read_sdepmaps(my_sdepmap_model->visible_rows, my_sdepmap_model->offset, my_buffer7);
    qDebug() << "Got " + QString::number(my_buffer7.size()) + " sdepmaps";
    for(int row = 0; row < my_sdepmap_model->visible_rows; row++){
        if(!my_buffer7.empty()){
            QTableWidgetItem *newPrimarySpellIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer7.front()->primary_spell_id));
            ui->sdepmap_table->setItem(row, 0, newPrimarySpellIDItem);
            QTableWidgetItem *newSecondarySpellIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer7.front()->secondary_spell_id));
            ui->sdepmap_table->setItem(row, 1, newSecondarySpellIDItem);
            QTableWidgetItem *newRelationItem = new QTableWidgetItem(tr("%1").arg(QString::number(my_buffer7.front()->relation)));
            ui->sdepmap_table->setItem(row, 2, newRelationItem);
            my_buffer7.pop_front();
        }else{
            QTableWidgetItem *newPrimarySpellIDItem = new QTableWidgetItem();
            ui->sdepmap_table->setItem(row, 0, newPrimarySpellIDItem);
            QTableWidgetItem *newSecondarySpellIDItem = new QTableWidgetItem();
            ui->sdepmap_table->setItem(row, 1, newSecondarySpellIDItem);
            QTableWidgetItem *newRelationItem = new QTableWidgetItem();
            ui->sdepmap_table->setItem(row, 2, newRelationItem);
        }
    }
    ui->sdepmap_v_scroll->setMinimum(0);
    int max_candidate1 = my_sdepmap_model->count_sdepmaps() - my_sdepmap_model->visible_rows;
    int max_candidate2 = 0;
    if(max_candidate1 < max_candidate2)
        ui->sdepmap_v_scroll->setMaximum(max_candidate2);
    else
        ui->sdepmap_v_scroll->setMaximum(max_candidate1);
    ui->sdepmap_v_scroll->setPageStep(my_sdepmap_model->visible_rows);
    ui->sdepmap_v_scroll->setSingleStep(1);
}

void MainWindow::create_impmap_table(){
    my_impmap_model = new impmap_model();
    ui->impmap_table->setColumnWidth(0, 280); // column with spell_id, which is 32 byte long hash
    ui->impmap_table->setColumnWidth(1, 280); // column with spell name, which can be quite large

    my_impmap_model->visible_rows = 20; // todo VIEW
    my_impmap_model->offset = 0; // todo VIEW
    ui->impmap_table->setRowCount(my_impmap_model->visible_rows);
    std::list<impmap_item*> my_buffer;
    my_impmap_model->read_impmaps(my_impmap_model->visible_rows, my_impmap_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " impmaps";
    for(int row = 0; row < my_impmap_model->visible_rows; row++){
        if(!my_buffer.empty()){
            QTableWidgetItem *newSpellIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->spell_id));
            ui->impmap_table->setItem(row, 0, newSpellIDItem);
            QTableWidgetItem *newCodeIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->code_id));
            ui->impmap_table->setItem(row, 1, newCodeIDItem);
            my_buffer.pop_front();
        }else{
            QTableWidgetItem *newSpellIDItem = new QTableWidgetItem();
            ui->impmap_table->setItem(row, 0, newSpellIDItem);
            QTableWidgetItem *newCodeIDItem = new QTableWidgetItem();
            ui->impmap_table->setItem(row, 1, newCodeIDItem);
        }
    }
    ui->impmap_v_scroll->setMinimum(0);
    int max_candidate1 = my_impmap_model->count_impmaps() - my_impmap_model->visible_rows;
    int max_candidate2 = 0;
    if(max_candidate1 < max_candidate2)
        ui->impmap_v_scroll->setMaximum(max_candidate2);
    else
        ui->impmap_v_scroll->setMaximum(max_candidate1);
    ui->impmap_v_scroll->setPageStep(my_impmap_model->visible_rows);
    ui->impmap_v_scroll->setSingleStep(1);
}


void MainWindow::create_calmap_table(){
    my_calmap_model = new calmap_model();
    ui->calmap_table->setColumnWidth(0, 280); // column with spell_id, which is 32 byte long hash
    ui->calmap_table->setColumnWidth(1, 280); // column with spell name, which can be quite large

    my_calmap_model->visible_rows = 20; // todo VIEW
    my_calmap_model->offset = 0; // todo VIEW
    ui->calmap_table->setRowCount(my_calmap_model->visible_rows);
    std::list<calmap_item*> my_buffer;
    my_calmap_model->read_calmaps(my_calmap_model->visible_rows, my_calmap_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " calmaps";
    for(int row = 0; row < my_calmap_model->visible_rows; row++){
        if(!my_buffer.empty()){
            QTableWidgetItem *newExtIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->ext_id));
            ui->calmap_table->setItem(row, 0, newExtIDItem);
            QTableWidgetItem *newCodeIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->code_id));
            ui->calmap_table->setItem(row, 1, newCodeIDItem);
            my_buffer.pop_front();
        }else{
            QTableWidgetItem *newExtIDItem = new QTableWidgetItem();
            ui->calmap_table->setItem(row, 0, newExtIDItem);
            QTableWidgetItem *newCodeIDItem = new QTableWidgetItem();
            ui->calmap_table->setItem(row, 1, newCodeIDItem);
        }
    }
    ui->calmap_v_scroll->setMinimum(0);
    int max_candidate1 = my_calmap_model->count_calmaps() - my_calmap_model->visible_rows;
    int max_candidate2 = 0;
    if(max_candidate1 < max_candidate2)
        ui->calmap_v_scroll->setMaximum(max_candidate2);
    else
        ui->calmap_v_scroll->setMaximum(max_candidate1);
    ui->calmap_v_scroll->setPageStep(my_calmap_model->visible_rows);
    ui->calmap_v_scroll->setSingleStep(1);
}

void MainWindow::create_geometry_table(){
    my_geometry_model = new geometry_model();
    ui->geometry_table->setColumnWidth(0, 280); // column with spell_id, which is 32 byte long hash
    ui->geometry_table->setColumnWidth(1, 280); // column with spell name, which can be quite large

    my_geometry_model->visible_rows = 20; // todo VIEW
    my_geometry_model->offset = 0; // todo VIEW
    ui->geometry_table->setRowCount(my_geometry_model->visible_rows);
    std::list<geometry_item*> my_buffer;
    my_geometry_model->read_geometries(my_geometry_model->visible_rows, my_geometry_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " geometries";
    for(int row = 0; row < my_geometry_model->visible_rows; row++){
        if(!my_buffer.empty()){
            QTableWidgetItem *newGeometryIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->geometry_id));
            ui->geometry_table->setItem(row, 0, newGeometryIDItem);
            QTableWidgetItem *newFilenameItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->filename));
            ui->geometry_table->setItem(row, 1, newFilenameItem);
            my_buffer.pop_front();
        }else{
            QTableWidgetItem *newGeometryIDItem = new QTableWidgetItem();
            ui->geometry_table->setItem(row, 0, newGeometryIDItem);
            QTableWidgetItem *newFilenameItem = new QTableWidgetItem();
            ui->geometry_table->setItem(row, 1, newFilenameItem);
        }
    }
    ui->geometry_v_scroll->setMinimum(0);
    int max_candidate1 = my_geometry_model->count_geometries() - my_geometry_model->visible_rows;
    int max_candidate2 = 0;
    if(max_candidate1 < max_candidate2)
        ui->geometry_v_scroll->setMaximum(max_candidate2);
    else
        ui->geometry_v_scroll->setMaximum(max_candidate1);
    ui->geometry_v_scroll->setPageStep(my_geometry_model->visible_rows);
    ui->geometry_v_scroll->setSingleStep(1);
}

void MainWindow::create_extended_table(){
    my_extended_model = new extended_model();
    ui->extended_table->setColumnWidth(0, 280); // column with spell_id, which is 32 byte long hash
    ui->extended_table->setColumnWidth(1, 280); // column with spell name, which can be quite large
    ui->extended_table->setColumnWidth(2, 280);
    ui->extended_table->setColumnWidth(3, 280);

    my_extended_model->visible_rows = 20; // todo VIEW
    my_extended_model->offset = 0; // todo VIEW
    ui->extended_table->setRowCount(my_extended_model->visible_rows);
    std::list<extended_item*> my_buffer;
    my_extended_model->read_extendeds(my_extended_model->visible_rows, my_extended_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " extendeds";
    for(int row = 0; row < my_extended_model->visible_rows; row++){
        if(!my_buffer.empty()){
            QTableWidgetItem *newExtendedIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->extended_id));
            ui->extended_table->setItem(row, 0, newExtendedIDItem);
            QTableWidgetItem *newGeometryItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->geometry_id));
            ui->extended_table->setItem(row, 1, newGeometryItem);
            QTableWidgetItem *newCodePreconditionItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->code_preconditions));
            ui->extended_table->setItem(row, 2, newCodePreconditionItem);
            QTableWidgetItem *newWhatToCalculateItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->what_to_calculate));
            ui->extended_table->setItem(row, 3, newWhatToCalculateItem);
            my_buffer.pop_front();
        }else{
            QTableWidgetItem *newExtendedIDItem = new QTableWidgetItem();
            ui->extended_table->setItem(row, 0, newExtendedIDItem);
            QTableWidgetItem *newGeometryIDItem = new QTableWidgetItem();
            ui->extended_table->setItem(row, 1, newGeometryIDItem);
            QTableWidgetItem *newCodePreconditionsItem = new QTableWidgetItem();
            ui->extended_table->setItem(row, 2, newCodePreconditionsItem);
            QTableWidgetItem *newWhatToCalculateItem = new QTableWidgetItem();
            ui->extended_table->setItem(row, 3, newWhatToCalculateItem);
        }
    }
    ui->extended_v_scroll->setMinimum(0);
    int max_candidate1 = my_extended_model->count_extendeds() - my_extended_model->visible_rows;
    int max_candidate2 = 0;
    if(max_candidate1 < max_candidate2)
        ui->extended_v_scroll->setMaximum(max_candidate2);
    else
        ui->extended_v_scroll->setMaximum(max_candidate1);
    ui->extended_v_scroll->setPageStep(my_extended_model->visible_rows);
    ui->extended_v_scroll->setSingleStep(1);
}

void MainWindow::create_prediction_table(){
    my_prediction_model = new prediction_model();
    ui->prediction_table->setColumnWidth(0, 280); // column with spell_id, which is 32 byte long hash
    ui->prediction_table->setColumnWidth(1, 280); // column with spell name, which can be quite large
    ui->prediction_table->setColumnWidth(2, 280);
    ui->prediction_table->setColumnWidth(3, 280);

    my_prediction_model->visible_rows = 20; // todo VIEW
    my_prediction_model->offset = 0; // todo VIEW
    ui->prediction_table->setRowCount(my_prediction_model->visible_rows);
    std::list<prediction_item*> my_buffer;
    my_prediction_model->read_predictions(my_prediction_model->visible_rows, my_prediction_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " predictions";
    for(int row = 0; row < my_prediction_model->visible_rows; row++){
        if(!my_buffer.empty()){
            QTableWidgetItem *newPredictionIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->prediction_id));
            ui->prediction_table->setItem(row, 0, newPredictionIDItem);
            QTableWidgetItem *newExtendedIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->extended_id));
            ui->prediction_table->setItem(row, 1, newExtendedIDItem);
            QTableWidgetItem *newMaxResourcesItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->maximum_resources));
            ui->prediction_table->setItem(row, 2, newMaxResourcesItem);
            QTableWidgetItem *newMaxCostItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->maximum_cost));
            ui->prediction_table->setItem(row, 3, newMaxCostItem);
            QTableWidgetItem *newResultItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->result));
            ui->prediction_table->setItem(row, 4, newResultItem);
            my_buffer.pop_front();
        }else{
            QTableWidgetItem *newPredictionIDItem = new QTableWidgetItem();
            ui->prediction_table->setItem(row, 0, newPredictionIDItem);
            QTableWidgetItem *newExtendedIDItem = new QTableWidgetItem();
            ui->prediction_table->setItem(row, 1, newExtendedIDItem);
            QTableWidgetItem *newMaxResourcesItem = new QTableWidgetItem();
            ui->prediction_table->setItem(row, 2, newMaxResourcesItem);
            QTableWidgetItem *newMaxCostItem = new QTableWidgetItem();
            ui->prediction_table->setItem(row, 3, newMaxCostItem);
            QTableWidgetItem *newResultItem = new QTableWidgetItem();
            ui->prediction_table->setItem(row, 4, newResultItem);
        }
    }
    ui->prediction_v_scroll->setMinimum(0);
    int max_candidate1 = my_prediction_model->count_predictions() - my_prediction_model->visible_rows;
    int max_candidate2 = 0;
    if(max_candidate1 < max_candidate2)
        ui->prediction_v_scroll->setMaximum(max_candidate2);
    else
        ui->prediction_v_scroll->setMaximum(max_candidate1);
    ui->prediction_v_scroll->setPageStep(my_prediction_model->visible_rows);
    ui->prediction_v_scroll->setSingleStep(1);
}

void MainWindow::create_verimap_table(){
    my_verimap_model = new verimap_model();
    ui->verimap_table->setColumnWidth(0, 280); // column with spell_id, which is 32 byte long hash
    ui->verimap_table->setColumnWidth(1, 280); // column with spell name, which can be quite large
    ui->verimap_table->setColumnWidth(2, 280);
    ui->verimap_table->setColumnWidth(3, 280);

    my_verimap_model->visible_rows = 20; // todo VIEW
    my_verimap_model->offset = 0; // todo VIEW
    ui->verimap_table->setRowCount(my_verimap_model->visible_rows);
    std::list<verimap_item*> my_buffer;
    my_verimap_model->read_verimaps(my_verimap_model->visible_rows, my_verimap_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " verimaps";
    for(int row = 0; row < my_verimap_model->visible_rows; row++){
        if(!my_buffer.empty()){
            QTableWidgetItem *newVerificationIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->verification_id));
            ui->verimap_table->setItem(row, 0, newVerificationIDItem);
            QTableWidgetItem *newPredictionIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->prediction_id));
            ui->verimap_table->setItem(row, 1, newPredictionIDItem);
            QTableWidgetItem *newExperimentIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->experiment_id));
            ui->verimap_table->setItem(row, 2, newExperimentIDItem);
            QTableWidgetItem *newComparisionItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->comparision));
            ui->verimap_table->setItem(row, 3, newComparisionItem);
            QTableWidgetItem *newGoodEnoughItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->good_enough));
            ui->verimap_table->setItem(row, 4, newGoodEnoughItem);
            my_buffer.pop_front();
        }else{
            QTableWidgetItem *newVerificationIDItem = new QTableWidgetItem();
            ui->verimap_table->setItem(row, 0, newVerificationIDItem);
            QTableWidgetItem *newPredictionIDItem = new QTableWidgetItem();
            ui->verimap_table->setItem(row, 1, newPredictionIDItem);
            QTableWidgetItem *newExperimentItem = new QTableWidgetItem();
            ui->verimap_table->setItem(row, 2, newExperimentItem);
            QTableWidgetItem *newComparisionItem = new QTableWidgetItem();
            ui->verimap_table->setItem(row, 3, newComparisionItem);
            QTableWidgetItem *newGoodEnoughItem = new QTableWidgetItem();
            ui->verimap_table->setItem(row, 4, newGoodEnoughItem);
        }
    }
    ui->verimap_v_scroll->setMinimum(0);
    int max_candidate1 = my_verimap_model->count_verimaps() - my_verimap_model->visible_rows;
    int max_candidate2 = 0;
    if(max_candidate1 < max_candidate2)
        ui->verimap_v_scroll->setMaximum(max_candidate2);
    else
        ui->verimap_v_scroll->setMaximum(max_candidate1);
    ui->verimap_v_scroll->setPageStep(my_verimap_model->visible_rows);
    ui->verimap_v_scroll->setSingleStep(1);
}

void MainWindow::create_tagart_table(){
    my_tagart_model = new tagart_model();
    ui->tagart_table->setColumnWidth(0, 280); // column with spell_id, which is 32 byte long hash
    ui->tagart_table->setColumnWidth(1, 280); // column with spell name, which can be quite large

    my_tagart_model->visible_rows = 20; // todo VIEW
    my_tagart_model->offset = 0; // todo VIEW
    ui->tagart_table->setRowCount(my_tagart_model->visible_rows);
    std::list<tagart_item*> my_buffer;
    my_tagart_model->read_tagarts(my_tagart_model->visible_rows, my_tagart_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " tag-to-article mappings";
    for(int row = 0; row < my_tagart_model->visible_rows; row++){
        if(!my_buffer.empty()){
            QTableWidgetItem *newArticleIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->experiment_id));
            ui->tagart_table->setItem(row, 0, newArticleIDItem);
            QTableWidgetItem *newTagIDItem = new QTableWidgetItem(tr("%1").arg(my_buffer.front()->tag_id));
            ui->tagart_table->setItem(row, 1, newTagIDItem);
            my_buffer.pop_front();
        }else{
            QTableWidgetItem *newArticleIDItem = new QTableWidgetItem();
            ui->tagart_table->setItem(row, 0, newArticleIDItem);
            QTableWidgetItem *newTagIDItem = new QTableWidgetItem();
            ui->tagart_table->setItem(row, 1, newTagIDItem);
        }
    }
    ui->tagart_v_scroll->setMinimum(0);
    int max_candidate1 = my_tagart_model->count_tagarts() - my_tagart_model->visible_rows;
    int max_candidate2 = 0;
    if(max_candidate1 < max_candidate2)
        ui->tagart_v_scroll->setMaximum(max_candidate2);
    else
        ui->tagart_v_scroll->setMaximum(max_candidate1);
    ui->tagart_v_scroll->setPageStep(my_tagart_model->visible_rows);
    ui->tagart_v_scroll->setSingleStep(1);
}

void MainWindow::update_spell_table(){
    qDebug() << "SLOT update_spell_table";
    std::list<spell_item*> my_buffer;
    my_spell_model->read_spells(my_spell_model->visible_rows, my_spell_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " spells";
    for(int row = 0; row < my_spell_model->visible_rows; row++){
        ui->spell_table->item(row, 0)->setText(my_buffer.front()->spell_id);
        ui->spell_table->item(row, 1)->setText(QString::number(my_buffer.front()->spell_type));
        ui->spell_table->item(row, 2)->setText(my_buffer.front()->name);
        ui->spell_table->item(row, 3)->setText(my_buffer.front()->use_cost);
        ui->spell_table->item(row, 4)->setText(my_buffer.front()->use_cost_currency);
        my_buffer.pop_front();
    }
    ui->spell_v_scroll->setMinimum(0);
    int max_candidate1 = my_spell_model->count_spells() - my_spell_model->visible_rows;
    int max_candidate2 = 0;
    if(max_candidate1 < max_candidate2){
        ui->spell_v_scroll->setMaximum(max_candidate2);
        ui->spell_v_scroll->rangeChanged(0, max_candidate2);
    }else{
        ui->spell_v_scroll->setMaximum(max_candidate1);
        ui->spell_v_scroll->rangeChanged(0, max_candidate1);
    }
}

void MainWindow::update_article_table(){
    std::list<article_item*> my_buffer;
    my_article_model->read_articles(my_article_model->visible_rows, my_article_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " articles";
    for(int row = 0; row < my_article_model->visible_rows; row++){
        ui->article_table->item(row, 0)->setText(my_buffer.front()->article_id);
        ui->article_table->item(row, 1)->setText(my_buffer.front()->name);
        ui->article_table->item(row, 2)->setText(my_buffer.front()->author);
        ui->article_table->item(row, 3)->setText(my_buffer.front()->license);
        ui->article_table->item(row, 4)->setText(my_buffer.front()->filename);
        ui->article_table->item(row, 5)->setText(my_buffer.front()->cost);
        ui->article_table->item(row, 6)->setText(QString::number(my_buffer.front()->is_book));
        ui->article_table->item(row, 7)->setText(QString::number(my_buffer.front()->has_experiment));
        my_buffer.pop_front();
    }
}

void MainWindow::update_tag_table(){
    std::list<tag_item*> my_buffer;
    my_tag_model->read_tags(my_tag_model->visible_rows, my_tag_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " tags";
    for(int row = 0; row < my_tag_model->visible_rows; row++){
        if(!my_buffer.empty()){
            ui->tag_table->item(row, 0)->setText(my_buffer.front()->tag_id);
            ui->tag_table->item(row, 1)->setText(my_buffer.front()->tag_name);
            my_buffer.pop_front();
        }else{
            ui->tag_table->item(row, 0)->setText("");
            ui->tag_table->item(row, 1)->setText("");
        }
    }
}

void MainWindow::update_code_table(){
    std::list<code_item*> my_buffer;
    my_code_model->read_codes(my_code_model->visible_rows, my_code_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " codes";
    for(int row = 0; row < my_code_model->visible_rows; row++){
        if(!my_buffer.empty()){
            ui->code_table->item(row, 0)->setText(my_buffer.front()->code_id);
            ui->code_table->item(row, 1)->setText(my_buffer.front()->filename);
            ui->code_table->item(row, 2)->setText(my_buffer.front()->use_cost);
            ui->code_table->item(row, 3)->setText(my_buffer.front()->use_cost_currency);
            ui->code_table->item(row, 4)->setText(my_buffer.front()->accuracy);
            ui->code_table->item(row, 5)->setText(my_buffer.front()->complexity);
            my_buffer.pop_front();
        }else{
            ui->code_table->item(row, 0)->setText("");
            ui->code_table->item(row, 1)->setText("");
            ui->code_table->item(row, 2)->setText("");
            ui->code_table->item(row, 3)->setText("");
            ui->code_table->item(row, 4)->setText("");
            ui->code_table->item(row, 5)->setText("");
        }
    }
}

void MainWindow::update_authorship_table(){
    std::list<authorship_item*> my_buffer;
    my_authorship_model->read_authorships(my_authorship_model->visible_rows, my_authorship_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " authorships";
    for(int row = 0; row < my_authorship_model->visible_rows; row++){
        if(!my_buffer.empty()){
            ui->authorship_table->item(row, 0)->setText(my_buffer.front()->spell_id);
            ui->authorship_table->item(row, 1)->setText(my_buffer.front()->author_id);
            my_buffer.pop_front();
        }else{
            ui->authorship_table->item(row, 0)->setText("");
            ui->authorship_table->item(row, 1)->setText("");
        }
    }
}

void MainWindow::update_sdepmap_table(){
    std::list<sdepmap_item*> my_buffer;
    my_sdepmap_model->read_sdepmaps(my_sdepmap_model->visible_rows, my_sdepmap_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " sdepmaps";
    for(int row = 0; row < my_sdepmap_model->visible_rows; row++){
        if(!my_buffer.empty()){
            ui->sdepmap_table->item(row, 0)->setText(my_buffer.front()->primary_spell_id);
            ui->sdepmap_table->item(row, 1)->setText(my_buffer.front()->secondary_spell_id);
            ui->sdepmap_table->item(row, 2)->setText(QString::number(my_buffer.front()->relation));
            my_buffer.pop_front();
        }else{
            ui->sdepmap_table->item(row, 0)->setText("");
            ui->sdepmap_table->item(row, 1)->setText("");
            ui->sdepmap_table->item(row, 2)->setText("");
        }
    }
}

void MainWindow::update_impmap_table(){
    std::list<impmap_item*> my_buffer;
    my_impmap_model->read_impmaps(my_impmap_model->visible_rows, my_impmap_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " impmaps";
    for(int row = 0; row < my_impmap_model->visible_rows; row++){
        if(!my_buffer.empty()){
            ui->impmap_table->item(row, 0)->setText(my_buffer.front()->spell_id);
            ui->impmap_table->item(row, 1)->setText(my_buffer.front()->code_id);
            my_buffer.pop_front();
        }else{
            ui->impmap_table->item(row, 0)->setText("");
            ui->impmap_table->item(row, 1)->setText("");
        }
    }
}

void MainWindow::update_calmap_table(){
    std::list<calmap_item*> my_buffer;
    my_calmap_model->read_calmaps(my_calmap_model->visible_rows, my_calmap_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " calmaps";
    for(int row = 0; row < my_calmap_model->visible_rows; row++){
        if(!my_buffer.empty()){
            ui->calmap_table->item(row, 0)->setText(my_buffer.front()->ext_id);
            ui->calmap_table->item(row, 1)->setText(my_buffer.front()->code_id);
            my_buffer.pop_front();
        }else{
            ui->calmap_table->item(row, 0)->setText("");
            ui->calmap_table->item(row, 1)->setText("");
        }
    }
}

void MainWindow::update_geometry_table(){
    std::list<geometry_item*> my_buffer;
    my_geometry_model->read_geometries(my_geometry_model->visible_rows, my_geometry_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " geometries";
    for(int row = 0; row < my_geometry_model->visible_rows; row++){
        if(!my_buffer.empty()){
            ui->geometry_table->item(row, 0)->setText(my_buffer.front()->geometry_id);
            ui->geometry_table->item(row, 1)->setText(my_buffer.front()->filename);
            my_buffer.pop_front();
        }else{
            ui->geometry_table->item(row, 0)->setText("");
            ui->geometry_table->item(row, 1)->setText("");
        }
    }
}

void MainWindow::update_extended_table(){
    std::list<extended_item*> my_buffer;
    my_extended_model->read_extendeds(my_extended_model->visible_rows, my_extended_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " extendeds";
    for(int row = 0; row < my_extended_model->visible_rows; row++){
        if(!my_buffer.empty()){
            ui->extended_table->item(row, 0)->setText(my_buffer.front()->extended_id);
            ui->extended_table->item(row, 1)->setText(my_buffer.front()->geometry_id);
            ui->extended_table->item(row, 2)->setText(my_buffer.front()->code_preconditions);
            ui->extended_table->item(row, 3)->setText(my_buffer.front()->what_to_calculate);
            my_buffer.pop_front();
        }else{
            ui->extended_table->item(row, 0)->setText("");
            ui->extended_table->item(row, 1)->setText("");
            ui->extended_table->item(row, 2)->setText("");
            ui->extended_table->item(row, 3)->setText("");
        }
    }
}

void MainWindow::update_prediction_table(){
    std::list<prediction_item*> my_buffer;
    my_prediction_model->read_predictions(my_prediction_model->visible_rows, my_prediction_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " predictions";
    for(int row = 0; row < my_prediction_model->visible_rows; row++){
        if(!my_buffer.empty()){
            ui->prediction_table->item(row, 0)->setText(my_buffer.front()->prediction_id);
            ui->prediction_table->item(row, 1)->setText(my_buffer.front()->extended_id);
            ui->prediction_table->item(row, 2)->setText(my_buffer.front()->maximum_resources);
            ui->prediction_table->item(row, 3)->setText(my_buffer.front()->maximum_cost);
            ui->prediction_table->item(row, 4)->setText(my_buffer.front()->result);
            my_buffer.pop_front();
        }else{
            ui->prediction_table->item(row, 0)->setText("");
            ui->prediction_table->item(row, 1)->setText("");
            ui->prediction_table->item(row, 2)->setText("");
            ui->prediction_table->item(row, 3)->setText("");
            ui->prediction_table->item(row, 4)->setText("");
        }
    }
}

void MainWindow::update_verimap_table(){
    std::list<verimap_item*> my_buffer;
    my_verimap_model->read_verimaps(my_verimap_model->visible_rows, my_verimap_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " verification map";
    for(int row = 0; row < my_verimap_model->visible_rows; row++){
        if(!my_buffer.empty()){
            ui->verimap_table->item(row, 0)->setText(my_buffer.front()->verification_id);
            ui->verimap_table->item(row, 1)->setText(my_buffer.front()->prediction_id);
            ui->verimap_table->item(row, 2)->setText(my_buffer.front()->experiment_id);
            ui->verimap_table->item(row, 3)->setText(my_buffer.front()->comparision);
            ui->verimap_table->item(row, 4)->setText(my_buffer.front()->good_enough);
            my_buffer.pop_front();
        }else{
            ui->verimap_table->item(row, 0)->setText("");
            ui->verimap_table->item(row, 1)->setText("");
            ui->verimap_table->item(row, 2)->setText("");
            ui->verimap_table->item(row, 3)->setText("");
            ui->verimap_table->item(row, 4)->setText("");
        }
    }
}

void MainWindow::update_tagart_table(){
    std::list<tagart_item*> my_buffer;
    my_tagart_model->read_tagarts(my_tagart_model->visible_rows, my_tagart_model->offset, my_buffer);
    qDebug() << "Got " + QString::number(my_buffer.size()) + " tag-to-article mappings";
    for(int row = 0; row < my_tagart_model->visible_rows; row++){
        if(!my_buffer.empty()){
            ui->tagart_table->item(row, 0)->setText(my_buffer.front()->experiment_id);
            ui->tagart_table->item(row, 1)->setText(my_buffer.front()->tag_id);
            my_buffer.pop_front();
        }else{
            ui->tagart_table->item(row, 0)->setText("");
            ui->tagart_table->item(row, 1)->setText("");
        }
    }
}

void MainWindow::on_spell_v_scroll_valueChanged(int value)
{
    if(value < 0)
            value = 0;
    my_spell_model->offset = value;
    update_spell_table();
}

void MainWindow::on_article_v_scroll_valueChanged(int value)
{
    if(value < 0)
            value = 0;
    my_article_model->offset = value;
    update_article_table();
}


void MainWindow::on_tag_v_scroll_valueChanged(int value)
{
    if(value < 0)
            value = 0;
    my_tag_model->offset = value;
    update_tag_table();
}


void MainWindow::on_code_v_scroll_valueChanged(int value)
{
    if(value < 0)
            value = 0;
    my_code_model->offset = value;
    update_code_table();
}


void MainWindow::on_authorship_v_scroll_valueChanged(int value)
{
    if(value < 0)
            value = 0;
    my_authorship_model->offset = value;
    update_authorship_table();
}


void MainWindow::on_sdepmap_v_scroll_valueChanged(int value)
{
    if(value < 0)
            value = 0;
    my_sdepmap_model->offset = value;
    update_sdepmap_table();
}


void MainWindow::on_impmap_v_scroll_valueChanged(int value)
{
    if(value < 0)
            value = 0;
    my_impmap_model->offset = value;
    update_impmap_table();
}


void MainWindow::on_calmap_v_scroll_valueChanged(int value)
{
    if(value < 0)
            value = 0;
    my_calmap_model->offset = value;
    update_calmap_table();
}


void MainWindow::on_geometry_v_scroll_valueChanged(int value)
{
    if(value < 0)
            value = 0;
    my_geometry_model->offset = value;
    update_geometry_table();
}


void MainWindow::on_extended_v_scroll_valueChanged(int value)
{
    if(value < 0)
            value = 0;
    my_extended_model->offset = value;
    update_extended_table();
}


void MainWindow::on_prediction_v_scroll_valueChanged(int value)
{
    if(value < 0)
            value = 0;
    my_prediction_model->offset = value;
    update_prediction_table();
}


void MainWindow::on_verimap_v_scroll_valueChanged(int value)
{
    if(value < 0)
            value = 0;
    my_verimap_model->offset = value;
    update_verimap_table();
}


void MainWindow::on_tagart_v_scroll_valueChanged(int value)
{
    if(value < 0)
            value = 0;
    my_tagart_model->offset = value;
    update_tagart_table();
}


void MainWindow::on_btn_spell_create_clicked()
{
    dialog_spell_edit * my_dialog = new dialog_spell_edit(nullptr, my_spell_model, nullptr);
    my_dialog->show();
}


void MainWindow::on_spell_table_itemDoubleClicked(QTableWidgetItem *item)
{
    qDebug() << "[INFO] Editing a spell";
    //int relative_column_index = item->column();
    int relative_row_index = item->row();
    //int absolute_row_index = relative_row_index + my_spell_model->offset;
    QString spell_id = ui->spell_table->item(relative_row_index, SPELL_ID_COLUMN)->text();
    //qDebug() << "DoubleClicked r_row=" << relative_row_index << ", r_col=" << relative_column_index << ", a_row=" << absolute_row_index << ", spell_id=" << spell_id;
    spell_item * edited_spell = new spell_item();
    int retval = my_spell_model->read_spell(spell_id, edited_spell);
    if(retval == SPE5_SUCCESS){
        dialog_spell_edit * my_dialog = new dialog_spell_edit(nullptr, my_spell_model, edited_spell);
        my_dialog->show();
    }else{
        qDebug() << "[ERROR] Failed to read a spell with spell_id " << spell_id;
    }
}

void MainWindow::delete_spell(){
    //qDebug() << "Delete_spell() for " << ui->spell_table->selectedItems().at(0)->data(0).toString();
    QList<QTableWidgetSelectionRange> my_ranges = ui->spell_table->selectedRanges();
    for(int i = 0; i < my_ranges.size(); i++){
        //qDebug() << "Rows = <" << my_ranges[i].bottomRow() << " ; " << my_ranges[i].topRow() << "> with size " << my_ranges[i].rowCount();
        for(int j = 0; j <= my_ranges[i].bottomRow() - my_ranges[i].topRow(); j++){
            QString spell_id = ui->spell_table->item(my_ranges[i].topRow() + j, SPELL_ID_COLUMN)->text();
            //qDebug() << "DELETING SPELL ID: " << spell_id;
            my_spell_model->delete_spell(spell_id);
        }
    }
}

void MainWindow::on_spell_table_itemClicked(QTableWidgetItem *item)
{
    if(latex_engine.compare("on") == 0){
        qDebug() << "[INFO] Rendering a spell";
        //int relative_column_index = item->column();
        int relative_row_index = item->row();
        //int absolute_row_index = relative_row_index + my_spell_model->offset;
        QString spell_id = ui->spell_table->item(relative_row_index, SPELL_ID_COLUMN)->text();
        //qDebug() << "DoubleClicked r_row=" << relative_row_index << ", r_col=" << relative_column_index << ", a_row=" << absolute_row_index << ", spell_id=" << spell_id;
        spell_item * rendered_spell = new spell_item();
        int retval = my_spell_model->read_spell(spell_id, rendered_spell);
        if(retval == SPE5_SUCCESS){
            qDebug() << "[LATEX] Got latex data with length " << QString::number(rendered_spell->description.size());
            QString latex = rendered_spell->description;
            //QString latex = "T = \\frac{1}{2}";
            //auto render = tex::LaTeX::parse(latex.toStdWString(), 500, 20, 10, tex::BLACK);
            //QPixmap img(render->getWidth(), render->getHeight());
            QPixmap img(200,200);
            img.fill(Qt::white);
            QPainter painter(&img);
            painter.setRenderHint(QPainter::Antialiasing, true);
            //tex::Graphics2D_qt g2(&painter);
            //qDebug() << "[LATEX] Width: " << render->getWidth() << ", Height:" << render->getHeight();
            //render->draw(g2, 0, 0);
            qDebug() << "[LATEX] Draw success";
            // crop pixmap (TODO fix this properly by implementing width limit for cLaTeXMath)
            QRect rect(0, 0, LATEX_VIEW_WIDTH, LATEX_VIEW_HEIGHT);
            QPixmap cropped = img.copy(rect);
            ui->spell_latex->setPixmap(cropped);
            qDebug() << "[LATEX] SetPixmap success";

        }else{
            qDebug() << "[ERROR] Failed to read a spell with spell_id " << spell_id;
        }
    }
}
