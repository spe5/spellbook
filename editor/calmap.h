#ifndef CALMAP_H
#define CALMAP_H

#include <QObject>

struct calmap_item{
    QString ext_id;
    QString code_id;
};

class calmap_model : public QObject
{
    Q_OBJECT
public:
    explicit calmap_model(QObject *parent = nullptr);

    int count_calmaps(const QString like_pattern);
    int count_calmaps();
    int create_calmap(const calmap_item new_calmap);
    int read_calmap(QString ext_id, QString code_id, calmap_item *buffer);
    int update_calmap(const calmap_item existing_calmap);
    int delete_calmap(QString ext_id, QString code_id);
    int read_calmaps(int limit, int offset, std::list<calmap_item*> &buffer);

    int visible_rows;
    int offset;

signals:

};

#endif // CALMAP_H
