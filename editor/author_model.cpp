#include "author_model.h"
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>

author_model::author_model(QObject *parent) : QObject(parent)
{

}

int author_model::count_authors(const QString like_pattern){
    // return -1 on error, otherwise return number of spells, which match given filters
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM author;";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            int found_authors = query.value(0).toInt();
            return found_authors;
        }
        return -1;
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int author_model::count_authors(){
    // return number of all spells (all filters permissive) or -1 on error
    return count_authors("");
}

int author_model::create_author(const author_item new_author){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "INSERT INTO author VALUES ('" + new_author.author_id + "','" + new_author.first_name + "','" + new_author.second_name + "','" + new_author.billing_number + "');";
        qDebug() << sql_command;
        db.exec(sql_command);
    }
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM author WHERE author_id='" + new_author.author_id + "', first_name='" + new_author.first_name + "', second_name='" + new_author.second_name + "', billing_number='" + new_author.billing_number + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        if(query.isActive()){
            int found_records = query.value(0).toInt();
            if(found_records == 1)
                return 1; // creation succeeded

            return 0; // creation failed inside PostgreSQL
        }
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int author_model::read_author(QString author_id, author_item *buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT * FROM author WHERE author_id='" + author_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            if(query.size() != 6)
                return 0; // read failed inside PostgreSQL
            QString author_id = query.value(0).toString();
            QString first_name = query.value(1).toString();
            QString second_name = query.value(2).toString();
            QString billing_number = query.value(3).toString();
            qDebug() << "Got spell with author_id=" + author_id + ", first_name=" + first_name + ", second_name=" + second_name + ", billing_number=" + billing_number;
            buffer->author_id = author_id;
            buffer->first_name = first_name;
            buffer->second_name = second_name;
            buffer->billing_number = billing_number;
            return 1; // read succeeded
        }
    }
    return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int author_model::update_author(const author_item existing_author){
    return -1;
}

int author_model::delete_author(QString author_id){
    return -1;
}

int author_model::read_authors(int limit, int offset, std::list<author_item*> &buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(!db.isValid())
        return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.

    QString sql_command = "SELECT * FROM author LIMIT " + QString::number(limit) + " OFFSET " + QString::number(offset) + ";";
    qDebug() << sql_command;
    QSqlQuery query(sql_command, db);
    query.first();
    for(int row = 0; row < limit; row++){
        if(!query.isActive())
            return -1; // read failed inside PostgreSQL
        //if(query.size() != 6)
        //    return 0; // read failed inside PostgreSQL
        if(!query.isValid())
            return 1; // if query is not valid, then all records must be already in buffer, lets return success
        QString author_id = query.value(0).toString();
        QString first_name = query.value(1).toString();
        QString second_name = query.value(2).toString();
        QString billing_number = query.value(3).toString();
        qDebug() << "Got spell with author_id=" + author_id + ", first_name=" + first_name + ", second_name=" + second_name + ", billing_number=" + billing_number;
        author_item* another_author = new author_item();
        another_author->author_id = author_id;
        another_author->first_name = first_name;
        another_author->second_name = second_name;
        another_author->billing_number = billing_number;
        buffer.push_back(another_author);
        query.next();

    }
    return 1; // read succeeded
}
