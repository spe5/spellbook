#ifndef VERIMAP_MODEL_H
#define VERIMAP_MODEL_H

#include <QObject>

struct verimap_item{
    QString verification_id;
    QString prediction_id;
    QString experiment_id;
    QString comparision;
    QString good_enough;
};

class verimap_model : public QObject
{
    Q_OBJECT
public:
    explicit verimap_model(QObject *parent = nullptr);

    int count_verimaps(const QString like_pattern);
    int count_verimaps();
    int create_verimap(const verimap_item new_verimap);
    int read_verimap(QString verification_id, verimap_item *buffer);
    int update_verimap(const verimap_item existing_verimap);
    int delete_verimap(QString verification_id);
    int read_verimaps(int limit, int offset, std::list<verimap_item*> &buffer);

    int visible_rows;
    int offset;
signals:

};

#endif // VERIMAP_MODEL_H
