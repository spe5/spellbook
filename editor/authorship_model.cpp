#include "authorship_model.h"
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>

authorship_model::authorship_model(QObject *parent) : QObject(parent)
{

}

int authorship_model::count_authorships(const QString like_pattern){
    // return -1 on error, otherwise return number of tags, which match given filters
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM authorship;";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            int found_authorships = query.value(0).toInt();
            return found_authorships;
        }
        return -1;
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int authorship_model::count_authorships(){
    // return number of all tags (all filters permissive) or -1 on error
    return count_authorships("");
}

int authorship_model::create_authorship(const authorship_item new_authorship){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "INSERT INTO authorship VALUES ('" + new_authorship.spell_id + "','" + new_authorship.author_id + "');";
        qDebug() << sql_command;
        db.exec(sql_command);
    }
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM authorship WHERE spell_id='" + new_authorship.spell_id + "' AND author_id='" + new_authorship.author_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        if(query.isActive()){
            int found_authorships = query.value(0).toInt();
            if(found_authorships == 1)
                return 1; // creation succeeded

            return 0; // creation failed inside PostgreSQL
        }
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int authorship_model::read_authorship(QString spell_id, QString author_id, authorship_item *buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT * FROM authorship WHERE spell_id='" + spell_id + " AND author_id='" + author_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            if(query.size() != 6)
                return 0; // read failed inside PostgreSQL
            QString spell_id = query.value(0).toString();
            QString author_id = query.value(1).toString();
            qDebug() << "Got authorship with spell_id=" + spell_id + ", author_id=" + author_id;
            buffer->spell_id = spell_id;
            buffer->author_id = author_id;
            return 1; // read succeeded
        }
    }
    return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.
}


int authorship_model::update_authorship(const authorship_item existing_authorship){

}

int authorship_model::delete_authorship(QString spell_id, QString author_id){

}

int authorship_model::read_authorships(int limit, int offset, std::list<authorship_item*> &buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(!db.isValid())
        return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.

    QString sql_command = "SELECT * FROM authorship LIMIT " + QString::number(limit) + " OFFSET " + QString::number(offset) + ";";
    qDebug() << sql_command;
    QSqlQuery query(sql_command, db);
    query.first();
    for(int row = 0; row < limit; row++){
        if(!query.isActive())
            return -1; // read failed inside PostgreSQL
        //if(query.size() != 6)
        //    return 0; // read failed inside PostgreSQL
        if(!query.isValid())
            return 1; // if query is not valid, then all records must be already in buffer, lets return success
        QString spell_id = query.value(0).toString();
        QString author_id = query.value(1).toString();
        qDebug() << "Got authorship with spell_id=" + spell_id + ", author_id=" + author_id;
        authorship_item* another_authorship = new authorship_item();
        another_authorship->spell_id = spell_id;
        another_authorship->author_id = author_id;
        buffer.push_back(another_authorship);
        query.next();

    }
    return 1; // read succeeded
}
