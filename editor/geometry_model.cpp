#include "geometry_model.h"
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>

geometry_model::geometry_model(QObject *parent) : QObject(parent)
{

}

int geometry_model::count_geometries(const QString like_pattern){
    // return -1 on error, otherwise return number of tags, which match given filters
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM geometry;";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            int found_geometries = query.value(0).toInt();
            return found_geometries;
        }
        return -1;
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int geometry_model::count_geometries(){
    // return number of all tags (all filters permissive) or -1 on error
    return count_geometries("");
}

int geometry_model::create_geometry(const geometry_item new_geometry){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "INSERT INTO geometry VALUES ('" + new_geometry.geometry_id + "','" + new_geometry.filename + "');";
        qDebug() << sql_command;
        db.exec(sql_command);
    }
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM geometry WHERE geometry_id='" + new_geometry.geometry_id + "', filename='" + new_geometry.filename + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        if(query.isActive()){
            int found_geometries = query.value(0).toInt();
            if(found_geometries == 1)
                return 1; // creation succeeded

            return 0; // creation failed inside PostgreSQL
        }
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int geometry_model::read_geometry(QString geometry_id, geometry_item *buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT * FROM geometry WHERE geometry_id='" + geometry_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            if(query.size() != 6)
                return 0; // read failed inside PostgreSQL
            QString geometry_id = query.value(0).toString();
            QString filename = query.value(1).toString();
            qDebug() << "Got geometry with geometry_id=" + geometry_id + ", filename=" + filename;
            buffer->geometry_id = geometry_id;
            buffer->filename = filename;
            return 1; // read succeeded
        }
    }
    return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int geometry_model::update_geometry(const geometry_item existing_geometry){

}

int geometry_model::delete_geometry(QString geometry_id){

}

int geometry_model::read_geometries(int limit, int offset, std::list<geometry_item*> &buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(!db.isValid())
        return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.

    QString sql_command = "SELECT * FROM geometry LIMIT " + QString::number(limit) + " OFFSET " + QString::number(offset) + ";";
    qDebug() << sql_command;
    QSqlQuery query(sql_command, db);
    query.first();
    for(int row = 0; row < limit; row++){
        if(!query.isActive())
            return -1; // read failed inside PostgreSQL
        //if(query.size() != 6)
        //    return 0; // read failed inside PostgreSQL
        if(!query.isValid())
            return 1; // if query is not valid, then all records must be already in buffer, lets return success
        QString geometry_id = query.value(0).toString();
        QString filename = query.value(1).toString();
        qDebug() << "Got geometry with geometry_id=" + geometry_id + ", filename=" + filename;
        geometry_item* another_geometry = new geometry_item();
        another_geometry->geometry_id = geometry_id;
        another_geometry->filename = filename;
        buffer.push_back(another_geometry);
        query.next();
    }
    return 1; // read succeeded
}
