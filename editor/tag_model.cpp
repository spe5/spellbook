#include "tag_model.h"
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>

tag_model::tag_model(QObject *parent) : QObject(parent)
{

}

int tag_model::count_tags(const QString like_pattern){
    // return -1 on error, otherwise return number of tags, which match given filters
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM tag;";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            int found_tags = query.value(0).toInt();
            return found_tags;
        }
        return -1;
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int tag_model::count_tags(){
    // return number of all tags (all filters permissive) or -1 on error
    return count_tags("");
}

int tag_model::create_tag(const tag_item new_tag){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "INSERT INTO tag VALUES ('" + new_tag.tag_id + "','" + new_tag.tag_name + "');";
        qDebug() << sql_command;
        db.exec(sql_command);
    }
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM tag WHERE tag_id='" + new_tag.tag_id + "', tag_name='" + new_tag.tag_name + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        if(query.isActive()){
            int found_tags = query.value(0).toInt();
            if(found_tags == 1)
                return 1; // creation succeeded

            return 0; // creation failed inside PostgreSQL
        }
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int tag_model::read_tag(QString tag_id, tag_item *buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT * FROM tag WHERE tag_id='" + tag_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            if(query.size() != 6)
                return 0; // read failed inside PostgreSQL
            QString tag_id = query.value(0).toString();
            QString tag_name = query.value(1).toString();
            qDebug() << "Got tag with tag_id=" + tag_id + ", tag_name=" + tag_name;
            buffer->tag_id = tag_id;
            buffer->tag_name = tag_name;
            return 1; // read succeeded
        }
    }
    return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int tag_model::update_tag(const tag_item existing_tag){

}

int tag_model::delete_tag(QString tag_id){

}

int tag_model::read_tags(int limit, int offset, std::list<tag_item*> &buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(!db.isValid())
        return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.

    QString sql_command = "SELECT * FROM tag LIMIT " + QString::number(limit) + " OFFSET " + QString::number(offset) + ";";
    qDebug() << sql_command;
    QSqlQuery query(sql_command, db);
    query.first();
    for(int row = 0; row < limit; row++){
        if(!query.isActive())
            return -1; // read failed inside PostgreSQL
        //if(query.size() != 6)
        //    return 0; // read failed inside PostgreSQL
        if(!query.isValid())
            return 1; // if query is not valid, then all records must be already in buffer, lets return success
        QString tag_id = query.value(0).toString();
        QString tag_name = query.value(1).toString();
        qDebug() << "Got tag with tag_id=" + tag_id + ", tag_name=" + tag_name;
        tag_item* another_tag = new tag_item();
        another_tag->tag_id = tag_id;
        another_tag->tag_name = tag_name;
        buffer.push_back(another_tag);
        query.next();

    }
    return 1; // read succeeded
}
