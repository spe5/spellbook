#include "code_model.h"
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>

code_model::code_model(QObject *parent) : QObject(parent)
{

}

int code_model::count_codes(const QString like_pattern){
    // return -1 on error, otherwise return number of tags, which match given filters
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM code;";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            int found_codes = query.value(0).toInt();
            return found_codes;
        }
        return -1;
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int code_model::count_codes(){
    // return number of all tags (all filters permissive) or -1 on error
    return count_codes("");
}

int code_model::create_code(const code_item new_code){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "INSERT INTO code VALUES ('" + new_code.code_id + "','" + new_code.filename + "','" + new_code.use_cost + "','" + new_code.use_cost_currency + "','" + new_code.accuracy + "','" + new_code.complexity + "');";
        qDebug() << sql_command;
        db.exec(sql_command);
    }
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM code WHERE code_id='" + new_code.code_id + "', filename='" + new_code.filename + "', use_cost='" + new_code.use_cost + "', use_cost_currency='" + new_code.use_cost_currency + "', accuracy='" + new_code.accuracy + "', complexity='" + new_code.complexity + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        if(query.isActive()){
            int found_code = query.value(0).toInt();
            if(found_code == 1)
                return 1; // creation succeeded

            return 0; // creation failed inside PostgreSQL
        }
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}


int code_model::read_code(QString code_id, code_item *buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT * FROM code WHERE code_id='" + code_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            if(query.size() != 6)
                return 0; // read failed inside PostgreSQL
            QString code_id = query.value(0).toString();
            QString filename = query.value(1).toString();
            QString use_cost = query.value(2).toString();
            QString use_cost_currency = query.value(3).toString();
            QString accuracy = query.value(4).toString();
            QString complexity = query.value(5).toString();
            qDebug() << "Got code with code_id=" + code_id + ", code_filename=" + filename + ", use_cost=" + use_cost + ", use_cost_currency=" + use_cost_currency + ", accuracy=" + accuracy + ", complexity=" + complexity;
            buffer->code_id = code_id;
            buffer->filename = filename;
            buffer->use_cost = use_cost;
            buffer->use_cost_currency = use_cost_currency;
            buffer->accuracy = accuracy;
            buffer->complexity = complexity;
            return 1; // read succeeded
        }
    }
    return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int code_model::update_code(const code_item existing_code){

}

int code_model::delete_code(QString code_id){

}

int code_model::read_codes(int limit, int offset, std::list<code_item*> &buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(!db.isValid())
        return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.

    QString sql_command = "SELECT * FROM code LIMIT " + QString::number(limit) + " OFFSET " + QString::number(offset) + ";";
    qDebug() << sql_command;
    QSqlQuery query(sql_command, db);
    query.first();
    for(int row = 0; row < limit; row++){
        if(!query.isActive())
            return -1; // read failed inside PostgreSQL
        //if(query.size() != 6)
        //    return 0; // read failed inside PostgreSQL
        if(!query.isValid())
            return 1; // if query is not valid, then all records must be already in buffer, lets return success
        QString code_id = query.value(0).toString();
        QString filename = query.value(1).toString();
        QString use_cost = query.value(2).toString();
        QString use_cost_currency = query.value(3).toString();
        QString accuracy = query.value(4).toString();
        QString complexity = query.value(5).toString();
        qDebug() << "Got code with code_id=" + code_id + ", code_filename=" + filename + ", use_cost=" + use_cost + ", use_cost_currency=" + use_cost_currency + ", accuracy=" + accuracy + ", complexity=" + complexity;
        code_item* another_code = new code_item();
        another_code->code_id = code_id;
        another_code->filename = filename;
        another_code->use_cost = use_cost;
        another_code->use_cost_currency = use_cost_currency;
        another_code->accuracy = accuracy;
        another_code->complexity = complexity;
        buffer.push_back(another_code);
        query.next();

    }
    return 1; // read succeeded
}
