#ifndef PREDICTION_MODEL_H
#define PREDICTION_MODEL_H

#include <QObject>

struct prediction_item{
    QString prediction_id;
    QString extended_id;
    QString maximum_resources;
    QString maximum_cost;
    QString result;
};

class prediction_model : public QObject
{
    Q_OBJECT
public:
    explicit prediction_model(QObject *parent = nullptr);

    int count_predictions(const QString like_pattern);
    int count_predictions();
    int create_prediction(const prediction_item new_prediction);
    int read_prediction(QString prediction_id, prediction_item *buffer);
    int update_prediction(const prediction_item existing_prediction);
    int delete_prediction(QString prediction_id);
    int read_predictions(int limit, int offset, std::list<prediction_item*> &buffer);

    int visible_rows;
    int offset;

signals:

};

#endif // PREDICTION_MODEL_H
