#include "extended_model.h"
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>

extended_model::extended_model(QObject *parent) : QObject(parent)
{

}

int extended_model::count_extendeds(const QString like_pattern){
    // return -1 on error, otherwise return number of tags, which match given filters
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM extended;";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            int found_extendeds = query.value(0).toInt();
            return found_extendeds;
        }
        return -1;
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int extended_model::count_extendeds(){
    // return number of all tags (all filters permissive) or -1 on error
    return count_extendeds("");
}

int extended_model::create_extended(const extended_item new_extended){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "INSERT INTO extended VALUES ('" + new_extended.extended_id + "','" + new_extended.geometry_id + "','" + new_extended.code_preconditions + "','" + new_extended.what_to_calculate + "');";
        qDebug() << sql_command;
        db.exec(sql_command);
    }
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM extended WHERE extended_id='" + new_extended.extended_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        if(query.isActive()){
            int found_geometries = query.value(0).toInt();
            if(found_geometries == 1)
                return 1; // creation succeeded

            return 0; // creation failed inside PostgreSQL
        }
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int extended_model::read_extended(QString extended_id, extended_item *buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT * FROM extended WHERE extended_id='" + extended_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            if(query.size() != 4)
                return 0; // read failed inside PostgreSQL
            QString extended_id = query.value(0).toString();
            QString geometry_id = query.value(1).toString();
            QString code_preconditions = query.value(2).toString();
            QString what_to_calculate = query.value(3).toString();
            qDebug() << "Got extended with extended_id=" + extended_id + ", geometry_id=" + geometry_id + ", code_preconditions='" + code_preconditions + ", what_to_calculate" + what_to_calculate;
            buffer->extended_id = extended_id;
            buffer->geometry_id = geometry_id;
            buffer->code_preconditions = code_preconditions;
            buffer->what_to_calculate = what_to_calculate;
            return 1; // read succeeded
        }
    }
    return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int extended_model::update_extended(const extended_item existing_extended){

}

int extended_model::delete_extended(QString extended_id){

}


int extended_model::read_extendeds(int limit, int offset, std::list<extended_item*> &buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(!db.isValid())
        return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.

    QString sql_command = "SELECT * FROM extended LIMIT " + QString::number(limit) + " OFFSET " + QString::number(offset) + ";";
    qDebug() << sql_command;
    QSqlQuery query(sql_command, db);
    query.first();
    for(int row = 0; row < limit; row++){
        if(!query.isActive())
            return -1; // read failed inside PostgreSQL
        //if(query.size() != 6)
        //    return 0; // read failed inside PostgreSQL
        if(!query.isValid())
            return 1; // if query is not valid, then all records must be already in buffer, lets return success
        QString extended_id = query.value(0).toString();
        QString geometry_id = query.value(1).toString();
        QString code_preconditions = query.value(2).toString();
        QString what_to_calculate = query.value(3).toString();
        qDebug() << "Got extended with extended_id=" + extended_id + ", geometry_id=" + geometry_id + ", code_preconditions=" + code_preconditions + ", what_to_calculate=" + what_to_calculate;
        extended_item* another_extended = new extended_item();
        another_extended->extended_id = extended_id;
        another_extended->geometry_id = geometry_id;
        another_extended->code_preconditions = code_preconditions;
        another_extended->what_to_calculate = what_to_calculate;
        buffer.push_back(another_extended);
        query.next();
    }
    return 1; // read succeeded
}
