#ifndef CODE_MODEL_H
#define CODE_MODEL_H

#include <QObject>

struct code_item{
    QString code_id;
    QString filename;
    QString use_cost;
    QString use_cost_currency;
    QString accuracy;
    QString complexity;
};

class code_model : public QObject
{
    Q_OBJECT
public:
    explicit code_model(QObject *parent = nullptr);

    int count_codes(const QString like_pattern);
    int count_codes();
    int create_code(const code_item new_code);
    int read_code(QString code_id, code_item *buffer);
    int update_code(const code_item existing_code);
    int delete_code(QString code_id);
    int read_codes(int limit, int offset, std::list<code_item*> &buffer);

    int visible_rows;
    int offset;


signals:

};

#endif // CODE_MODEL_H
