#include "sdepmap_model.h"
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>

sdepmap_model::sdepmap_model(QObject *parent) : QObject(parent)
{

}

int sdepmap_model::count_sdepmaps(const QString like_pattern){
    // return -1 on error, otherwise return number of tags, which match given filters
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM sdepmap;";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            int found_sdepmaps = query.value(0).toInt();
            return found_sdepmaps;
        }
        return -1;
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int sdepmap_model::count_sdepmaps(){
    // return number of all tags (all filters permissive) or -1 on error
    return count_sdepmaps("");
}

int sdepmap_model::create_sdepmap(const sdepmap_item new_sdepmap){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "INSERT INTO sdepmap VALUES ('" + new_sdepmap.primary_spell_id + "','" + new_sdepmap.secondary_spell_id + "');";
        qDebug() << sql_command;
        db.exec(sql_command);
    }
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM sdepmap WHERE primary_spell_id='" + new_sdepmap.primary_spell_id + "', secondary_spell_id='" + new_sdepmap.secondary_spell_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        if(query.isActive()){
            int found_items = query.value(0).toInt();
            if(found_items == 1)
                return 1; // creation succeeded

            return 0; // creation failed inside PostgreSQL
        }
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}


int sdepmap_model::read_sdepmap(QString primary_spell_id, QString secondary_spell_id, sdepmap_item *buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT * FROM sdepmap WHERE primary_spell_id='" + primary_spell_id + " AND secondary_spell_id='" + secondary_spell_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            if(query.size() != 6)
                return 0; // read failed inside PostgreSQL
            QString primary_spell_id = query.value(0).toString();
            QString secondary_spell_id = query.value(1).toString();
            int relation = query.value(2).toInt();
            qDebug() << "Got sdepmap with primary_spell_id=" + primary_spell_id + ", secondary_spell_id=" + secondary_spell_id + ", relation=" + QString::number(relation);
            buffer->primary_spell_id = primary_spell_id;
            buffer->secondary_spell_id = secondary_spell_id;
            buffer->relation = relation;
            return 1; // read succeeded
        }
    }
    return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int sdepmap_model::update_sdepmap(const sdepmap_item existing_sdepmap){

}

int sdepmap_model::delete_sdepmap(QString primary_spell_id, QString secondary_spell_id){

}

int sdepmap_model::read_sdepmaps(int limit, int offset, std::list<sdepmap_item*> &buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(!db.isValid())
        return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.

    QString sql_command = "SELECT * FROM sdepmap LIMIT " + QString::number(limit) + " OFFSET " + QString::number(offset) + ";";
    qDebug() << sql_command;
    QSqlQuery query(sql_command, db);
    query.first();
    for(int row = 0; row < limit; row++){
        if(!query.isActive())
            return -1; // read failed inside PostgreSQL
        //if(query.size() != 6)
        //    return 0; // read failed inside PostgreSQL
        if(!query.isValid())
            return 1; // if query is not valid, then all records must be already in buffer, lets return success
        QString primary_spell_id = query.value(0).toString();
        QString secondary_spell_id = query.value(1).toString();
        int relation = query.value(2).toInt();
        qDebug() << "Got sdepmap with primary_spell_id=" + primary_spell_id + ", secondary_spell_id=" + secondary_spell_id + ", relation=" + QString::number(relation);
        sdepmap_item* another_sdepmap = new sdepmap_item();
        another_sdepmap->primary_spell_id = primary_spell_id;
        another_sdepmap->secondary_spell_id = secondary_spell_id;
        another_sdepmap->relation = relation;
        buffer.push_back(another_sdepmap);
        query.next();

    }
    return 1; // read succeeded
}

