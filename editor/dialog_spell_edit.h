#ifndef DIALOG_SPELL_EDIT_H
#define DIALOG_SPELL_EDIT_H

#include <QDialog>
#include "spell_model.h"

namespace Ui {
class dialog_spell_edit;
}

class dialog_spell_edit : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_spell_edit(QWidget *parent = nullptr, spell_model * the_spell_model = nullptr, spell_item * the_spell_item = nullptr);
    ~dialog_spell_edit();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::dialog_spell_edit *ui;
    spell_model * my_spell_model;
    bool create_new_spell;
};

#endif // DIALOG_SPELL_EDIT_H
