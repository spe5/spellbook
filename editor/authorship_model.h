#ifndef AUTHORSHIP_MODEL_H
#define AUTHORSHIP_MODEL_H

#include <QObject>

struct authorship_item{
    QString spell_id;
    QString author_id;
};

class authorship_model : public QObject
{
    Q_OBJECT
public:
    explicit authorship_model(QObject *parent = nullptr);

    int count_authorships(const QString like_pattern);
    int count_authorships();
    int create_authorship(const authorship_item new_authorship);
    int read_authorship(QString spell_id, QString author_id, authorship_item *buffer);
    int update_authorship(const authorship_item existing_authorship);
    int delete_authorship(QString spell_id, QString author_id);
    int read_authorships(int limit, int offset, std::list<authorship_item*> &buffer);

    int visible_rows;
    int offset;

signals:

};

#endif // AUTHORSHIP_MODEL_H
