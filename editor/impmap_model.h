#ifndef IMPMAP_MODEL_H
#define IMPMAP_MODEL_H

#include <QObject>

struct impmap_item{
    QString spell_id;
    QString code_id;
};


class impmap_model : public QObject
{
    Q_OBJECT
public:
    explicit impmap_model(QObject *parent = nullptr);

    int count_impmaps(const QString like_pattern);
    int count_impmaps();
    int create_impmap(const impmap_item new_item);
    int read_impmap(QString spell_id, QString code_id, impmap_item *buffer);
    int update_impmap(const impmap_item existing_item);
    int delete_impmap(QString spell_id, QString code_id);
    int read_impmaps(int limit, int offset, std::list<impmap_item*> &buffer);

    int visible_rows;
    int offset;

signals:

};

#endif // IMPMAP_MODEL_H
