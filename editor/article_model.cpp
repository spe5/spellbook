#include "article_model.h"
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>

article_model::article_model(QObject *parent) : QObject(parent)
{

}

int article_model::count_articles(const bool articles, const bool books, const bool experiments, const QString like_pattern){
    // return -1 on error, otherwise return number of articles, which match given filters
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM experiment;";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            int found_spells = query.value(0).toInt();
            return found_spells;
        }
        return -1;
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int article_model::count_articles(){
    // return number of all articles (all filters permissive) or -1 on error
    return count_articles(true, true, true, "");
}

int article_model::create_article(const article_item new_article){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "INSERT INTO experiment VALUES ('" + new_article.article_id + "','" + new_article.name + "','" + new_article.author + "','" + new_article.license + "','" + new_article.filename + "','" + new_article.cost + "'," + QString::number(new_article.is_book) + "," + QString::number(new_article.has_experiment) + ");";
        qDebug() << sql_command;
        db.exec(sql_command);
    }
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM experiment WHERE experiment_id='" + new_article.article_id + "', name='" + new_article.name + "', author='" + new_article.author + "', license='" + new_article.license + "', filename='" + new_article.filename + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        if(query.isActive()){
            int found_records = query.value(0).toInt();
            if(found_records == 1)
                return 1; // creation succeeded

            return 0; // creation failed inside PostgreSQL
        }
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int article_model::read_article(const QString article_id, article_item *buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT * FROM experiment WHERE experiment_id='" + article_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            if(query.size() != 8)
                return 0; // read failed inside PostgreSQL
            QString article_id = query.value(0).toString();
            QString name = query.value(1).toString();
            QString author = query.value(2).toString();
            QString license = query.value(3).toString();
            QString filename = query.value(4).toString();
            QString cost = query.value(5).toString();
            int is_book = query.value(6).toInt();
            int has_experiment = query.value(7).toInt();
            qDebug() << "Got article with article_id=" + article_id + ", name=" + name + ", author=" + author + ", license=" + license + ", cost=" + cost + ", is_book=" + QString::number(is_book) + ", has_exp=" + QString::number(has_experiment);
            buffer->article_id = article_id;
            buffer->name = name;
            buffer->author = author;
            buffer->license = license;
            buffer->filename = filename;
            buffer->cost = cost;
            buffer->is_book = is_book;
            buffer->has_experiment = has_experiment;
            return 1; // read succeeded
        }
    }
    return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int article_model::update_article(const article_item existing_spell){

}

int article_model::delete_article(QString article_id){

}

int article_model::read_articles(int limit, int offset, std::list<article_item*> &buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(!db.isValid())
        return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.

    QString sql_command = "SELECT * FROM experiment LIMIT " + QString::number(limit) + " OFFSET " + QString::number(offset) + ";";
    qDebug() << sql_command;
    QSqlQuery query(sql_command, db);
    query.first();
    for(int row = 0; row < limit; row++){
        if(!query.isActive())
            return -1; // read failed inside PostgreSQL
        //if(query.size() != 6)
        //    return 0; // read failed inside PostgreSQL

        QString article_id = query.value(0).toString();
        QString name = query.value(1).toString();
        QString author = query.value(2).toString();
        QString license = query.value(3).toString();
        QString filename = query.value(4).toString();
        QString cost = query.value(5).toString();
        int is_book = query.value(6).toInt();
        int has_experiment = query.value(7).toInt();
        qDebug() << "Got spell with article_id=" + article_id + ", name=" + name + ", author=" + author + ", license=" + license + ", filename=" + filename + ", cost=" + cost + ", is_book=" + QString::number(is_book) + ", has_experiment=" + QString::number(has_experiment);
        article_item* another_article = new article_item();
        another_article->article_id = article_id;
        another_article->name = name;
        another_article->author = author;
        another_article->license = license;
        another_article->filename = filename;
        another_article->cost = cost;
        another_article->is_book = is_book;
        another_article->has_experiment = has_experiment;
        buffer.push_back(another_article);

        query.next();

    }
    return 1; // read succeeded
}



