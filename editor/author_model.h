#ifndef AUTHOR_MODEL_H
#define AUTHOR_MODEL_H

#include <QObject>

struct author_item{
    QString author_id;
    QString first_name;
    QString second_name;
    QString billing_number;
};

class author_model : public QObject
{
    Q_OBJECT
public:
    explicit author_model(QObject *parent = nullptr);

    int count_authors(const QString like_pattern);
    int count_authors();
    int create_author(const author_item new_author);
    int read_author(QString author_id, author_item *buffer);
    int update_author(const author_item existing_author);
    int delete_author(QString author_id);
    int read_authors(int limit, int offset, std::list<author_item*> &buffer);

    int visible_rows;
    int offset;

signals:

};

#endif // AUTHOR_MODEL_H
