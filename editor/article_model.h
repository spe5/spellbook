#ifndef ARTICLE_MODEL_H
#define ARTICLE_MODEL_H

#include <QObject>

struct article_item{
    QString article_id;
    QString name;
    QString author;
    QString license;
    QString filename;
    QString cost;
    int is_book;
    int has_experiment;
};

class article_model : public QObject
{
    Q_OBJECT
public:
    explicit article_model(QObject *parent = nullptr);

    int count_articles(const bool articles, const bool books, const bool experiments, const QString like_pattern);
    int count_articles();
    int create_article(const article_item new_article);
    int read_article(QString article_id, article_item *buffer);
    int update_article(const article_item existing_spell);
    int delete_article(QString article_id);
    int read_articles(int limit, int offset, std::list<article_item*> &buffer);

    int visible_rows;
    int offset;

signals:

};

#endif // ARTICLE_MODEL_H
