#include "tagart_model.h"
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>

tagart_model::tagart_model(QObject *parent) : QObject(parent)
{

}

int tagart_model::count_tagarts(const QString like_pattern){
    // return -1 on error, otherwise return number of tags, which match given filters
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM tag_article;";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            int found_tagarts = query.value(0).toInt();
            return found_tagarts;
        }
        return -1;
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int tagart_model::count_tagarts(){
    // return number of all tags (all filters permissive) or -1 on error
    return count_tagarts("");
}

int tagart_model::create_tagart(const tagart_item new_tagart){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "INSERT INTO tag_article VALUES ('" + new_tagart.experiment_id + "','" + new_tagart.tag_id + "');";
        qDebug() << sql_command;
        db.exec(sql_command);
    }
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM tag_article WHERE experiment_id='" + new_tagart.experiment_id + "', tag_id='" + new_tagart.tag_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        if(query.isActive()){
            int found_tagarts = query.value(0).toInt();
            if(found_tagarts == 1)
                return 1; // creation succeeded

            return 0; // creation failed inside PostgreSQL
        }
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int tagart_model::read_tagart(QString experiment_id, QString tag_id, tagart_item *buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT * FROM tag_article WHERE experiment_id='" + experiment_id + "' AND tag_id='" + tag_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            if(query.size() != 6)
                return 0; // read failed inside PostgreSQL
            QString experiment_id = query.value(0).toString();
            QString tag_id = query.value(1).toString();
            qDebug() << "Got tagart with experiment_id=" + experiment_id + ", tag_id=" + tag_id;
            buffer->experiment_id = experiment_id;
            buffer->tag_id = tag_id;
            return 1; // read succeeded
        }
    }
    return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int tagart_model::update_tagart(const tagart_item existing_tagart){

}

int tagart_model::delete_tagart(QString experiment_id, QString tag_id){

}

int tagart_model::read_tagarts(int limit, int offset, std::list<tagart_item*> &buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(!db.isValid())
        return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.

    QString sql_command = "SELECT * FROM tag_article LIMIT " + QString::number(limit) + " OFFSET " + QString::number(offset) + ";";
    qDebug() << sql_command;
    QSqlQuery query(sql_command, db);
    query.first();
    for(int row = 0; row < limit; row++){
        if(!query.isActive())
            return -1; // read failed inside PostgreSQL
        //if(query.size() != 6)
        //    return 0; // read failed inside PostgreSQL
        if(!query.isValid())
            return 1; // if query is not valid, then all records must be already in buffer, lets return success
        QString experiment_id = query.value(0).toString();
        QString tag_id = query.value(1).toString();
        qDebug() << "Got tag_article with experiment_id=" + experiment_id + ", tag_id=" + tag_id;
        tagart_item* another_tagart = new tagart_item();
        another_tagart->experiment_id = experiment_id;
        another_tagart->tag_id = tag_id;
        buffer.push_back(another_tagart);
        query.next();

    }
    return 1; // read succeeded
}


