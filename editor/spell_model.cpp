#include "spell_model.h"
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>
#include <unistd.h>
#include <QByteArray>

spell_model::spell_model(QObject *parent) : QObject(parent)
{

}

int spell_model::count_spells(const bool definitions, const bool theorems, const bool proofs, const QString like_pattern){
    // return -1 on error, otherwise return number of spells, which match given filters
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM spell;";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            int found_spells = query.value(0).toInt();
            return found_spells;
        }
        return -1;
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int spell_model::count_spells(){
    // return number of all spells (all filters permissive) or -1 on error
    return count_spells(true, true, true, "");
}

int spell_model::create_spell(const spell_item new_spell){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    QString encoded_data = base64_encode(new_spell.description);
    if(db.isValid()){
        QString sql_command = "INSERT INTO spell VALUES ('" + new_spell.spell_id + "'," + QString::number(new_spell.spell_type) + ",'" + new_spell.name + "','" + encoded_data + "','" + new_spell.use_cost + "','" + new_spell.use_cost_currency + "');";
        qDebug() << sql_command;
        db.exec(sql_command);
    }
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM spell WHERE spell_id='" + new_spell.spell_id + "' AND spell_type=" + QString::number(new_spell.spell_type) + " AND description='" + encoded_data + "' AND use_cost='" + new_spell.use_cost + "' AND use_cost_currency='" + new_spell.use_cost_currency + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            int found_records = query.value(0).toInt();
            if(found_records == 1){
                qDebug() << "EMIT update_spell_table()";
                emit update_spell_table();
                return 1; // creation succeeded
            }

            return 0; // creation failed inside PostgreSQL
        }
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int spell_model::read_spell(QString spell_id, spell_item *buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT * FROM spell WHERE spell_id='" + spell_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            qDebug() << "Got " << query.size() << " items";
            if(query.size() != 1)
                return 0; // read failed inside PostgreSQL
            QString spell_id = query.value(0).toString();
            int spell_type = query.value(1).toInt();
            QString name = query.value(2).toString();
            QString encoded_data = query.value(3).toString();
            QString description = base64_decode(encoded_data);
            QString use_cost = query.value(4).toString();
            QString use_cost_currency = query.value(5).toString();
            qDebug() << "Got spell with spell_id=" + spell_id + ", spell_type=" + QString::number(spell_type) + ", name=" + name + ", len(description)=" + QString::number(description.size()) + ", use_cost=" + use_cost + ", currency=" + use_cost_currency;
            buffer->spell_id = spell_id;
            buffer->spell_type = spell_type;
            buffer->name = name;
            buffer->description = description;
            buffer->use_cost = use_cost;
            buffer->use_cost_currency = use_cost_currency;
            return 1; // read succeeded

        }
    }
    return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int spell_model::update_spell(const spell_item existing_spell){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    QString encoded_data = base64_encode(existing_spell.description);
    if(db.isValid()){
        QString sql_command = "UPDATE spell SET spell_type=" + QString::number(existing_spell.spell_type) + ", name='" + existing_spell.name + "', description='" + encoded_data + "', use_cost='" + existing_spell.use_cost + "', use_cost_currency='" + existing_spell.use_cost_currency + "' WHERE spell_id='" + existing_spell.spell_id + "';";
        qDebug() << sql_command;
        db.exec(sql_command);
    }
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM spell WHERE spell_id='" + existing_spell.spell_id + "' AND spell_type=" + QString::number(existing_spell.spell_type) + " AND name='" + existing_spell.name + "' AND description='" + encoded_data + "' AND use_cost='" + existing_spell.use_cost + "' AND use_cost_currency='" + existing_spell.use_cost_currency + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            int found_records = query.value(0).toInt();
            if(found_records == 1){
                qDebug() << "EMIT update_spell_table()";
                emit update_spell_table();
                return 1; // creation succeeded
            }
            return 0; // creation failed inside PostgreSQL
        }
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int spell_model::delete_spell(QString spell_id){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "DELETE FROM spell WHERE spell_id='" + spell_id + "';";
        qDebug() << sql_command;
        db.exec(sql_command);
    }
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM spell WHERE spell_id='" + spell_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            int found_records = query.value(0).toInt();
            if(found_records == 0){
                qDebug() << "EMIT update_spell_table()";
                emit update_spell_table();
                return 1; // creation succeeded
            }
            return 0; // creation failed inside PostgreSQL
        }
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int spell_model::read_spells(int limit, int offset, std::list<spell_item*> &buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(!db.isValid())
        return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.

    QString sql_command = "SELECT * FROM spell LIMIT " + QString::number(limit) + " OFFSET " + QString::number(offset) + ";";
    qDebug() << sql_command;
    QSqlQuery query(sql_command, db);
    query.first();
    for(int row = 0; row < limit; row++){
        if(!query.isActive())
            return -1; // read failed inside PostgreSQL
        //if(query.size() != 6)
        //    return 0; // read failed inside PostgreSQL

        QString spell_id = query.value(0).toString();
        int spell_type = query.value(1).toInt();
        QString name = query.value(2).toString();
        QString encoded_data = query.value(3).toString();
        QString description = base64_decode(encoded_data);
        QString use_cost = query.value(4).toString();
        QString use_cost_currency = query.value(5).toString();
        qDebug() << "Got spell with spell_id=" + spell_id + ", spell_type=" + QString::number(spell_type) + ", name=" + name + ", len(description)=" + QString::number(description.size()) + ", use_cost=" + use_cost + ", currency=" + use_cost_currency;
        spell_item* another_spell = new spell_item();
        another_spell->spell_id = spell_id;
        another_spell->spell_type = spell_type;
        another_spell->name = name;
        another_spell->description = description;
        another_spell->use_cost = use_cost;
        another_spell->use_cost_currency = use_cost_currency;
        buffer.push_back(another_spell);

        query.next();

    }
    return 1; // read succeeded
}

int spell_model::read_spells_2021_10_10(int limit, int offset, std::list<spell_item*> &buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(!db.isValid())
        return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.

    QString sql_command = "SELECT * FROM spell LIMIT " + QString::number(limit) + " OFFSET " + QString::number(offset) + ";";
    qDebug() << sql_command;
    QSqlQuery query(sql_command, db);
    query.first();
    for(int row = 0; row < limit; row++){
        if(!query.isActive())
            return -1; // read failed inside PostgreSQL
        //if(query.size() != 6)
        //    return 0; // read failed inside PostgreSQL

        QString spell_id = query.value(0).toString();
        int spell_type = query.value(1).toInt();
        QString name = query.value(2).toString();
        QString description = query.value(3).toString();
        QString use_cost = query.value(4).toString();
        QString use_cost_currency = query.value(5).toString();
        qDebug() << "Got spell with spell_id=" + spell_id + ", spell_type=" + QString::number(spell_type) + ", name=" + name + ", len(description)=" + QString::number(description.size()) + ", use_cost=" + use_cost + ", currency=" + use_cost_currency;
        spell_item* another_spell = new spell_item();
        another_spell->spell_id = spell_id;
        another_spell->spell_type = spell_type;
        another_spell->name = name;
        another_spell->description = description;
        another_spell->use_cost = use_cost;
        another_spell->use_cost_currency = use_cost_currency;
        buffer.push_back(another_spell);

        query.next();

    }
    return 1; // read succeeded
}

void spell_model::generate_random_id(QString &buffer)
{
    const int HASH_LENGTH = 32;
    std::string new_id;
    static const char alphanum[] =
            "0123456789"
            "abcdefghijklmnopqrstuvwxyz";

    srand( (unsigned) time(NULL) * getpid());

    new_id.reserve(HASH_LENGTH);

    for (int i = 0; i < HASH_LENGTH; ++i)
            new_id += alphanum[rand() % (sizeof(alphanum) - 1)];

    buffer = QString(new_id.c_str());

}

spell_type spell_model::int_to_spell_type(const int spell_type){
    if(spell_type == 0)
        return spell_type::UNKNOWN;
    if(spell_type == 1)
        return spell_type::DEFINITION;
    if(spell_type == 2)
        return spell_type::THEOREM;
    if(spell_type == 3)
        return spell_type::PROOF;
    return spell_type::UNKNOWN;
}

int spell_model::spell_type_to_int(const spell_type spell_type){
    if(spell_type == spell_type::UNSET)
        return 0;
    if(spell_type == spell_type::DEFINITION)
        return 1;
    if(spell_type == spell_type::THEOREM)
        return 2;
    if(spell_type == spell_type::PROOF)
        return 3;
    return 255;
}

spell_type spell_model::string_to_spell_type(const std::string spell_type){
    if(spell_type.compare("Unset") == 0)
        return spell_type::UNSET;
    if(spell_type.compare("Definition") == 0)
        return spell_type::DEFINITION;
    if(spell_type.compare("Theorem") == 0)
        return spell_type::THEOREM;
    if(spell_type.compare("Proof") == 0)
        return spell_type::PROOF;
    if(spell_type.compare("Unknown") == 0)
        return spell_type::UNKNOWN;
    return spell_type::UNKNOWN;
}

std::string spell_model::spell_type_to_string(const spell_type spell_type){
    if(spell_type == spell_type::UNSET)
        return "Unset";
    if(spell_type == spell_type::DEFINITION)
        return "Definition";
    if(spell_type == spell_type::THEOREM)
        return "Theorem";
    if(spell_type == spell_type::PROOF)
        return "Proof";
    if(spell_type == spell_type::UNKNOWN)
        return "Unknown";
    return "Unknown";
}

QString spell_model::base64_encode(QString string){
    QByteArray ba;
    ba.append(string.toUtf8());
    return ba.toBase64();
}

QString spell_model::base64_decode(QString string){
    QByteArray ba;
    QByteArray ba2 = ba.fromBase64(string.toUtf8(), QByteArray::Base64Encoding);
    return ba2;
}

int spell_model::migration_2021_10_10(){
    // return 1 on success, return -1 or 0 if migration failed
    int total_spells = count_spells();
    std::list<spell_item*> buffer;
    read_spells_2021_10_10(total_spells,0,buffer); // get all spells
    int retval = 1;
    int counter = 0;
    while(!buffer.empty()){
        qDebug() << "[MIGRATION] Converting " << QString::number(counter) << " " << buffer.front()->name;
        int retval2 = update_spell(*(buffer.front()));
        if(retval2 != 1){
            retval = -1;
        }
        buffer.pop_front();
        counter++;
    }

    return retval;
}
