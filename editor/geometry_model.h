#ifndef GEOMETRY_MODEL_H
#define GEOMETRY_MODEL_H

#include <QObject>

struct geometry_item{
    QString geometry_id;
    QString filename;
};

class geometry_model : public QObject
{
    Q_OBJECT
public:
    explicit geometry_model(QObject *parent = nullptr);

    int count_geometries(const QString like_pattern);
    int count_geometries();
    int create_geometry(const geometry_item new_geometry);
    int read_geometry(QString geometry_id, geometry_item *buffer);
    int update_geometry(const geometry_item existing_geometry);
    int delete_geometry(QString geometry_id);
    int read_geometries(int limit, int offset, std::list<geometry_item*> &buffer);

    int visible_rows;
    int offset;

signals:

};

#endif // GEOMETRY_MODEL_H
