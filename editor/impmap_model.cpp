#include "impmap_model.h"
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>

impmap_model::impmap_model(QObject *parent) : QObject(parent)
{

}

int impmap_model::count_impmaps(const QString like_pattern){
    // return -1 on error, otherwise return number of tags, which match given filters
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM impmap;";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            int found_items = query.value(0).toInt();
            return found_items;
        }
        return -1;
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int impmap_model::count_impmaps(){
    // return number of all tags (all filters permissive) or -1 on error
    return count_impmaps("");
}

int impmap_model::create_impmap(const impmap_item new_item){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "INSERT INTO impmap VALUES ('" + new_item.spell_id + "','" + new_item.code_id + "');";
        qDebug() << sql_command;
        db.exec(sql_command);
    }
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM impmap WHERE spell_id='" + new_item.spell_id + "', code_id='" + new_item.code_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        if(query.isActive()){
            int found_items = query.value(0).toInt();
            if(found_items == 1)
                return 1; // creation succeeded

            return 0; // creation failed inside PostgreSQL
        }
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int impmap_model::read_impmap(QString spell_id, QString code_id, impmap_item *buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT * FROM impmap WHERE spell_id='" + spell_id + "','" + code_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            if(query.size() != 6)
                return 0; // read failed inside PostgreSQL
            QString spell_id = query.value(0).toString();
            QString code_id = query.value(1).toString();
            qDebug() << "Got impmap with spell_id=" + spell_id + ", code_id=" + code_id;
            buffer->spell_id = spell_id;
            buffer->code_id = code_id;
            return 1; // read succeeded
        }
    }
    return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.
}


int impmap_model::update_impmap(const impmap_item existing_item){

}

int impmap_model::delete_impmap(QString spell_id, QString code_id){

}


int impmap_model::read_impmaps(int limit, int offset, std::list<impmap_item*> &buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(!db.isValid())
        return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.

    QString sql_command = "SELECT * FROM impmap LIMIT " + QString::number(limit) + " OFFSET " + QString::number(offset) + ";";
    qDebug() << sql_command;
    QSqlQuery query(sql_command, db);
    query.first();
    for(int row = 0; row < limit; row++){
        if(!query.isActive())
            return -1; // read failed inside PostgreSQL
        //if(query.size() != 6)
        //    return 0; // read failed inside PostgreSQL
        if(!query.isValid())
            return 1; // if query is not valid, then all records must be already in buffer, lets return success
        QString spell_id = query.value(0).toString();
        QString code_id = query.value(1).toString();
        qDebug() << "Got impmap with spell_id=" + spell_id + ", code_id=" + code_id;
        impmap_item* another_impmap = new impmap_item();
        another_impmap->spell_id = spell_id;
        another_impmap->code_id = code_id;
        buffer.push_back(another_impmap);
        query.next();

    }
    return 1; // read succeeded
}
