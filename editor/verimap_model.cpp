#include "verimap_model.h"
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>

verimap_model::verimap_model(QObject *parent) : QObject(parent)
{

}

int verimap_model::count_verimaps(const QString like_pattern){
    // return -1 on error, otherwise return number of tags, which match given filters
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM verification;";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            int found_verimaps = query.value(0).toInt();
            return found_verimaps;
        }
        return -1;
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int verimap_model::count_verimaps(){
    // return number of all tags (all filters permissive) or -1 on error
    return count_verimaps("");
}

int verimap_model::create_verimap(const verimap_item new_verimap){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "INSERT INTO verimap VALUES ('" + new_verimap.verification_id + "','" + new_verimap.prediction_id + "','" + new_verimap.experiment_id + "','" + new_verimap.comparision + "','" + new_verimap.good_enough + "');";
        qDebug() << sql_command;
        db.exec(sql_command);
    }
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM verification WHERE verification_id='" + new_verimap.verification_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        if(query.isActive()){
            int found_verimaps = query.value(0).toInt();
            if(found_verimaps == 1)
                return 1; // creation succeeded

            return 0; // creation failed inside PostgreSQL
        }
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int verimap_model::read_verimap(QString verimap_id, verimap_item *buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT * FROM verification WHERE verification_id='" + verimap_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            if(query.size() != 6)
                return 0; // read failed inside PostgreSQL
            QString verification_id = query.value(0).toString();
            QString prediction_id = query.value(1).toString();
            QString experiment_id = query.value(2).toString();
            QString comparision = query.value(3).toString();
            QString good_enough = query.value(4).toString();
            qDebug() << "Got verification with verification_id=" + verification_id + ", prediction_id=" + prediction_id + ", experiment_id=" + experiment_id + ", comparision=" + comparision + ", good_enough=" + good_enough;
            buffer->verification_id = verification_id;
            buffer->prediction_id = prediction_id;
            buffer->experiment_id = experiment_id;
            buffer->comparision = comparision;
            buffer->good_enough = good_enough;
            return 1; // read succeeded
        }
    }
    return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int verimap_model::update_verimap(const verimap_item existing_verimap){

}

int verimap_model::delete_verimap(QString verimap_id){

}

int verimap_model::read_verimaps(int limit, int offset, std::list<verimap_item*> &buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(!db.isValid())
        return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.

    QString sql_command = "SELECT * FROM verimap LIMIT " + QString::number(limit) + " OFFSET " + QString::number(offset) + ";";
    qDebug() << sql_command;
    QSqlQuery query(sql_command, db);
    query.first();
    for(int row = 0; row < limit; row++){
        if(!query.isActive())
            return -1; // read failed inside PostgreSQL
        //if(query.size() != 6)
        //    return 0; // read failed inside PostgreSQL
        if(!query.isValid())
            return 1; // if query is not valid, then all records must be already in buffer, lets return success
        QString verification_id = query.value(0).toString();
        QString prediction_id = query.value(1).toString();
        QString experiment_id = query.value(2).toString();
        QString comparision = query.value(3).toString();
        QString good_enough = query.value(4).toString();
        qDebug() << "Got verification with verification_id=" + verification_id + ", prediction_id=" + prediction_id + ", experiment_id=" + experiment_id + ", compairision=" + comparision + ", good_enough=" + good_enough;
        verimap_item* another_verimap = new verimap_item();
        another_verimap->verification_id = verification_id;
        another_verimap->prediction_id = prediction_id;
        another_verimap->experiment_id = experiment_id;
        another_verimap->comparision = comparision;
        another_verimap->good_enough = good_enough;
        buffer.push_back(another_verimap);
        query.next();
    }
    return 1; // read succeeded
}


