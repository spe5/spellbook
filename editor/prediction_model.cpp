#include "prediction_model.h"
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>

prediction_model::prediction_model(QObject *parent) : QObject(parent)
{

}


int prediction_model::count_predictions(const QString like_pattern){
    // return -1 on error, otherwise return number of tags, which match given filters
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM prediction;";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            int found_predictions = query.value(0).toInt();
            return found_predictions;
        }
        return -1;
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int prediction_model::count_predictions(){
    // return number of all tags (all filters permissive) or -1 on error
    return count_predictions("");
}

int prediction_model::create_prediction(const prediction_item new_prediction){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "INSERT INTO prediction VALUES ('" + new_prediction.prediction_id + "','" + new_prediction.extended_id + "','" + new_prediction.maximum_resources + "','" + new_prediction.maximum_cost + "','" + new_prediction.result + "');";
        qDebug() << sql_command;
        db.exec(sql_command);
    }
    if(db.isValid()){
        QString sql_command = "SELECT COUNT(*) FROM prediction WHERE prediction_id='" + new_prediction.prediction_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        if(query.isActive()){
            int found_predictions = query.value(0).toInt();
            if(found_predictions == 1)
                return 1; // creation succeeded

            return 0; // creation failed inside PostgreSQL
        }
    }
    return -1; // creation failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int prediction_model::read_prediction(QString prediction_id, prediction_item *buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(db.isValid()){
        QString sql_command = "SELECT * FROM prediction WHERE prediction_id='" + prediction_id + "';";
        qDebug() << sql_command;
        QSqlQuery query(sql_command, db);
        query.first();
        if(query.isActive()){
            if(query.size() != 6)
                return 0; // read failed inside PostgreSQL
            QString prediction_id = query.value(0).toString();
            QString extended_id = query.value(1).toString();
            QString maximum_resources = query.value(2).toString();
            QString maximum_cost = query.value(3).toString();
            QString result = query.value(4).toString();
            qDebug() << "Got prediction with prediction_id=" + prediction_id + ", extended_id=" + extended_id + ", maximum_resources=" + maximum_resources + ", maximum_cost=" + maximum_cost + ", result=" + result;
            buffer->prediction_id = prediction_id;
            buffer->extended_id = extended_id;
            buffer->maximum_resources = maximum_resources;
            buffer->maximum_cost = maximum_cost;
            buffer->result = result;
            return 1; // read succeeded
        }
    }
    return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.
}

int prediction_model::update_prediction(const prediction_item existing_prediction){

}

int prediction_model::delete_prediction(QString prediction_id){

}

int prediction_model::read_predictions(int limit, int offset, std::list<prediction_item*> &buffer){
    // return 1 on success, 0 on fail inside PostgreSQL, or -1 on fail outside PostgreSQL (network, Qt, etc)
    QSqlDatabase db = QSqlDatabase::database();
    if(!db.isValid())
        return -1; // read failed outside of PostgreSQL, due to network, due to Qt, etc.

    QString sql_command = "SELECT * FROM prediction LIMIT " + QString::number(limit) + " OFFSET " + QString::number(offset) + ";";
    qDebug() << sql_command;
    QSqlQuery query(sql_command, db);
    query.first();
    for(int row = 0; row < limit; row++){
        if(!query.isActive())
            return -1; // read failed inside PostgreSQL
        //if(query.size() != 6)
        //    return 0; // read failed inside PostgreSQL
        if(!query.isValid())
            return 1; // if query is not valid, then all records must be already in buffer, lets return success
        QString prediction_id = query.value(0).toString();
        QString extended_id = query.value(1).toString();
        QString maximum_resources = query.value(2).toString();
        QString maximum_cost = query.value(3).toString();
        QString result = query.value(4).toString();
        qDebug() << "Got prediction with prediction_id=" + prediction_id;
        prediction_item* another_prediction = new prediction_item();
        another_prediction->prediction_id = prediction_id;
        another_prediction->extended_id = extended_id;
        another_prediction->maximum_resources = maximum_resources;
        another_prediction->maximum_cost = maximum_cost;
        another_prediction->result = result;
        buffer.push_back(another_prediction);
        query.next();

    }
    return 1; // read succeeded
}




