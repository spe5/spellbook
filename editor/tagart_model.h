#ifndef TAGART_MODEL_H
#define TAGART_MODEL_H

#include <QObject>

struct tagart_item{
    QString experiment_id;
    QString tag_id;
};

class tagart_model : public QObject
{
    Q_OBJECT
public:
    explicit tagart_model(QObject *parent = nullptr);

    int count_tagarts(const QString like_pattern);
    int count_tagarts();
    int create_tagart(const tagart_item new_tagart);
    int read_tagart(QString experiment_id, QString tag_id, tagart_item *buffer);
    int update_tagart(const tagart_item existing_tagart);
    int delete_tagart(QString experiment_id, QString tag_id);
    int read_tagarts(int limit, int offset, std::list<tagart_item*> &buffer);

    int visible_rows;
    int offset;

signals:

};

#endif // TAGART_MODEL_H
