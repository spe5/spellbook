#include "dialog_spell_edit.h"
#include "ui_dialog_spell_edit.h"

dialog_spell_edit::dialog_spell_edit(QWidget *parent, spell_model * the_spell_model, spell_item * the_spell_item) :
    QDialog(parent),
    ui(new Ui::dialog_spell_edit)
{
    ui->setupUi(this);
    create_new_spell = false;
    my_spell_model = the_spell_model;
    if(the_spell_item == nullptr){ // creating new spell
        create_new_spell = true;
        QString new_spell_id;
        the_spell_model->generate_random_id(new_spell_id);
        ui->lbl_id->setText(new_spell_id);
        int index_type = ui->cmb_type->findText("Definition");
        if (index_type != -1)
            ui->cmb_type->setCurrentIndex(index_type);
        int index_currency = ui->cmb_currency->findText("EUR");
        if (index_currency != -1)
            ui->cmb_currency->setCurrentIndex(index_currency);
    }else { // editing existing spell
        create_new_spell = false;
        ui->lbl_id->setText(the_spell_item->spell_id);
        ui->edt_name->insert(the_spell_item->name);
        spell_type st_type = the_spell_model->int_to_spell_type(the_spell_item->spell_type);
        QString qs_type = QString((the_spell_model->spell_type_to_string(st_type)).c_str());
        int index_type = ui->cmb_type->findText(qs_type);
        if (index_type != -1)
            ui->cmb_type->setCurrentIndex(index_type);
        ui->edt_cost->insert(the_spell_item->use_cost);
        int index_currency = ui->cmb_type->findText(the_spell_item->use_cost_currency);
        if (index_currency != -1)
            ui->cmb_currency->setCurrentIndex(index_currency);
        ui->edt_description->insertPlainText(the_spell_item->description);
    }
}

dialog_spell_edit::~dialog_spell_edit()
{
    delete ui;
}

void dialog_spell_edit::on_buttonBox_accepted()
{
    // TODO check valididty of user-provided values
    spell_item * new_spell_item = new spell_item();
    new_spell_item->spell_id = ui->lbl_id->text();
    new_spell_item->spell_type = my_spell_model->spell_type_to_int(my_spell_model->string_to_spell_type((ui->cmb_type->currentText()).toStdString()));
    new_spell_item->name = ui->edt_name->text();
    new_spell_item->use_cost = ui->edt_cost->text();
    new_spell_item->use_cost_currency = ui->cmb_currency->currentText();
    new_spell_item->description = ui->edt_description->toPlainText();
    if(create_new_spell){
        my_spell_model->create_spell(*new_spell_item);
    }else{
        my_spell_model->update_spell(*new_spell_item);
        // INSERT INTO spell VALUES ();
    }
}

void dialog_spell_edit::on_buttonBox_rejected()
{
    // close dialog, do nothing on the model
}

